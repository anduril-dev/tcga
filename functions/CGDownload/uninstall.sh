#!/bin/bash
## This script removes GeneTorrent of CGHub from  Ubuntu/CentOS system

version=0 # OS Distro version number

# Identifying distro

cat /proc/version | grep ubuntu # Is it Ubuntu?

if [ $? -eq 0 ]; then
	isUbuntu=1
	release=`lsb_release -a | grep Release`
#	echo "Release>>> $release <<<"
	if [[ $release =~ "10." ]]; then
		version=10
	fi
	if [[ $release =~ "11." ]]; then
		version=11
	fi
	if [[ $release =~ "12." ]]; then
		version=12
	fi
else
	isUbuntu=0
fi


cat /proc/version | grep centos #Is it CentOS

if [ $? -eq 0 ]; then
	isCentos=1
	release=`lsb_release -a | grep Release`
	if [[ $release =~ "5." ]]; then
		version=5
	fi
	if [[ $release =~ "6." ]]; then
		version=6
	fi
else
	isCentos=0
fi

if [ $isUbuntu -eq 1 ]; then
	echo "This is Ubuntu"
fi

if [ $isCentos -eq 1 ]; then
	echo "This is Centos"
fi


if [ $version -eq 5 ]; then # CentOS 5
	sudo rm /usr/bin/GeneTorrent
	sudo rm /usr/bin/GTLoadBalancer
	sudo rm /usr/bin/gtoinfo
	sudo rm /usr/share/GeneTorrent/dhparam.pem
	sudo rm -r /usr/share/GeneTorrent
	sudo rm /usr/share/man/man1/GeneTorrent.1
fi

if [ $version -eq 6 ]; then # CentOS 6
	sudo rm /usr/bin/GeneTorrent
	sudo rm /usr/bin/GTLoadBalancer
	sudo rm /usr/bin/gtoinfo
	sudo rm /usr/share/GeneTorrent/dhparam.pem
	sudo rm -r /usr/share/GeneTorrent
	sudo rm /usr/share/man/man1/GeneTorrent.1
fi

if [ $version -eq 10 ]; then # Ubuntu 10
	sudo rm /usr/bin/GeneTorrent
	sudo rm /usr/bin/GTLoadBalancer
	sudo rm /usr/bin/gtoinfo
	sudo rm /usr/share/GeneTorrent/dhparam.pem
	sudo rm -r /usr/share/GeneTorrent
	sudo rm /usr/share/man/man1/GeneTorrent.1
fi

if [ $version -eq 11 ]; then # Ubuntu 11
	sudo rm /usr/bin/GeneTorrent
	sudo rm /usr/bin/GTLoadBalancer
	sudo rm /usr/bin/gtoinfo
	sudo rm /usr/share/GeneTorrent/dhparam.pem
	sudo rm -r /usr/share/GeneTorrent
	sudo rm /usr/share/man/man1/GeneTorrent.1
fi

if [ $version -eq 12 ]; then # Ubuntu 12
	sudo rm /usr/bin/GeneTorrent
	sudo rm /usr/bin/GTLoadBalancer
	sudo rm /usr/bin/gtoinfo
	sudo rm /usr/share/GeneTorrent/dhparam.pem
	sudo rm -r /usr/share/GeneTorrent
	sudo rm /usr/share/man/man1/GeneTorrent.1
fi
