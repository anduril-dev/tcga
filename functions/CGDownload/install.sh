#!/bin/bash
## This script installs GeneTorrent from CGHub to Ubuntu/CentOS
## Note: only 64 bit OS is supported

if [ `uname -m` = "x86_64" ]; then
	echo	"This is 64bit system. Good :)"
else
	echo	"Oops. This is 32bit system. Only 64bit systems are currently supported in GeneTorrent!"
	exit	1
fi

version=0 # OS Distro version number

# Identifying distro

cat /proc/version | grep ubuntu # Is it Ubuntu?

if [ $? -eq 0 ]; then
	isUbuntu=1
	release=`lsb_release -a | grep Release`
#	echo "Release>>> $release <<<"
	if [[ $release =~ "10." ]]; then
		version=10
	fi
	if [[ $release =~ "11." ]]; then
		version=11
	fi
	if [[ $release =~ "12." ]]; then
		version=12
	fi
else
	isUbuntu=0
fi


cat /proc/version | grep centos #Is it CentOS

if [ $? -eq 0 ]; then
	isCentos=1
	release=`lsb_release -a | grep Release`
	if [[ $release =~ "5." ]]; then
		version=5
	fi
	if [[ $release =~ "6." ]]; then
		version=6
	fi
else
	isCentos=0
fi

if [ $isUbuntu -eq 1 ]; then
	echo "This is Ubuntu"
fi

if [ $isCentos -eq 1 ]; then
	echo "This is Centos"
fi

echo "Identified version number as $version"

srcRoot="https://cghub.ucsc.edu/software/downloads/GeneTorrent/3.3.4/"

if [ $version -eq 5 ]; then
	srcTARGZ="GeneTorrent-3.3.4-CentOS5.x86_64.tar.gz"
fi

if [ $version -eq 6 ]; then
	srcTARGZ="GeneTorrent-3.3.4-CentOS6.x86_64.tar.gz"
fi

if [ $version -eq 10 ]; then
	srcTARGZ="GeneTorrent-3.3.4-Ubuntu10.x86_64.tar.gz"
fi

if [ $version -eq 11 ]; then
	srcTARGZ="GeneTorrent-3.3.4-Ubuntu11.x86_64.tar.gz"
fi

if [ $version -eq 12 ]; then
	srcTARGZ="GeneTorrent-3.3.4-Ubuntu11.x86_64.tar.gz"
fi

# Searching/creating temp directory
mkdir -p ~/tmp/gtorrent/
if [ $? -gt 0 ]; then
	echo	"Error: could not create temp dir ~/tmp/gtorrent!"
	exit 1
fi

cd ~/tmp/gtorrent
wget $srcRoot$srcTARGZ --no-check-certificate -c

if [ $? -eq 0 ]; then
	sudo tar -zxvf $srcTARGZ -C /
	rm $srcTARGZ
	rm -r ~/tmp/gtorrent
else
	exit 1
fi