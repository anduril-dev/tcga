<?xml version="1.1" encoding="UTF-8" standalone="yes"?>
<component>
    <name>CGQuery</name>
    <version>1.0</version>
    <doc>Searches for the sequence data at CGHub.
    Implements CGHub REST API to query CGHub data portal. 
    
    	Each sequence in the CGHub is described by metadata, which contains 
		attributes such as the date, sample type, and disease abbreviation. (consult 
		<a href="https://cghub.ucsc.edu/docs/CGHubUserGuide.pdf#page=12">
		UCSC CGHub User Guide, section 6.1</a> for the list of searchable attributes).
		Querying the sequence metadata is a powerful method of identifying sequences 
		of interest.
				
		As an input, CGQuery function may take both the set of parameters and/or the table of queries.
		The function returns either one and/or array of XML files as a response to the query(ies).  
    	</doc>
    <author email="Vladimir.Rogojin@Helsinki.FI">Vladimir Rogojin</author>
    <category>Data Import</category>
    <inputs>
    	<input name="queryTable"	type="CSV"	optional="true">
    		<doc>The table of queries. Each row is a query. Each column is a query attribute (except 
    			of the column named "key" and "xml").	The column name	is the attribute name. The corresponding 
    			field content is the attribute value.
    			For each query in the table, cgquery will search for items in CGHub portal satisfying 
    			all the query attributes.
    			
    			If the query table is provided, the array of XML files will be generated as the output.
    			If xml-column is provided, XML files will be saved to the corresponding paths provided in 
    			xml-column.
    			The values from the key-column will be used as the key values for the output array, otherwise,
    			the key values will be automatically generated for the array.
    			
    			For the list of query attributes consult 
				<a href="https://cghub.ucsc.edu/docs/CGHubUserGuide.pdf#page=12">
				UCSC CGHub User Guide, section 6.1</a>.
    		</doc>
    	</input>
    </inputs>
    <outputs>
    	<output name="content_specifier" type="XML">
    		<doc>The content annotation for the query. It is generated if at least one non-default 
    			query parameter is provided.</doc>
    	</output>
    	<output name="content_specifiers" type="XML" array="true">
    		<doc>Array content annotation files for queries from the table. 
    		It is generated if the table of queries is provided.</doc>
    	</output>
    </outputs>
    <parameters>
    	<parameter name="analysis_id" type="string" default="">
    		<doc>
    			Globally unique ID (uuid) associated with a collection of sequencing data files and their metadata.
    		</doc>
    	</parameter>
    	<parameter name="analysis_accession" type="string" default="">
    		<doc>
    			Legacy SRZnnnnnn analysis accession # for data migrated from SRA.
    		</doc>
    	</parameter>
    	<parameter name="state" type="string" default="live">
    		<doc>
    			Determines if the submission is still being processed, is live or has been suppressed.  
				Users can only download data with state=live.
    		</doc>
    	</parameter>
    	<parameter name="study" type="string" default="phs000178">
    		<doc>
    			The study for which the data was generated.  This value is used to determine which 
				users can download the data.
    		</doc>
    	</parameter>
    	<parameter name="aliquot_id" type="string" default="">
    		<doc>
    			Globally unique ID for the aliquot used to generate this analysis. 
    			
    			This is a primary identifier for
				each project‟s sample database (e.g. TCGA‟s DCC Portal). 
				
				For TCGA, this will be a RFC4122 aliquot uuid 
				managed by the DCC.
			</doc>
    	</parameter>
    	<parameter name="sample_accession" type="string" default="">
    		<doc>
    			Legacy sample accession # for data migrated from SRA.
    		</doc>
    	</parameter>
    	<parameter name="legacy_sample_id" type="string" default="">
    		<doc>
    			This may be any project specific ID.
    			
    			For TCGA, this will be the 7 part barcode retrieved from the DCC.
    		</doc>
    	</parameter>
    	<parameter name="disease_abbr" type="string" default="">
    		<doc>
    			The short disease name. 
    			
    			The set of legal TCGA values are defined by the DCC codes table at: 
    			<a href="http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm">
    			http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm</a>
    		</doc>
    	</parameter>
    	<parameter name="participant_id" type="string" default="">
    		<doc>
    			The ID of the human participant providing the sample. Previously represented by the TCGA 3 
				part barcode (e.g. TCGA-AB-2802).
				
				For TCGA this will be a participant UUID provided by the DCC.
    		</doc>
    	</parameter>
    	<parameter name="sample_id" type="string" default="">
    		<doc>
    			The ID for the root sample from which the aliquot was derived. Previously represented by the TCGA 5 
				part barcode (e.g. TCGA-AB-2802-03C-01W).
				
				For TCGA this will be a sample UUID provided by the DCC.
    		</doc>
    	</parameter>
    	<parameter name="analyte_code" type="string" default="">
    		<doc>
    			The type of analyte (e.g. D=DNA, R=RNA, etc.).
    			
    			The set of legal TCGA values are defined by the DCC at: 
    			<a href="http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm">
    			http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm</a>
    		</doc>
    	</parameter>
    	<parameter name="sample_type" type="string" default="">
    		<doc>
    			The type of sample (e.g. Blood Derived Normal, Primary solid Tumor, etc.).
	
				The set of legal TCGA values are defined by the DCC codes table at:
				<a href="http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm">
				http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm</a>
    		</doc>
    	</parameter>
    	<parameter name="tss_id" type="string" default="">
    		<doc>
    			The Tissue Source Site.

				The set of legal TCGA values are defined by the DCC codes table at: 
				<a href="http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm">
				http://tcgadata.nci.nih.gov/datareports/codeTablesReport.htm</a>
    		</doc>
    	</parameter>
    	<parameter name="last_modified" type="string" default="">
    		<doc>
    			Date the object was last modified.
    		</doc>
    	</parameter>
    	<parameter name="center_name" type="string" default="">
    		<doc>
    			Short names defined by the project (e.g. BI, BCM).
		
				The set of legal TCGA values are defined in 
				<a href="https://cghub.ucsc.edu/docs/CGHubUserGuide.pdf#page=16">
				Table 1 – TCGA Center Names on page 16</a>.
    		</doc>
    	</parameter>
    	<parameter name="alias" type="string" default="">
    		<doc>
    			Name of the analysis object as defined by the submitting center.
    		</doc>
    	</parameter>
    	<parameter name="Title" type="string" default="">
    		<doc>
    			Title of the analysis object as defined by the submitting center.
    		</doc>
    	</parameter>
    	<parameter name="analysis_type" type="string" default="REFERENCE_ALIGNMENT">
    		<doc>
    			The type of analysis. Uses the values from SRA 1.3 schema. For TCGA, this will be REFERENCE_ALIGNMENT.
    		</doc>
    	</parameter>
    	<parameter name="library_strategy" type="string" default="">
    		<doc>
    			The sequencing technique used.  Values are defined by the SRA 1.3 schema:
				<ul>
					<li>WGS</li> 
					<li>WXS</li> 
					<li>RNA-Seq</li>
					<li>WCS</li>
					<li>CLONE</li>
					<li>POOLCLONE</li>
					<li>AMPLICON</li>
					<li>CLONEEND</li>
					<li>FINISHING</li>
					<li>ChIP-Seq</li>
					<li>MNase-Seq</li>
					<li>DNaseHypersensitivity</li>
					<li>Bisulfite-Seq</li>
					<li>EST</li>
					<li>FL-cDNA</li>
					<li>CTS</li>
					<li>MRE-Seq</li>
					<li>MeDIP-Seq</li>
					<li>MDB-Seq</li>
					<li>OTHER</li>
				</ul>
    		</doc>
    	</parameter>
    	<parameter name="platform" type="string" default="">
    		<doc>
    			The machine used to generate the sequence data. Values are defined by the SRA 1.3 schema:
    			<ul>
					<li>LS454</li>
					<li>ILLUMINA</li>
					<li>HELICOS</li>
					<li>ABI_SOLID</li>
					<li>COMPLETE_GENOMICS</li>
					<li>PACBIO_SMRT</li>
					<li>ION_TORRENT</li>
				</ul>
    		</doc>
    	</parameter>
    	<parameter name="filename" type="string" default="">
    		<doc>
    			Name of a data file associated with an analysis.
    		</doc>
    	</parameter>
    	<parameter name="xml_text" type="string" default="">
    		<doc>
    			Free form search from the original submission XML documents.
    		</doc>
    	</parameter>
    	<parameter name="custom_query" type="string" default="">
    		<doc>
    			The query string transmitted directly to <i>cgquery</i>. On the format of query string
    			consult <a href="https://cghub.ucsc.edu/docs/CGHubUserGuide.pdf#page=12">
				UCSC CGHub User Guide, section 6.1</a>.
    			If provided, overrides all the query parameters.
    		</doc>
    	</parameter>
    	<parameter name="server" type="string" default="https://cghub.ucsc.edu">
    		<doc>
    			CGHub server location, including protocol and port,
                        e.g. <a href="https://cghub-01.ucsc.edu">
                        https://cghub-01.ucsc.edu</a>
    		</doc>
    	</parameter>
    	<parameter name="attributes" type="boolean" default="false">
    		<doc>
    			 query the /cghub/metadata/analysisAttributes instead
                        of analysisObjects resource.
    		</doc>
    	</parameter>
    	<parameter name="verbose" type="boolean" default="false">
    		<doc>
    			 enable verbose output
    		</doc>
    	</parameter>
    </parameters>
</component>