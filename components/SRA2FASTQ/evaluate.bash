# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh" "$1"

read=$( getinput read )

reads=$( getoutput reads )
mate=$( getoutput mate )
fastqs=$( getoutput fastqs )
fastqsindex=$( getoutput _index_fastqs )

fastqDump=$( getparameter fastqDump )
options=$( getparameter options )
twoReadsOnly=$( getparameter twoReadsOnly )

mkdir -p "${fastqs}" || echo error creating output fastqs >> $errorfile

COMMAND=$( readlink -f "$fastqDump" )
writelog "Executing $COMMAND $read $options -O $fastqs"
"$COMMAND" "$read" $options -O "$fastqs"
exit_status=$?

IFS=$'\n'
files=( $( ls "$fastqs" ) )
[[ ${#files[@]} -eq 0 ]] && {
    writeerror "No files produced"
    exit 1
}
# stat the files to help out clearing NFS stale handles
for (( i=0; i<${#files[@]}; i++ ))
do stat -c "%n" ${fastqs}"/"${files[$i]} || i=$(( $i -1 ))
done

mv "${fastqs}"/${files[0]} "$reads"
addarray fastqs ${files[0]} "$reads"
# second output is copied if only one file generated
[[ ${#files[@]} -eq 1 ]] && {
    cp -l "$reads" "$mate"
}
# When two files generated, both outputs populated
[[ ${#files[@]} -gt 1 ]] && {
    mv "${fastqs}"/${files[1]} "$mate"
    addarray fastqs ${files[1]} "$mate"
}
# More than two files: keep the rest in the array folder, and add keys in the index
[[ ${#files[@]} -gt 2 ]] && {
    [[ $twoReadsOnly = "true" ]] && {
        writeerror "Too many files produced"
        exit 1
    } || {
        for (( i=2; i<${#files[@]}; i++ ))
        do addarray fastqs ${files[$i]} ${fastqs}"/"${files[$i]}
        done
    }
}

exit $exit_status
