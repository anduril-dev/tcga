import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.tcga.Cache;
import fi.helsinki.ltdk.csbl.tcga.DirBrowser;

import java.util.Hashtable;


public class Func2SeqRefTable extends SkeletonComponent{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	Func2SeqRefTable().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		CSVParser	fileReference=new	CSVParser(cf.getInput("fileReference"));
		CSVParser	manifest=cf.inputDefined("manifest")?
						new	CSVParser(cf.getInput("manifest")):null;
					
		
		String[]	seqFilesMapCols={"url","name","type","disease","patient","sample","sample_type",
						"sample_type_code","analyte_type","run","read","aux_files","last_modified","size","MD5"};				
		CSVWriter	seqFilesMap=new	CSVWriter(seqFilesMapCols,cf.getOutput("SeqFileMap"));
		
		Hashtable<String,String[]>	fileRefTable=new	Hashtable<String,String[]>();
		
		for(String[]	line:fileReference)
		{
			fileRefTable.put(line[fileReference.getColumnIndex("FileName")], line);
		}
		
		Hashtable<String,String[]>	manifestTable=new	Hashtable<String,String[]>();
		
		if(manifest!=null)
			for(String[]	line:manifest)
			{
				manifestTable.put(line[manifest.getColumnIndex("file9")], line);
			}
		
		for(String	name:fileRefTable.keySet()){
			if(!name.matches(".*\\.bam\\.(bai|header|md5|xml)")){
				String[]	line=fileRefTable.get(name);
				String	url=line[fileReference.getColumnIndex("ArchiveFileName")].replaceAll("\\.tar\\.gz", "");
				seqFilesMap.write(url);
				seqFilesMap.write(name);
				String	type="";
				if(name.matches(".*\\.bam"))
					type="analysis";
				else
					if(name.matches(".*\\.sra"))
						type="reads";
				seqFilesMap.write(type);
				seqFilesMap.write(line[fileReference.getColumnIndex("CancerType")]);
				String	sample=line[fileReference.getColumnIndex("Aliquots")];
				seqFilesMap.write(sample.replaceAll("-\\w\\w\\w-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w", ""));
				seqFilesMap.write(sample);
				String	sample_type;
				if(sample.matches("TCGA-\\w\\w-\\w\\w\\w\\w-(01|02|03|04|05|06|07|08|09)\\w-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w"))
					sample_type="tumor";
				else
					if(sample.matches("TCGA-\\w\\w-\\w\\w\\w\\w-(10|11|12|13|14|15|16|17|18|19)\\w-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w"))
						sample_type="normal";
					else
						sample_type="control";
				seqFilesMap.write(sample_type);
				String	sample_type_code=sample.replaceAll("TCGA-\\w\\w-\\w\\w\\w\\w-", 
						"").replaceAll("\\w-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w","");
				seqFilesMap.write(sample_type_code);
				String	analyte_type=sample.replaceAll("TCGA-\\w\\w-\\w\\w\\w\\w-\\w\\w\\w-\\w\\w", 
				"").replaceAll("-\\w\\w\\w\\w-\\w\\w","");
				seqFilesMap.write(analyte_type);
				String[]	manifestLine=manifestTable.get(name);
				String	run="";
				String	read="";
				if(manifestLine!=null){
					run=manifestLine[manifest.getColumnIndex("file7")];
					read=manifestLine[manifest.getColumnIndex("file8")];
				}
				seqFilesMap.write(run);
				seqFilesMap.write(read);
				String	aux_files="";
				if(fileRefTable.get(name+".bai")!=null)
					aux_files+=(aux_files.length()>0?",":"")+
						name+".bai";
				if(fileRefTable.get(name+".header")!=null)
					aux_files+=(aux_files.length()>0?",":"")+
						name+".header";
				if(fileRefTable.get(name+".md5")!=null)
					aux_files+=(aux_files.length()>0?",":"")+
						name+".md5";
				seqFilesMap.write(aux_files);
				seqFilesMap.write(Cache.getLastModified(url, "", ""));
				seqFilesMap.write(Cache.getSize(url, "", ""));
				if(DirBrowser.fileAccessible(url+".md5", "", ""))
				{
					String	md5str=Cache.getMD5(url, "", "");
					if(md5str.length()==0)
						md5str=Cache.calculateMD5(url,"","");
					seqFilesMap.write(md5str);
				}
				else
					{
					String	md5str=(Cache.calculateMD5(url,"",""));
					seqFilesMap.write(md5str);
					Cache.writeMD5(url.replaceAll("file:", ""),md5str);
					}
//				Cache.calculateMD5(url,"","");
			}
		}
		
		seqFilesMap.close();
		manifest.close();
		fileReference.close();
		
		return ErrorCode.OK;
	}

}
