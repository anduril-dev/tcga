import java.math.BigInteger;
import java.net.URL;


public class ArchivesTableRow {
	public	URL	url;
	public	String	DataLevel;
	public	String	Cancer;
	public	String	CenterType;
	public	String	Center;
	public	String	Platform;
	public	String	DataType;
	public	String	Archive;
	public	long	size;
	public	long	time;
	public	boolean	isDir;
	public	int	Index;
	public	int	Revision;
	public	int	Series;
}
