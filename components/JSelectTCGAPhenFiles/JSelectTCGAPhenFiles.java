import java.io.File;
import java.net.URL;
import java.util.LinkedList;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.tcga.AuthUtils;
import fi.helsinki.ltdk.csbl.tcga.Cache;
import fi.helsinki.ltdk.csbl.tcga.DirBrowser;


public class JSelectTCGAPhenFiles extends SkeletonComponent{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	JSelectTCGAPhenFiles().run(args);
	}

	
	
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		String	remoteSiteURL=cf.getParameter("remoteSiteURL");
		boolean	authorized=cf.getBooleanParameter("authorized");
		String	remoteURL;
		String	directoryPriority=cf.getParameter("directoryPriority");
		
		File	authFile=cf.getInput("authFile");
		File	keyFile=cf.getInput("keyFile");
		
		String	authData=AuthUtils.getAuthData(authFile, keyFile);
		String	userName=AuthUtils.getUserName(authData);
		String	passwd=AuthUtils.getPasswd(authData);
		
		if (remoteSiteURL.length()==0) {
				if (!authorized) {
					remoteURL="https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/anonymous/tumor/";
				}
				else
					{
						remoteURL="https://tcga-data-secure.nci.nih.gov/tcgafiles/tcga4yeo/tumor/";
					}
			}
				else
					{
						remoteURL=remoteSiteURL+"/";
					}
		
		remoteURL=remoteURL.replaceAll("://", "###").replaceAll("///","/").replaceAll("//", "/").replaceAll("###", "://");
		remoteURL=remoteURL+"###";
		remoteURL=remoteURL.replaceAll("/###", "").replaceAll("###", "");
		
		CSVParser	selection=new	CSVParser(cf.getInput("selection"));
		
		LinkedList<ArchivesTableRow>	includeArchiveTables=new	LinkedList<ArchivesTableRow>();
		LinkedList<ArchivesTableRow>	excludeArchiveTables=new	LinkedList<ArchivesTableRow>();
		for(String[]	row:selection)
		{
			String[]	cancers=AsserUtil.split(row[selection.getColumnIndex("Cancer")],",");
			String[]	centers=AsserUtil.split(row[selection.getColumnIndex("Center")],",");
			String[]	platforms=AsserUtil.split(row[selection.getColumnIndex("Platform")],",");
			String[]	dataTypes=AsserUtil.split(row[selection.getColumnIndex("DataType")],",");
			String[]	dataLevelsAbr;
			
			String	datalevel=row[selection.getColumnIndex("DataLevel")];
			if(datalevel.length()>0)
				dataLevelsAbr=AsserUtil.split(datalevel+",.mage-tab.",",");
			else
				dataLevelsAbr=AsserUtil.split("level1,level2,level3,level4,aux,clin,unknown,.mage-tab.",",");
			String[]	dataLevels=new	String[dataLevelsAbr.length];
			int	i=0;
			for(String	dataLevel:dataLevelsAbr)
			{
				
				if(!dataLevel.matches("level1|level2|level3|level4|aux|clin|unknown|\\.mage-tab\\.")){
					cf.writeError("Invalid datalevel abbreviation "+dataLevel+" in selection input");
					return	ErrorCode.INVALID_INPUT;
				}
				dataLevels[i++]=dataLevel.replace("level1", ".Level_1.").replace("level2", ".Level_2.").replace("level3", ".Level_3.").replace("level4", ".Level_4.").replace("aux", ".aux.").replace("clin", "_clinical_.*").replace("unknown", ".");
			}
			System.out.println("Inspecting directories:");
			for(String	cancer:cancers){
				System.out.println(cancer);
				DirBrowser	cancerPaths=new	DirBrowser(remoteURL+"/"+cancer, userName, passwd);
				for(String	cancer_centertype:cancerPaths.browseDirsOnly()){
					System.out.println("\t"+cancer_centertype);
					DirBrowser	cancerCenterTypePaths=new	DirBrowser(remoteURL+"/"+cancer+"/"+cancer_centertype, 
									userName, passwd);
					LinkedList<String>	filteredCancerCenterTypeContent=new	LinkedList<String>();
					if(centers[0].length()>0)
					{
						for(String	center:centers)
						{
							
							for(String	center2:cancerCenterTypePaths.browseDirsOnly())
							{
//								System.out.println("center="+center+",center2="+center2);
								if(center.equalsIgnoreCase(center2))
									filteredCancerCenterTypeContent.add(center2);
							}
						}
					}
					else
					{
						for(String	center2:cancerCenterTypePaths.browseDirsOnly())
						{
							filteredCancerCenterTypeContent.add(center2);
						}
					}
					for(String	center:filteredCancerCenterTypeContent){
						System.out.println("\t\t"+center);
						DirBrowser	centerPaths=new	DirBrowser(remoteURL+"/"+cancer+"/"+cancer_centertype+"/"+center, 
								userName, passwd);	
						LinkedList<String>	filteredCenterContent=new	LinkedList<String>();
						if(platforms[0].length()>0)
						{
							for(String	platform:platforms){
								for(String	platform2:centerPaths.browseDirsOnly()){
									if(platform.equalsIgnoreCase(platform2))
										filteredCenterContent.add(platform2);
								}
							}
						}
						else
						{
							for(String	platform2:centerPaths.browseDirsOnly()){
								filteredCenterContent.add(platform2);
							}
						}
						for(String	platform:filteredCenterContent){
							System.out.println("\t\t\t"+platform);
							DirBrowser	platformPaths=new	DirBrowser(remoteURL+"/"+cancer+"/"+cancer_centertype+"/"+center+"/"+
											platform, 
											userName, passwd);
							LinkedList<String>	filteredPlatformContent=new	LinkedList<String>();
							if(dataTypes[0].length()>0){
								for(String	datatype:dataTypes){
									for(String	datatype2:platformPaths.browseDirsOnly()){
										if(datatype.equalsIgnoreCase(datatype2))
											filteredPlatformContent.add(datatype2);
									}
								}
							}
							else
							{
								for(String	datatype2:platformPaths.browseDirsOnly()){
									filteredPlatformContent.add(datatype2);
								}
							}
							for(String	dataType:filteredPlatformContent){
								System.out.println("\t\t\t\t"+dataType);
								DirBrowser	dataTypePaths=new	DirBrowser(remoteURL+"/"+cancer+"/"+cancer_centertype+"/"+center+"/"+
										platform+"/"+dataType, 
										userName, passwd);
								for(String	dataLevel:dataLevels){
									String	dataLevel2=dataLevel.replaceAll("\\.Level_1\\.","level1").replaceAll("\\.Level_2\\.","level2").replaceAll("\\.Level_3\\.","level3").replaceAll("\\.Level_4\\.","level4").replaceAll("\\.aux\\.", "aux").replaceAll("_clinical_\\.\\*","clin").replaceAll("\\.mage-tab\\.", "mage-tab").replaceAll("\\.", "unknown");
									System.out.println("\t\t\t\t\t"+dataLevel2);
									LinkedList<String>	filteredDataTypeFiles=new	LinkedList<String>();
									for(String	archive:dataTypePaths.browseFilesOnly()){
										if(archive.matches("(?i)"+center+"(_"+cancer+
																 "."+platform+")?"+
																 dataLevel+
																 "(\\d{1,6}\\.\\d{1,6}\\.\\d{1,6})?\\.(tar\\.gz|txt)")){
											filteredDataTypeFiles.add(archive);
										}
									}
									LinkedList<String>	filteredDataTypeFolders=new	LinkedList<String>();
									for(String	archive:dataTypePaths.browseDirsOnly()){
										if(archive.matches("(?i)"+center+"_"+cancer+
												 "."+platform+
												 dataLevel+
												 "\\d{1,6}\\.\\d{1,6}\\.\\d{1,6}")){
											filteredDataTypeFolders.add(archive);
										}
									}
									LinkedList<String>	archivesTableFilesFolders=new	LinkedList<String>();
									archivesTableFilesFolders.addAll(filteredDataTypeFiles);
									archivesTableFilesFolders.addAll(filteredDataTypeFolders);
									LinkedList<ArchivesTableRow>	archivesTableAnnotated=new	LinkedList<ArchivesTableRow>();
									for(String	archive:archivesTableFilesFolders)
									{
										ArchivesTableRow	archiveRow=new	ArchivesTableRow();
										archiveRow.url=new	URL(remoteURL+"/"+cancer+"/"+cancer_centertype+"/"+center+"/"+
														platform+"/"+dataType+"/"+archive);
										archiveRow.Cancer=cancer;
										archiveRow.Center=center;
										archiveRow.CenterType=cancer_centertype;
										archiveRow.DataLevel=dataLevel2;
										archiveRow.DataType=dataType;
//										System.out.println("DataLevel="+archiveRow.DataLevel);
										if(!archiveRow.DataLevel.equalsIgnoreCase("clin")){
											archiveRow.Index=new	Integer("0"+archive.replaceAll("(?i)"+center+"_"+cancer+
													 "."+platform+dataLevel, "").replaceAll("\\.\\d{1,6}\\.\\d{1,6}(\\.tar\\.gz)?", 
															 ""));
											archiveRow.Revision=new	Integer("0"+archive.replaceAll("(?i)"+center+"_"+cancer+
													 "."+platform+dataLevel+"\\d{1,6}\\.", "").replaceAll("\\.\\d{1,6}(\\.tar\\.gz)?", 
													 ""));
											archiveRow.Series=new	Integer("0"+archive.replaceAll("(?i)"+center+"_"+cancer+
													 "."+platform+dataLevel+"\\d{1,6}\\.\\d{1,6}\\.", 
													 	"").replaceAll("(\\.tar\\.gz)?", 
													 		""));
										}
										else
											archiveRow.Index=archiveRow.Revision=archiveRow.Series=0;
										
										archiveRow.isDir=filteredDataTypeFolders.contains(archive);
										archiveRow.Platform=platform;
										
										if(!archiveRow.isDir)
											archiveRow.size=Cache.getSize(archiveRow.url.toString(), userName, passwd);
										else
											archiveRow.size=0;
										archiveRow.time=Cache.getLastModified(archiveRow.url.toString(), userName, passwd);
										archiveRow.Archive=archive;
										archivesTableAnnotated.add(archiveRow);
										System.out.println("\t\t\t\t\t\t"+archiveRow.Archive);
									}
									if(cf.getBooleanParameter("updateLatest")){
										if(!dataLevel2.equalsIgnoreCase("clin")){
											for(ArchivesTableRow	archiveRow:archivesTableAnnotated){
												int	revision=archiveRow.Revision;
												for(ArchivesTableRow	archiveRow2:archivesTableAnnotated){
													if((archiveRow.Index==archiveRow2.Index)&&(archiveRow2.Revision>revision)&&
															(!archiveRow.DataLevel.equalsIgnoreCase("clin"))&&
																(!archiveRow2.DataLevel.equalsIgnoreCase("clin")))
														revision=archiveRow2.Revision;
												}
												int	count=0;
												for(ArchivesTableRow	archiveRow2:archivesTableAnnotated){
													if((archiveRow.Index==archiveRow2.Index)&&(archiveRow2.Revision==revision)&&
															(!archiveRow.DataLevel.equalsIgnoreCase("clin"))&&
																(!archiveRow2.DataLevel.equalsIgnoreCase("clin")))
													{
														count++;
														if(count>2)
														{
															cf.writeError("Internal error: too many archive files "+archiveRow.Archive+" with same revision");
															return	ErrorCode.ERROR;
														}
														if(!includeArchiveTables.contains(archiveRow2)){
															includeArchiveTables.add(archiveRow2);
														}
													}
												}
											}
											excludeArchiveTables.addAll(archivesTableAnnotated);
											excludeArchiveTables.removeAll(includeArchiveTables);
										}
										else
											includeArchiveTables.addAll(archivesTableAnnotated);
									}
									else
										includeArchiveTables.addAll(archivesTableAnnotated);
								}
									
							}
						}
					}
				}
			}
		}
		
		LinkedList<ArchivesTableRow>	includeArchives=new	LinkedList<ArchivesTableRow>();
		LinkedList<ArchivesTableRow>	excludeArchives=new	LinkedList<ArchivesTableRow>();
		for(ArchivesTableRow	archiveRow:includeArchiveTables)
		{
//			System.out.println(">>> "+archiveRow.Archive+" : "+archiveRow.DataLevel);
			if(!archiveRow.DataLevel.equalsIgnoreCase("clin")){
				boolean	filePresent=false;
				boolean	folderPresent=false;
				for(ArchivesTableRow	archiveRow2:includeArchiveTables)
				{
					if((archiveRow.Index==archiveRow2.Index)&&(archiveRow.Revision==archiveRow2.Revision)&&
							(archiveRow.Series==archiveRow2.Series))
					{
						folderPresent=folderPresent||archiveRow2.isDir;
						filePresent=filePresent||!archiveRow2.isDir;
					}
				}
				if((directoryPriority.equalsIgnoreCase("file-only"))||((directoryPriority.equalsIgnoreCase("file-then-folder"))
						&&(filePresent))){
					if(!archiveRow.isDir)
						includeArchives.add(archiveRow);
					else
						excludeArchives.add(archiveRow);
				}
				else
					if((directoryPriority.equalsIgnoreCase("folder-only"))||((directoryPriority.equalsIgnoreCase("folder-then-file"))
							&&(!filePresent))){
						if(archiveRow.isDir)
							includeArchives.add(archiveRow);
						else
							excludeArchives.add(archiveRow);
					}
					else
						includeArchives.add(archiveRow);			
			}
			else
				if(!directoryPriority.equalsIgnoreCase("folder-only"))
					includeArchives.add(archiveRow);
				else
					excludeArchives.add(archiveRow);
		}
		excludeArchives.addAll(excludeArchiveTables);
		
		String[]	columns={"url","DataLevel","Cancer","CenterType","Center","Platform","DataType","Archive","size",
						"time","isDir","Index","Revision","Series"};
		
		CSVWriter	includeArchivesCSV=new	CSVWriter(columns, cf.getOutput("includeArchives"));
		CSVWriter	excludeArchivesCSV=new	CSVWriter(columns, cf.getOutput("excludeArchives"));
		CSVWriter	parentFoldersCSV=new	CSVWriter(columns, cf.getOutput("parentFolders"));
		
		for(ArchivesTableRow	archiveRow:includeArchives)
		{
			includeArchivesCSV.write(archiveRow.url.toString());
			includeArchivesCSV.write(archiveRow.DataLevel);
			includeArchivesCSV.write(archiveRow.Cancer);
			includeArchivesCSV.write(archiveRow.CenterType);
			includeArchivesCSV.write(archiveRow.Center);
			includeArchivesCSV.write(archiveRow.Platform);
			includeArchivesCSV.write(archiveRow.DataType);
			includeArchivesCSV.write(archiveRow.Archive);
			includeArchivesCSV.write(archiveRow.size);
			includeArchivesCSV.write(archiveRow.time);
			includeArchivesCSV.write(archiveRow.isDir);
			includeArchivesCSV.write(archiveRow.Index);
			includeArchivesCSV.write(archiveRow.Revision);
			includeArchivesCSV.write(archiveRow.Series);
			
			String[]	path=AsserUtil.split(archiveRow.url.toString(),"/");
			String	parent=path[0];
			for(int	i=1;i<path.length-1;i++)
				parent=parent+"/"+path[i];
			parentFoldersCSV.write(parent);
			parentFoldersCSV.write(archiveRow.DataLevel);
			parentFoldersCSV.write(archiveRow.Cancer);
			parentFoldersCSV.write(archiveRow.CenterType);
			parentFoldersCSV.write(archiveRow.Center);
			parentFoldersCSV.write(archiveRow.Platform);
			parentFoldersCSV.write(archiveRow.DataType);
			parentFoldersCSV.write(archiveRow.Archive);
			parentFoldersCSV.write(archiveRow.size);
			parentFoldersCSV.write(archiveRow.time);
			parentFoldersCSV.write(archiveRow.isDir);
			parentFoldersCSV.write(archiveRow.Index);
			parentFoldersCSV.write(archiveRow.Revision);
			parentFoldersCSV.write(archiveRow.Series);
		}
		includeArchivesCSV.close();
		
		for(ArchivesTableRow	archiveRow:excludeArchives) 
		{
			excludeArchivesCSV.write(archiveRow.url.toString());
			excludeArchivesCSV.write(archiveRow.DataLevel);
			excludeArchivesCSV.write(archiveRow.Cancer);
			excludeArchivesCSV.write(archiveRow.CenterType);
			excludeArchivesCSV.write(archiveRow.Center);
			excludeArchivesCSV.write(archiveRow.Platform);
			excludeArchivesCSV.write(archiveRow.DataType);
			excludeArchivesCSV.write(archiveRow.Archive);
			excludeArchivesCSV.write(archiveRow.size);
			excludeArchivesCSV.write(archiveRow.time);
			excludeArchivesCSV.write(archiveRow.isDir);
			excludeArchivesCSV.write(archiveRow.Index);
			excludeArchivesCSV.write(archiveRow.Revision);
			excludeArchivesCSV.write(archiveRow.Series);
			
			String[]	path=AsserUtil.split(archiveRow.url.toString(),"/");
			String	parent=path[0];
			for(int	i=1;i<path.length-1;i++)
				parent=parent+"/"+path[i];
			parentFoldersCSV.write(parent);
			parentFoldersCSV.write(archiveRow.DataLevel);
			parentFoldersCSV.write(archiveRow.Cancer);
			parentFoldersCSV.write(archiveRow.CenterType);
			parentFoldersCSV.write(archiveRow.Center);
			parentFoldersCSV.write(archiveRow.Platform);
			parentFoldersCSV.write(archiveRow.DataType);
			parentFoldersCSV.write(archiveRow.Archive);
			parentFoldersCSV.write(archiveRow.size);
			parentFoldersCSV.write(archiveRow.time);
			parentFoldersCSV.write(archiveRow.isDir);
			parentFoldersCSV.write(archiveRow.Index);
			parentFoldersCSV.write(archiveRow.Revision);
			parentFoldersCSV.write(archiveRow.Series);
		}
		excludeArchivesCSV.close();
		
		parentFoldersCSV.close();
		
		return ErrorCode.OK;
	}

}
