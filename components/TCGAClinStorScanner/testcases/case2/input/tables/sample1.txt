bcr_sample_barcode	current_weight	days_to_collection	freezing_method	initial_weight	intermediate_dimension	longest_dimension	oct_embedded	sample_type	shortest_dimension	time_between_clamping_and_freezing	time_between_excision_and_freezing
TCGA-AA-A0CX-01A	null	null	null	560	null	null	true	Primary Tumor	null	null	null
TCGA-AA-A0CX-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AA-A0D0-01A	null	null	null	500	null	null	true	Primary Tumor	null	null	null
TCGA-AA-A0D0-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AA-A0D4-01A	null	null	null	220	null	null	true	Primary Tumor	null	null	null
TCGA-AA-A0D4-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AB-A0CD-01A	null	null	null	420	null	null	false	Primary Tumor	null	null	null
TCGA-AB-A0CD-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AB-A0CE-01A	null	null	null	330	null	null	false	Primary Tumor	null	null	null
TCGA-AB-A0CE-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AB-A0CG-01A	null	null	null	200	null	null	false	Primary Tumor	null	null	null
TCGA-AB-A0CG-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AB-A0CH-01A	null	null	null	360	null	null	false	Primary Tumor	null	null	null
TCGA-AB-A0CH-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
TCGA-AB-A0CJ-01A	null	null	null	310	null	null	false	Primary Tumor	null	null	null
TCGA-AB-A0CJ-10A	null	null	null	null	null	null	false	Blood Derived Normal	null	null	null
