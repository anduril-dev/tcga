import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CSVReader;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;


public class TCGAClinStorScanner extends SkeletonComponent{

	/**
	 * @param args
	 */
	
	private	boolean	timeStampPrefix;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	TCGAClinStorScanner().run(args);
	}
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		IndexFile	tables=IndexFile.read(cf.getInputArrayIndex("tables"));
		Hashtable<String,String>	keyDiseaseMapping=null;
		
		this.timeStampPrefix = cf.getBooleanParameter("timeStampPrefix");
		
		if(cf.getInput("keyDiseaseMapping")!=null){
			CSVReader	keyDiseaseMappingCSV=new	CSVReader(cf.getInput("keyDiseaseMapping"));
			keyDiseaseMapping=new	Hashtable<String,String>();
			for(List<String>	row:keyDiseaseMappingCSV){
				keyDiseaseMapping.put(row.get(0),row.get(1));
			}
		}
		
		Hashtable<File,String>	fileDiseaseMapping=null;
		if(keyDiseaseMapping!=null)
		{
			fileDiseaseMapping=new	Hashtable<File,String>();
			for(String	keyRegexp:keyDiseaseMapping.keySet())
			{
				for(String	key:tables)
				{
					if(key.matches(keyRegexp))
						fileDiseaseMapping.put(tables.getFile(key), keyDiseaseMapping.get(keyRegexp));
				}
			}
		}
		
		Hashtable<File,String>	fileTimestampMapping=new	Hashtable<File,String>();
		for(String	key:tables)
		{
			File	file=tables.getFile(key);
			String	timeStamp=timeStampPrefix?key.replaceAll("_.*", ""):
				Long.toString(file.lastModified());
			fileTimestampMapping.put(file, timeStamp);
		}
		
		//----------------------------------------------------------------------
		// Building list of columns for the output tables
		LinkedList<String>	aliquotColumns=new	LinkedList<String>();
		LinkedList<File>	aliquotFiles=new	LinkedList<File>();
		aliquotColumns.add("disease");
		aliquotColumns.add("fileTime");
		aliquotColumns.add("upTime");
		aliquotColumns.add("latest");
		
		LinkedList<String>	analyteColumns=new	LinkedList<String>();
		LinkedList<File>	analyteFiles=new	LinkedList<File>();
		analyteColumns.add("disease");
		analyteColumns.add("fileTime");
		analyteColumns.add("upTime");
		analyteColumns.add("latest");
		
		LinkedList<String>	drugColumns=new	LinkedList<String>();
		LinkedList<File>	drugFiles=new	LinkedList<File>();
		drugColumns.add("disease");
		drugColumns.add("fileTime");
		drugColumns.add("upTime");
		drugColumns.add("latest");
		
		
		LinkedList<String>	patientColumns=new	LinkedList<String>();
		LinkedList<File>	patientFiles=new	LinkedList<File>();
		patientColumns.add("disease");
		patientColumns.add("fileTime");
		patientColumns.add("upTime");
		patientColumns.add("latest");
		
		LinkedList<String>	portionColumns=new	LinkedList<String>();
		LinkedList<File>	portionFiles=new	LinkedList<File>();
		portionColumns.add("disease");
		portionColumns.add("fileTime");
		portionColumns.add("upTime");
		portionColumns.add("latest");
		
		LinkedList<String>	radiationColumns=new	LinkedList<String>();
		LinkedList<File>	radiationFiles=new	LinkedList<File>();
		radiationColumns.add("disease");
		radiationColumns.add("fileTime");
		radiationColumns.add("upTime");
		radiationColumns.add("latest");
		
		LinkedList<String>	sampleColumns=new	LinkedList<String>();
		LinkedList<File>	sampleFiles=new	LinkedList<File>();
		sampleColumns.add("disease");
		sampleColumns.add("fileTime");
		sampleColumns.add("upTime");
		sampleColumns.add("latest");
		
		LinkedList<String>	slideColumns=new	LinkedList<String>();
		LinkedList<File>	slideFiles=new	LinkedList<File>();
		slideColumns.add("disease");
		slideColumns.add("fileTime");
		slideColumns.add("upTime");
		slideColumns.add("latest");
		
		for(String	key:tables)
		{
			File	tableFile;
			CSVReader	table=new	CSVReader(tableFile=tables.getFile(key));
			if(table.getHeader().contains("bcr_aliquot_barcode"))
			{
					aliquotColumns.addAll(table.getHeader());
					aliquotFiles.add(tableFile);
					table.close();
					continue;
			}
			if(table.getHeader().contains("bcr_analyte_barcode")){
						analyteColumns.addAll(table.getHeader());
						analyteFiles.add(tableFile);
						table.close();
						continue;
			}
			if(table.getHeader().contains("bcr_drug_barcode")){
						drugColumns.addAll(table.getHeader());
						drugFiles.add(tableFile);
						table.close();
						continue;
			}
			if(table.getHeader().contains("bcr_portion_barcode")){
				portionColumns.addAll(table.getHeader());
				portionFiles.add(tableFile);
				table.close();
				continue;
			}
			if(table.getHeader().contains("bcr_radiation_barcode")){
				radiationColumns.addAll(table.getHeader());
				radiationFiles.add(tableFile);
				table.close();
				continue;
			}
			if(table.getHeader().contains("bcr_slide_barcode")){
				slideColumns.addAll(table.getHeader());
				slideFiles.add(tableFile);
				table.close();
				continue;
			}
			if(table.getHeader().contains("bcr_sample_barcode")){
					sampleColumns.addAll(table.getHeader());
					sampleFiles.add(tableFile);
					table.close();
					continue;
			}
			if(table.getHeader().contains("bcr_patient_barcode")){
				patientColumns.addAll(table.getHeader());
				patientFiles.add(tableFile);
			}
			table.close();
		}
		
//		System.out.println("\nALIQUOT TABLE:");
		writeTable(aliquotColumns,"bcr_aliquot_barcode",aliquotFiles,cf.getOutput("aliquot"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nANALYTE TABLE:");
		writeTable(analyteColumns,"bcr_analyte_barcode",analyteFiles,cf.getOutput("analyte"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nDRUG TABLE:");
		writeTable(drugColumns,"bcr_drug_barcode",drugFiles,cf.getOutput("drug"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nPORTION TABLE:");
		writeTable(portionColumns,"bcr_portion_barcode",portionFiles,cf.getOutput("portion"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nRADIATION TABLE:");
		writeTable(radiationColumns,"bcr_radiation_barcode",radiationFiles,cf.getOutput("radiation"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nSLIDE TABLE:");
		writeTable(slideColumns,"bcr_slide_barcode",slideFiles,cf.getOutput("slide"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nSAMPLE TABLE:");
		writeTable(sampleColumns,"bcr_sample_barcode",sampleFiles,cf.getOutput("sample"),
				fileDiseaseMapping,fileTimestampMapping);
//		System.out.println("\nPATIENT TABLE:");
		writeTable(patientColumns,"bcr_patient_barcode",patientFiles,cf.getOutput("patient"),
				fileDiseaseMapping,fileTimestampMapping);
		
		return ErrorCode.OK;
	}

	private void writeTable(LinkedList<String> columns,String	keyColumn,
			LinkedList<File> files, File output, Hashtable<File,String> fileDiseaseMapping,
			Hashtable<File,String>	fileTimestampMapping) throws IOException {
		// TODO Auto-generated method stub
		LinkedList<String>	noDupColumns=new	LinkedList<String>();
		
		for(String	column:columns)
			if(!noDupColumns.contains(column)){
				noDupColumns.add(column);
//				System.out.print(column+"\t");
			}
		
		String[]	cols;
		CSVWriter	out=new	CSVWriter(cols=noDupColumns.toArray(new String[noDupColumns.size()]),output,
				true);
		LinkedList<String[]>	outTable=new	LinkedList<String[]>();
		LinkedList<String[]>	outTable2=new	LinkedList<String[]>();
		for(File	file:files)
		{
			CSVParser	csv=new	CSVParser(file);
			String	disease=null;
			String	fileTime=fileTimestampMapping.get(file);
			String	upTime=Long.toString(System.currentTimeMillis());
			
			if(fileDiseaseMapping!=null)
			{
				disease=fileDiseaseMapping.get(file);
			}
			
			for(String[]	row:csv)
			{
				String[]	line=new	String[noDupColumns.size()];
				
				int	i=0;
				
				line[noDupColumns.indexOf("disease")]=disease;
				line[noDupColumns.indexOf("fileTime")]=fileTime;
				line[noDupColumns.indexOf("upTime")]=upTime;
				line[noDupColumns.indexOf("latest")]="FALSE";
//				System.out.println();
				for(String	item:row)
				{
//					System.out.println("### "+csv.getColumnNames()[i]);
					if(item!=null)
						if(item.equals("null"))
							item=null;
					line[noDupColumns.indexOf(csv.getColumnNames()[i])]=item;
					i++;
				}
				outTable.add(line);
//				for(String	item:line)
//					out.write(item);
			}
			
			csv.close();
		}
		
		LinkedList<String>	keys=new	LinkedList<String>();
		for(String[]	line:outTable)
		{
			boolean	addNewLine=true;
			for(String[]	line2:outTable2)
			{
				if((line[noDupColumns.indexOf(keyColumn)].equals(line2[noDupColumns.indexOf(keyColumn)]))&&
						(line[noDupColumns.indexOf("fileTime")].equals(line2[noDupColumns.indexOf("fileTime")])) 
						)
				{
					for(String	column:noDupColumns){
						if(line[noDupColumns.indexOf(column)]!=null){
							if(line2[noDupColumns.indexOf(column)]==null){
								line2[noDupColumns.indexOf(column)]=line[noDupColumns.indexOf(column)];
							}
							else
								if(!line[noDupColumns.indexOf(column)].equals(line2[noDupColumns.indexOf(column)])){
									if(!column.equals("upTime"))
									{
										System.out.println("WARNING: Conflicting field values for the same record from different input rows!");
										System.out.println("ROW1: fileTime="+line[noDupColumns.indexOf("fileTime")]+
												", "+keyColumn+"="+
												line[noDupColumns.indexOf(keyColumn)]+": "+column+"="+
												line[noDupColumns.indexOf(column)]);
										System.out.println("ROW2: fileTime="+line2[noDupColumns.indexOf("fileTime")]+
												", "+keyColumn+"="+
												line2[noDupColumns.indexOf(keyColumn)]+": "+column+"="+
												line2[noDupColumns.indexOf(column)]);
										System.out.println("Adding both...");
										line2[noDupColumns.indexOf(column)]=line2[noDupColumns.indexOf(column)]+"/"+
												line[noDupColumns.indexOf(column)];
									}
								}
						}
					}
					addNewLine=false;
				}
			}
			if(addNewLine){
				outTable2.add(line);
				if(!keys.contains(line[noDupColumns.indexOf(keyColumn)]))
					keys.add(line[noDupColumns.indexOf(keyColumn)]);
			}
		}
		
		for(String	key:keys)
		{
			long	filedate=0;
			String[]	latestLine=null;
			for(String[]	line:outTable2)
			{
				if(key.equals(line[noDupColumns.indexOf(keyColumn)]))
				{
					if(Long.parseLong(line[noDupColumns.indexOf("fileTime")])>=filedate)
					{
						filedate=Long.parseLong(line[noDupColumns.indexOf("fileTime")]);
						latestLine=line;
					}
				}
			}
			latestLine[noDupColumns.indexOf("latest")]="TRUE";
		}
		
		for(String[]	line:outTable2){
			for(String	item:line)
				out.write(item);
		}
		
		out.close();
	}
	
}
