import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.tcga.SpaceDistribution;


public class SpaceDistr {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Args: "+args.length);
		if(args.length<1)
		{
			System.out.println("Usage: SpaceDistribution <dirs-list-csv> [size][available][files <list-of-files-to-distrib-csv>][restrictions <list-of-available-space-restrictions-csv>]");
			System.exit(0);
		}
		LinkedList<String>	dirs=new LinkedList<String>();
		try {
			CSVParser	dirsCSV=new	CSVParser(args[0]);
			for(String[]	dir:dirsCSV)
			{
				dirs.add(dir[0]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR: could not read list of dirs to process from file "+args[0]);
			System.exit(1);
		}
		
		Hashtable<String,Long>	sizes=null;
		Hashtable<String,Long>	available=null;
		Hashtable<String,Long>	files=null;
		Hashtable<String,Long>	restrictions=null;
		boolean		showAvailable=false;
		int	i=0;
		for(String arg:args)
		{
			if(arg.equalsIgnoreCase("size")){
				sizes=SpaceDistribution.fsSize(dirs);
			}
			if(arg.equalsIgnoreCase("available")){
				showAvailable=true;
			}
			if(arg.equalsIgnoreCase("files")){
				try {
					CSVParser	filesCSV=new	CSVParser(args[i+1]);
					files=new	Hashtable<String,Long>();
					for(String[]	file:filesCSV)
					{
						files.put(file[0], new Long(file[1]));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			}
			if(arg.equalsIgnoreCase("restrictions")){
				try {
					CSVParser	restrictionsCSV=new	CSVParser(args[i+1]);
					restrictions=new	Hashtable<String,Long>();
					for(String[]	dir:restrictionsCSV)
					{
						restrictions.put(dir[0], new Long(dir[1]));
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			}
			i++;
		}
		if(showAvailable){
			if(restrictions!=null)
				available=SpaceDistribution.fsAvailable(dirs,restrictions);
			else
				available=SpaceDistribution.fsAvailable(dirs);
		}
		if(sizes!=null){
			System.out.println("dir\tsize");
			for(String	dir:sizes.keySet())
			{
				System.out.println(dir+"\t"+sizes.get(dir));
			}
			System.out.println();
		}
		if(available!=null){
			System.out.println("dir\tavailable");
			for(String	dir:available.keySet())
			{
				System.out.println(dir+"\t"+available.get(dir));
			}
			System.out.println();
		}
		if(files!=null){
			System.out.println("file\tpath");
			Hashtable<String,String>	fileDistr=SpaceDistribution.assignDestination(files, dirs, restrictions);
			for(String	file:fileDistr.keySet()){
				System.out.println(file+"\t"+fileDistr.get(file));
			}
		}
	}

}
