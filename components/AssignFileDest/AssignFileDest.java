import java.util.Hashtable;
import java.util.LinkedList;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.tcga.SpaceDistribution;


public class AssignFileDest extends SkeletonComponent{

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	AssignFileDest().run(args);
	}
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		CSVParser	destDirs=new	CSVParser(cf.getInput("destDirs"));
		CSVParser	files=new	CSVParser(cf.getInput("files"));
		CSVParser	restrictions=null;
		
		if(cf.getInput("restrictions")!=null)
		{
			restrictions=new	CSVParser(cf.getInput("restrictions"));
		}
		
		LinkedList<String>	dirsList=new	LinkedList<String>();
		
		for(String[]	row:destDirs)
		{
			dirsList.add(row[0]);
		}
		
		Hashtable<String,Long>	filesTable=new	Hashtable<String,Long>();
		
		for(String[]	row:files)
		{
			filesTable.put(row[0], new	Long(row[1]));
		}
		
		Hashtable<String,Long>	restrictionsTable=null;
		
		if(restrictions!=null){
			restrictionsTable=new	Hashtable<String,Long>();
			for(String[]	row:restrictions)
			{
				restrictionsTable.put(row[0], new	Long(row[1]));
			}
		}
		
		Hashtable<String,String>	fileDestsTable=SpaceDistribution.assignDestination(filesTable, dirsList, restrictionsTable);

		String[]	columns={"file","dest"};
		
		CSVWriter	fileDests=new	CSVWriter(columns,cf.getOutput("fileDests"),true);
		
		for(String	file:fileDestsTable.keySet())
		{
			fileDests.write(file);
			fileDests.write(fileDestsTable.get(file));
		}
		
		fileDests.close();
		
		return ErrorCode.OK;
	}

	/**
	 * @param args
	 */
	

}
