import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;

import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.tcga.ArchiveCategoryStatisticsNode;
import fi.helsinki.ltdk.csbl.tcga.CategoryType;
import fi.helsinki.ltdk.csbl.tcga.Codes2Titles;


public class Reporter {

	private	String	document;
	private	String	header,footer,body;
	private	String	date;
	private	String	mode;
	private	String	query;
	private	String	serialNumbers;
	private	String	revisions;
	private	String	project;
	private	String	analytesID,siteID,patientID,sampleID,portionID,plateID;
	private	byte[]	buffer=new	byte[4096];
	private	Hashtable<String,String>	cancers;
	private	Hashtable<String,String>	centers;
	private	Hashtable<String,String>	platforms;
	private	Hashtable<String,String>	dataTypes;
	private	Hashtable<String,Boolean>	selectedLevels;
	private	LinkedList<String>	newFiles;
	private	LinkedList<String>	updates;
	private	LinkedList<String>	imports;
	private	LinkedList<String>	orphans;
	private	Hashtable<String,String>	badMageTab;
	private	String	cancersDoc;
	private	String	centersDoc;
	private	String	platformsDoc;
	private	String	dataTypesDoc;
	private	String	samplesDoc;
	private	String	newDownloadsDoc;
	private	String	updatesDoc;
	private	String	importsDoc;
	private	String	orphansDoc;
	private	String	badMageTabDoc;
	private	File	reportFile;
	private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archives;
	
	Reporter(File	reportFile,String	remoteURL,String	mirrorDir,boolean	noOutput,boolean	syncAllFiles,boolean	browseOnly)
	{
		date=new	Date().toString();
		mode="<table class=\"oddeven\">\n"+
			"<tr class=\"odd\"><th>Remote site URL:</th><td>"+remoteURL+"</td></tr>\n"+
			(mirrorDir.length()>0?("<tr class=\"even\"><th>Mirror dir URL:</th><td>"+mirrorDir+"</td></tr>\n"):"")+
			"<tr class=\"odd\"><th>Enabled functions:</th><td>"+(browseOnly?"browse only (all other functions disabled)":
			(
			(noOutput?"":"data import")+
				(mirrorDir.length()>0?((noOutput?"":", ")+"mirror synchronization"+
						(syncAllFiles?"(update all files)":"")):"")
						))+
						"</td></tr>\n"+
			"</table>";
		header=footer=body="";
		InputStream in;
		try {
			in = new	FileInputStream("header.html");
		
			int	readBytes=0;
			do{
				readBytes=in.read(buffer);
				if(readBytes>0)
					header+=new	String(buffer).substring(0,readBytes);
			}while(readBytes>=1);
			in.close();
			
			in = new	FileInputStream("footer.html");
			readBytes=0;
			do{
				readBytes=in.read(buffer);
				if(readBytes>0)
					footer+=new	String(buffer).substring(0,readBytes);
			}while(readBytes>=1);
			in.close();
			
			in = new	FileInputStream("body.html");
			readBytes=0;
			do{
				readBytes=in.read(buffer);
				if(readBytes>0)
					body+=new	String(buffer).substring(0,readBytes);
			}while(readBytes>=1);
			in.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
		cancers=new	Hashtable<String,String>();
		centers=new	Hashtable<String,String>();
		platforms=new	Hashtable<String,String>();
		dataTypes=new	Hashtable<String,String>();
		newFiles=new	LinkedList<String>();
		updates=new	LinkedList<String>();
		imports=new	LinkedList<String>();
		orphans=new	LinkedList<String>();
		badMageTab=new	Hashtable<String,String>();
		reportFile.mkdirs();
		try {
			Tools.copyFile(new	File("style.css"), new	File(reportFile.getAbsolutePath()+File.separator+"style.css"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		this.reportFile=reportFile;
	}
	
	public	void	setLevels(Hashtable<String,Boolean>	selectedLevels)
	{
		this.selectedLevels=selectedLevels;
	}
	
	public	void	setSerialNumbers(String	serialNumbers)
	{
		this.serialNumbers=serialNumbers;
	}
	
	public	void	setRevisions(String	revisions)
	{
		this.revisions=revisions;
	}
	
	public	void	setProject(String	project)
	{
		this.project=project;
	}
	
	public	void	setSampleParams(String	analytesID,String	siteID,String	patientID,String	sampleID,
			String	portionID,String	plateID)
	{
		this.analytesID=analytesID;
		this.siteID=siteID;
		this.patientID=patientID;
		this.sampleID=sampleID;
		this.portionID=portionID;
		this.plateID=plateID;
	}
	
	public	void	setArchives(Hashtable<String,ArchiveCategoryStatisticsNode[]>	archives)
	{
		this.archives=archives;
	}
	
	public	void	addCancer(String	cancerCode)
	{
		cancers.put(cancerCode, Codes2Titles.cancerTitle(new	File(GetFromTcga.CANCER_TYPES_FILE), cancerCode));
	}
	
	public	void	addCenter(String	centerCode)
	{
		centers.put(centerCode, Codes2Titles.centerName(new	File(GetFromTcga.CENTER_IDS_FILE), centerCode));
	}
	
	public	void	addPlatform(String	platformCode)
	{
		platforms.put(platformCode, Codes2Titles.platformName(new	File(GetFromTcga.PLATFORM_TYPES_FILE), platformCode));
	}
	
	public	void	addDataType(String	dataType)
	{
		dataTypes.put(dataType, Codes2Titles.platformName(new	File(GetFromTcga.DATA_TYPES_FILE), dataType));
	}
	
	public	void	addNewFile(String	fileName)
	{
		newFiles.add(fileName);
	}
	
	public	void	addUpdate(String	fileName)
	{
		updates.add(fileName);
	}
	
	public	boolean	isInUpdate(String	fileName)
	{
		return	updates.contains(fileName);
	}
	
	public	void	addImport(String	fileName)
	{
		imports.add(fileName);
	}
	
	public	void	addOrphan(String	fileName)
	{
		orphans.add(fileName);
	}
	
	public	void	addBadMageTab(String	fileName,String	error)
	{
		badMageTab.put(fileName, error);
	}
	
	private	String	generateTableReport(CategoryType	category,Hashtable<String,String>	table,File	tableFile,File	docFile)
	{
		FileOutputStream out;
		String	doc="";
		try {
			 
			if(table.keySet().size()>0)
			{
				out = new	FileOutputStream(tableFile);
				doc="<table class=\"oddeven\">\n";
				boolean	isEven=false;
				for(String	key:table.keySet()){
					doc+="<tr class=\""+(isEven?"even":"odd")+"\"><td>"+key+"</td><td>"+table.get(key)+"</td></tr>\n";
					isEven=(!isEven);
					String	line=key+"\t"+table.get(key)+"\n";
					out.write(line.getBytes());
				}
				doc+="</table>\n";
				out.close();
			}
			else{
				try {
					switch(category){
					case	CANCER_TYPE:	doc="All cancers selected";
											Tools.copyFile(new	File(GetFromTcga.CANCER_TYPES_FILE), tableFile);
											break;
					case	CENTER:		doc="All centers selected";
											Tools.copyFile(new	File(GetFromTcga.CENTER_IDS_FILE), tableFile);
											break;
					case	PLATFORM:	doc="All platforms selected";
											Tools.copyFile(new	File(GetFromTcga.PLATFORM_TYPES_FILE), tableFile);
											break;
					case	DATA_TYPE:	doc="All data types selected";
											Tools.copyFile(new	File(GetFromTcga.DATA_TYPES_FILE), tableFile);
											break;
					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
		catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		try {
			out=new	FileOutputStream(docFile);
			out.write(doc.getBytes());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		return	doc;
	}
	
	private	void	generateCancers()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"cancers.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"cancers.html");
		cancersDoc=generateTableReport(CategoryType.CANCER_TYPE,cancers,tableFile,docFile);
	}
	
	private	void	generateCenters()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"centers.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"centers.html");
		centersDoc=generateTableReport(CategoryType.CENTER,centers,tableFile,docFile);
	}
	
	private	void	generatePlatforms()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"platforms.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"platforms.html");
		platformsDoc=generateTableReport(CategoryType.PLATFORM,platforms,tableFile,docFile);
	}
	
	private	void	generateDataTypes()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"dataTypes.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"dataTypes.html");
		dataTypesDoc=generateTableReport(CategoryType.DATA_TYPE,dataTypes,tableFile,docFile);
	}
	
	private	void	generateSamples()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"samples.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"samples.html");
		String	samplesTab="\t\n";
		samplesDoc="<table class=\"oddeven\">\n"+
			"<tr class=\"odd\"><th>Project: </th><td>"+project+"</td></tr>\n"+
			"<tr class=\"even\"><th>Data levels:</th><td>";
		String	levels="";
		for(String	level:selectedLevels.keySet())
		{
			if(selectedLevels.get(level))
				levels+=(levels.length()>0?", ":"")+level;
					
		}
		samplesDoc+=levels+"</td></tr>\n";
		samplesDoc+="<tr class=\"odd\"><th>Archive serial numbers:</th><td>"+serialNumbers+"</td></tr>\n";
		samplesDoc+="<tr class=\"even\"><th>Archive revisions:</th><td>"+revisions+"</td></tr>\n";
		samplesDoc+="<tr class=\"odd\"><th>Analytes: </th><td>"+analytesID+"</td></tr>\n";
		samplesDoc+="<tr class=\"even\"><th>Sites: </th><td>"+siteID+"</td></tr>\n";
		samplesDoc+="<tr class=\"odd\"><th>Patients: </th><td>"+patientID+"</td></tr>\n";
		samplesDoc+="<tr class=\"even\"><th>Samples: </th><td>"+sampleID+"</td></tr>\n";
		samplesDoc+="<tr class=\"odd\"><th>Portions: </th><td>"+portionID+"</td></tr>\n";
		samplesDoc+="<tr class=\"even\"><th>Samples: </th><td>"+plateID+"</td></tr>\n";
		samplesDoc+="</table>\n";
		samplesTab+="Data Levels\t"+levels+"\n";
		samplesTab+="Archive serial numbers\t"+serialNumbers+"\n";
		samplesTab+="Archive revisions\t"+revisions+"\n";
		samplesTab+="Analytes\t"+analytesID+"\n";
		samplesTab+="Sites\t"+siteID+"\n";
		samplesTab+="Patients\t"+patientID+"\n";
		samplesTab+="Samples\t"+sampleID+"\n";
		samplesTab+="Portions\t"+portionID+"\n";
		samplesTab+="Samples\t"+plateID+"\n";
	}

	private	String	generateTableReport(LinkedList<String>	list,File	tableFile,File	docFile)
	{
		String	doc=list.size()>0?"<table class=\"oddeven\">\n<tr><th>File</th><th>Cancer</th><th>Platform</th><th>DataType</th></tr>":
			"no files\n";
		String	table="File\tCancer\tPlatform\tDataType\n";
		boolean	isEven=false;
		for(String	line:list)
		{
			if(line==null)
				continue;
			ArchiveCategoryStatisticsNode[]	arcNode=archives.get(line);
			if(arcNode==null)
				continue;
			doc+="<tr class=\""+(isEven?"even":"odd")+"\"><td>"+line+"</td><td>"+arcNode[0].title+"</td><td>"+arcNode[3].title+
			"</td><td>"+arcNode[4].title+"</td></tr>\n";
			table+=line+"\t"+arcNode[0].title+"\t"+arcNode[3].title+"\t"+
			arcNode[4].title+"\n";
			isEven=(!isEven);
		}
		doc+="</table>\n";
		try {
			FileOutputStream	out=new	FileOutputStream(tableFile);
			out.write(table.getBytes());
			out.close();
			out=new	FileOutputStream(docFile);
			out.write(doc.getBytes());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		return	doc;
		
	}
	
	private	void	generateNewDownloads()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"new.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"new.html");
		newDownloadsDoc=generateTableReport(newFiles,tableFile,docFile);
	}
	
	private	void	generateUpdates()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"updates.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"updates.html");
		updatesDoc=generateTableReport(updates,tableFile,docFile);
	}
	
	private	void	generateImports()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"imports.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"imports.html");
		importsDoc=generateTableReport(imports,tableFile,docFile);
	}
	
	private	void	generateOrphans()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"orphans.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"orphans.html");
		
		orphansDoc="<table class=\"oddeven\">\n<tr><th>Archive</th><th>File</th></tr>\n";
		String	orphanTab="Archive\tFile\n";
		
		Collections.sort(orphans);
		String	last="";
		boolean	isEven=false;
		for(String	line:orphans)
		{
			String[]	orphan=AsserUtil.split(line, "\t");
			orphansDoc+="<tr class=\""+(isEven?"even":"odd")+"\"><td>";
			isEven=(!isEven);
			if(!orphan[0].equals(last))
				orphansDoc+=orphan[0];
			last=orphan[0];
			orphansDoc+="</td><td>"+orphan[1]+"</td></tr>\n";
			orphanTab+=orphan[0]+"\t"+orphan[1]+"\n";
		}
		orphansDoc+="</table>\n";
		try {
			FileOutputStream	out=new	FileOutputStream(tableFile);
			out.write(orphanTab.getBytes());
			out.close();
			out=new	FileOutputStream(docFile);
			out.write(orphansDoc.getBytes());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private	void	generateBadMageTab()
	{
		File	tableFile=new	File(reportFile.getAbsolutePath()+File.separator+"badMageTab.csv");
		File	docFile=new	File(reportFile.getAbsolutePath()+File.separator+"badMageTab.html");
		
		badMageTabDoc="<table class=\"oddeven\">\n<tr><th>File</th><th>Error</th></tr>\n";
		String	table="File\tError\n";
		boolean	isEven=false;
		for(String	file:badMageTab.keySet())
		{
			badMageTabDoc+="<tr class=\""+(isEven?"even":"odd")+"\"><td>"+file+"</td><td>"+badMageTab.get(file)+"</td></tr>\n";
			table+=file+"\t"+badMageTab.get(file)+"\n";
			isEven=(!isEven);
		}
		badMageTabDoc+="</table>\n";
		try {
			FileOutputStream	out=new	FileOutputStream(tableFile);
			out.write(table.getBytes());
			out.close();
			out=new	FileOutputStream(docFile);
			out.write(badMageTabDoc.getBytes());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	private	String	resolveMarkers(String	str)
	{
		str=str.replace("%date%", date);
		str=str.replace("%mode%", mode);
		str=str.replace("%cancers%", cancersDoc);
		str=str.replace("%centers%", centersDoc);
		str=str.replace("%platforms%", platformsDoc);
		str=str.replace("%dataTypes%", dataTypesDoc);
		str=str.replace("%samples%", samplesDoc);
		str=str.replace("%new_downloads%", newDownloadsDoc);
		str=str.replace("%updates%", updatesDoc);
		str=str.replace("%imports%", importsDoc);
		str=str.replace("%orphans%", orphansDoc);
		str=str.replace("%bad_mage_tab%", badMageTabDoc);
		return	str;
	}
	
	public	void	generateReport()
	{
		generateCancers();
		generateCenters();
		generatePlatforms();
		generateDataTypes();
		generateSamples();
		generateNewDownloads();
		generateUpdates();
		generateImports();
		generateOrphans();
		generateBadMageTab();
		header=resolveMarkers(header);
		footer=resolveMarkers(footer);
		body=resolveMarkers(body);
		document=header+"\n"+body+"\n"+footer;
		
		File	headerFile=new	File(reportFile.getAbsolutePath()+File.separator+"header.html");
		File	footerFile=new	File(reportFile.getAbsolutePath()+File.separator+"footer.html");
		File	documentFile=new	File(reportFile.getAbsolutePath()+File.separator+"index.html");
		try {
			FileOutputStream	out=new	FileOutputStream(headerFile);
			out.write(header.getBytes());
			out.close();
			out=new	FileOutputStream(footerFile);
			out.write(footer.getBytes());
			out.close();
			out=new	FileOutputStream(documentFile);
			out.write(document.getBytes());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
}
