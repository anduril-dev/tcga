

import java.io.ByteArrayInputStream;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.DigestInputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import fi.helsinki.ltdk.csbl.tcga.AuthUtils;


public class TCGAPasswd {

	/**
	 * @param args
	 */
	
	
	public	static	String	userName="";
	public	static	String	keyFileName="";
	public	static	String	fileName="";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Authentification file creator for accessing TCGA with Anduril component GetFromTCGA");
		String	passwd="";
		
		Key	key=null;
		
//		String[]	args2={"file1.bin", "file2.bin", "-u"};
		//args=args2;
		
		readArguments(args);
		if((keyFileName.length()<=0)||(fileName.length()<=0))
			return;
		if(userName.length()<=0){
			userName=readUserName();
		}
		passwd=readPasswd();
		
		key=AuthUtils.generateKey(new	File(keyFileName));
		AuthUtils.setAuth(new	File(fileName),key,userName,passwd);
//		Key	key2=AuthUtils.readKey(new File(keyFileName));
//		String	authStr=AuthUtils.getAuth(new File(fileName), key2);
//		System.out.println(authStr);
		
		String	keySuperDir=new	File(new	File(keyFileName).getAbsolutePath()).getParent();
		String	authSuperDir=new	File(new	File(fileName).getAbsolutePath()).getParent();
		if(keySuperDir.equals(authSuperDir)){
			System.out.println("WARNING: You are keeping both key and auth files in the SAME DIRECTORY "+authSuperDir+
					"! It is strongly recommended to RELOCATE one of them into another directory.");
		}
	}
	
	private static String readPasswd() {
		// TODO Auto-generated method stub
		Console	con=System.console();
		if(con==null)
		{
			System.out.println("Error: could not initialize console.\nFailed to create authentification file, exiting...");
			System.exit(1);
			return	"";
		}
		con.printf("Type in password: ");
		char[]	passArray=con.readPassword();
		con.printf("Retype the password: ");
		char[]	passArray2=con.readPassword();
		if(Arrays.equals(passArray, passArray2))
			return	new	String(passArray);
		System.out.println("Passwords did not much, try again.\nFailed to create authentification file");
		System.exit(1);
		return	"";
	}

	private static String readUserName() {
		// TODO Auto-generated method stub
		Console	con=System.console();
		if(con==null)
		{
			System.out.println("Error: could not initialize console.\nFailed to create authentification file, exiting...");
			System.exit(1);
			return	"";
		}
		con.printf("Username: ");
		return	con.readLine();
	}

	public	static	void	readArguments(String[]	args)
	{
		Hashtable<String,String>	params=new	Hashtable<String,String>();
		LinkedList<String>	argList=new	LinkedList<String>();
		
		for(int	i=0;i<args.length;i++){
			String	arg=args[i];
			if(arg.matches("-([a-z]|[A-Z])")){
				String	value=(i<args.length-1)?args[i+1]:"";
				if(value.matches("-([a-z]|[A-Z])"))
					value="";
				params.put(arg, value);
				if(value.length()>0)
					i++;
			}
			else
				argList.add(arg);
		}
		
		if(argList.size()!=2)
		{
			outputUsageHint();
			return;
		}
		fileName=argList.get(0);
		keyFileName=argList.get(1);
		if(params.get("-u")!=null)
			userName=params.get("-u");
	}
	
	private static void outputUsageHint() {
		// TODO Auto-generated method stub
		System.out.println("Usage: java TCGAPasswd <AuthFileName> <KeyFileName> [-u <UserName>]");
	}

	
	
/*	
	public	static	void	initPGP()
	{
		java.security.Security.addProvider(new cryptix.jce.provider.CryptixCrypto());
		java.security.Security.addProvider(new cryptix.openpgp.provider.CryptixOpenPGP());
	}
	
	public	static	PGPKeyBundle	generateKeyPair(String	passwd,String	pubKeyFileName,String	privKeyFileName)
	{
		SecureRandom	sr=new	SecureRandom();
		KeyBundleFactory	kbf=null;
		PGPKeyBundle	publicKey=null;
		PGPKeyBundle	privateKey=null;
		
		try {
			kbf=KeyBundleFactory.getInstance("OpenPGP");
			
			publicKey=(PGPKeyBundle) kbf.generateEmptyKeyBundle();
			privateKey=(PGPKeyBundle) kbf.generateEmptyKeyBundle();
			
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyBundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		KeyPairGenerator	keyPairGen=null;;
		try {
			keyPairGen=KeyPairGenerator.getInstance("OpenPGP/Signing/DSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		keyPairGen.initialize(1024,sr);
		KeyPair	keyPair=keyPairGen.generateKeyPair();
		PublicKey	pubkey=keyPair.getPublic();
		PrivateKey	privkey=keyPair.getPrivate();
		Principal	userid=null;
		
		try {
			PrincipalBuilder	pBuilder=PrincipalBuilder.getInstance("OpenPGP/UserID");
			userid=pBuilder.build("GetFromTCGA <tcga@domain.name>");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PrincipalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Certificate	cert=null;
		
		CertificateBuilder cBuilder;
		try {
			cBuilder = CertificateBuilder.getInstance("OpenPGP/Self");
			cert=cBuilder.build(pubkey, userid, privkey, sr);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			publicKey.addCertificate(cert);
			privateKey.addCertificate(cert);
		} catch (KeyBundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			privateKey.addPrivateKey(privkey, pubkey,passwd.toCharArray(),sr);
		} catch (KeyBundleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PGPArmouredMessage	armoured;
		
		armoured=new	PGPArmouredMessage(publicKey);
		try {
			FileOutputStream	out=new	FileOutputStream(pubKeyFileName);
			out.write(armoured.getEncoded());
			out.close();
			
			armoured=new	PGPArmouredMessage(privateKey);
			out=new	FileOutputStream(privKeyFileName);
			out.write(armoured.getEncoded());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return	publicKey;
	}
	
	public	static	void	EncodeSaveMessage(String	message,KeyBundle	publicKey,String	fileName)
	{
		LiteralMessage	litmsg=null;
		
		try {
			LiteralMessageBuilder	litMesBuilder=LiteralMessageBuilder.getInstance("OpenPGP");
			litMesBuilder.init(message);
			litmsg=(LiteralMessage) litMesBuilder.build();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Message	eMessage=null;
		try {
			EncryptedMessageBuilder	emb=EncryptedMessageBuilder.getInstance("OpenPGP");
			emb.init(litmsg);
			emb.addRecipient(publicKey);
			eMessage=emb.build();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PGPArmouredMessage	armoured=null;
		
		armoured=new	PGPArmouredMessage(eMessage);
		FileOutputStream out;
		try {
			out = new	FileOutputStream(fileName);
			out.write(armoured.getEncoded());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public	static	String	LoadDecodeMessage(String	fileName,String	privKeyFileName,String	passwd)
	{
		KeyBundle	privateKey=null;
		MessageFactory	mesFact=null;
		try {
			FileInputStream	in=new	FileInputStream(privKeyFileName);
			
			mesFact=MessageFactory.getInstance("OpenPGP");
			Collection<?>	msgs=mesFact.generateMessages(in);
			
			KeyBundleMessage	kbm=(KeyBundleMessage)msgs.iterator().next();
			
			privateKey=kbm.getKeyBundle();
			
			in.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		EncryptedMessage	eMessage=null;
		
		FileInputStream in;
		try {
			in = new	FileInputStream(fileName);
			
			Collection<?>	msgs=mesFact.generateMessages(in);
		
			eMessage=(EncryptedMessage)msgs.iterator().next();
		
			in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Message msg;
		try {
			msg = eMessage.decrypt(privateKey,passwd.toCharArray());
			return	((LiteralMessage)msg).getTextData();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotEncryptedToParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return	null;
	}*/

}
