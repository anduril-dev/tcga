import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.htmlparser.lexer.Lexer;
import org.htmlparser.util.ParserException;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.AsserUtil;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.javatools.utils.TextUtils;
import fi.helsinki.ltdk.csbl.tcga.ArchiveCategoryStatisticsNode;
import fi.helsinki.ltdk.csbl.tcga.Cache;
import fi.helsinki.ltdk.csbl.tcga.CategoryType;
import fi.helsinki.ltdk.csbl.tcga.Codes2Titles;
import fi.helsinki.ltdk.csbl.tcga.DirBrowser;
import fi.helsinki.ltdk.csbl.tcga.TarZip;


public class AliquotSelector {
	
public	static	final	String	ignoreFilePattern="((.*!)|DESCRIPTION.txt|MANIFEST.txt|README_DCC.txt|" +
		"README.txt|CHANGES_DCC.txt|(.*\\.idf\\.txt)|(.*\\.sdrf\\.txt)|(.*\\.adf\\.txt)|" +
		"(.*\\.beta-value\\.txt)|(.*\\.cy3-cy5-value\\.txt)|(.*\\.detection-p-value\\.txt)|" +
		"rename.sh|(.*\\.probe\\.matrix\\.txt)|DCC_ALTERED_FILES.txt|(.*_ver1_1\\.xsd)|" +
		"(.*\\.idf\\.hla\\.txt)|(.*\\.sdrf\\.hla\\.txt)|(.*_Batch1-4_roi\\.txt))";

public	static	final	String	ignoreArchivePattern="((.*!)|DESCRIPTION.txt|MANIFEST.txt|README_DCC.txt|" +
"README.txt|CHANGES_DCC.txt|rename.sh|DCC_ALTERED_FILES.txt|(.*\\.manifest)|(.*\\.md5))";

public	static	final	String	mageTabTemplateExclude=".*\\.hla\\.sdrf\\.txt";
public	static	final	String	mageTabTemplate=".*\\.sdrf\\.txt";
	
private	LinkedList<String>	cancerDirs;
private	Hashtable<String,ArchiveCategoryStatisticsNode>	cancerNodes;
private	LinkedList<String>	centerTypeDirs;
//private	Hashtable<String,ArchiveCategoryStatisticsNode>	centerTypeNodes;
private	LinkedList<String>	centerDirs;
private	Hashtable<String,ArchiveCategoryStatisticsNode>	centerNodes;
private	LinkedList<String>	platformDirs;
private	Hashtable<String,ArchiveCategoryStatisticsNode>	platformNodes;
private	LinkedList<String>	dataTypeDirs;
private	Hashtable<String,ArchiveCategoryStatisticsNode>	dataTypeNodes;
private	Date[]	dateIntervals;
private	int	dateIntervalsNum;
private	int	dataLevel;
private	String	projectName;
private	LinkedList<String>	archiveFileNames;
private	Hashtable<String,ArchiveCategoryStatisticsNode>	archiveNodes;

private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archives;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	dataFiles;
private	Hashtable<String,LinkedList<String>>	archives2Aliquots;

private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	filteredArchives;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesLevel1;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesLevel2;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesLevel3;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesLevel4;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesMageTab;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesAux;
private	Hashtable<String,ArchiveCategoryStatisticsNode[]>	archivesMisc;

private	String	aliquotsIDPattern;
private	Hashtable<String,Boolean>	selectedLevels;

private	Hashtable<String,LinkedList<String>>	aliquotFiles;
private	LinkedList<String>	selectedAliquots;
private	Hashtable<String,LinkedList<String>>	files2Unpack;
private	Hashtable<String,Integer>	filesLevelfromMageTab;
private	LinkedList<String>	filesLevelUnknown;
private	LinkedList<String>	filesLevel1;
private	LinkedList<String>	filesLevel2;
private	LinkedList<String>	filesLevel3;
private	LinkedList<String>	filesLevel4;
private	LinkedList<String>	filesAux;
private	LinkedList<String>	filesMisc;
private	LinkedList<String>	outputFiles;

//private	MyOutput	myOutput;
private	Cache	cache;

private	String	userName;
private	String	passwd;

private	File	levelFiles;
private	File	auxFiles;
private	File	miscFiles;
private	File	readmeDir;
private	File	readmeDCCDir;
private	File	descriptionDir;
private	IndexFile	readme;
private	IndexFile	readme_dcc;
private	IndexFile	description;


private	boolean	unpackReadme;
private	boolean	importOrphans;

private	boolean	invertPlatformSelection;
private	boolean	invertDataType;

private	Reporter	reporter;

private	Map<String,String>	aliquotQuery;
private	Map<String,String>	archiveQuery;
private	LinkedList<String>	aliquotList;

private	CommandFile	cf;

private	boolean	debug;

	AliquotSelector(String	remoteSiteURL,String	userName,String	passwd,boolean	importOrphans,String	cancerType,String	platformType,
			boolean	invertPlatformSelection,
			String	dataType,boolean	invertDataType,String	submissionDate,String	serialNumber,String	revision,String	levelType,String	analytesID,String	projectName,
			String	siteID,String	patientID,String	sampleID,String	portionID,
			String	plateID,String	centerID,
			File	aliquotsQuery,File	 archivesQuery,File	 aliquots,
			File	cancerFile,File	centerFile,File	platformFile,File	dataFile,
			IndexFile readme, IndexFile readmeDcc, IndexFile description,
			File	dataFilesDir,
			File readmeDir, File readmeDCCDir, File descriptionDir, Cache	cache,Reporter	reporter, 
			boolean unpackReadme,CommandFile cf, boolean debug) throws IOException
	{
		this.cf=cf;
		this.reporter=reporter;
//		this.myOutput=myOutput;
		this.cache=cache;
		this.projectName=projectName;
		this.userName=userName;
		this.passwd=passwd;
		remoteSiteURL=remoteSiteURL.replace("file:///", "file:/");
		this.importOrphans=importOrphans;
		this.invertPlatformSelection=invertPlatformSelection;
		this.invertDataType=invertDataType;
		this.readmeDir=readmeDir;
		this.readmeDCCDir=readmeDCCDir;
		this.descriptionDir=descriptionDir;
		this.readme=readme;
		this.readme_dcc=readmeDcc;
		this.description=description;
		this.unpackReadme=unpackReadme;
		this.debug=debug;
		if(aliquotsQuery!=null)
			aliquotQuery=parseProperties(aliquotsQuery);
		else
			aliquotQuery=new	Hashtable<String,String>();
		if(archivesQuery!=null)
			archiveQuery=parseProperties(archivesQuery);
		else
			archiveQuery=new	Hashtable<String,String>();
		if(!analytesID.equals(".*"))
			aliquotQuery.put("aliquotsID", analytesID);
		if(!projectName.equals("TCGA"))
			aliquotQuery.put("projectName", projectName);
		if(siteID.length()>0)
			aliquotQuery.put("siteID", siteID);
		if(patientID.length()>0)
			aliquotQuery.put("patientID", patientID);
		if(sampleID.length()>0)
			aliquotQuery.put("sampleID", sampleID);
		if(portionID.length()>0)
			aliquotQuery.put("portionID", portionID);
		if(plateID.length()>0)
			aliquotQuery.put("plateID", plateID);
		if(centerID.length()>0)
			aliquotQuery.put("centerID", centerID);
		
		if(aliquotQuery.containsKey("aliquotsID"))
			analytesID=aliquotQuery.get("aliquotsID");
		if(aliquotQuery.containsKey("projectName"))
			projectName=aliquotQuery.get("projectName");
		if(aliquotQuery.containsKey("siteID"))
			siteID=aliquotQuery.get("siteID");
		if(aliquotQuery.containsKey("patientID"))
			patientID=aliquotQuery.get("patientID");
		if(aliquotQuery.containsKey("sampleID"))
			sampleID=aliquotQuery.get("sampleID");
		if(aliquotQuery.containsKey("portionID"))
			portionID=aliquotQuery.get("portionID");
		if(aliquotQuery.containsKey("plateID"))
			plateID=aliquotQuery.get("plateID");
		if(aliquotQuery.containsKey("centerID"))
			centerID=aliquotQuery.get("centerID");
		
		if(serialNumber.length()>0)
			archiveQuery.put("serialNumber", serialNumber);
		if(revision.length()>0)
			archiveQuery.put("outputRevision", revision);
		if(debug)System.out.println(cancerType);
		if(cancerType.length()>0)
			archiveQuery.put("cancerType", cancerType);
		if(platformType.length()>0)
			archiveQuery.put("platformType", platformType);
		if(dataType.length()>0)
			archiveQuery.put("dataType", dataType);
		if(submissionDate.length()>0)
			archiveQuery.put("submissionDate", submissionDate);
		if(!levelType.equals("all"))
			archiveQuery.put("levelType", levelType);
		
		if(archiveQuery.containsKey("serialNumber"))
			serialNumber=archiveQuery.get("serialNumber");
		if(archiveQuery.containsKey("outputRevision"))
			revision=archiveQuery.get("outputRevision");
		if(archiveQuery.containsKey("cancerType"))
			cancerType=archiveQuery.get("cancerType");
		if(archiveQuery.containsKey("platformType"))
			platformType=archiveQuery.get("platformType");
		if(archiveQuery.containsKey("dataType"))
			dataType=archiveQuery.get("dataType");
		if(archiveQuery.containsKey("submissionDate"))
			submissionDate=archiveQuery.get("submissionDate");
		if(archiveQuery.containsKey("levelType"))
			levelType=archiveQuery.get("levelType");
		
		if(debug)for(String	key:archiveQuery.keySet())
			System.out.println(key+"\t=\t"+archiveQuery.get(key));
		aliquotList=new	LinkedList<String>();
		if(aliquots!=null)
		{
			CSVParser	aliquotsLines=new	CSVParser(aliquots);
			for(String[]	line:aliquotsLines)
				aliquotList.add(line[0]);
		}
		if(DirBrowser.fileAccessible(remoteSiteURL,userName,passwd)){
			archives=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			reporter.setArchives(archives);
			dataFiles=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archives2Aliquots=new	Hashtable<String,LinkedList<String>>();
			filteredArchives=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesLevel1=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesLevel2=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesLevel3=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesLevel4=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesMageTab=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesAux=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			archivesMisc=new Hashtable<String,ArchiveCategoryStatisticsNode[]>();
			aliquotFiles=new	Hashtable<String,LinkedList<String>>();
			selectedAliquots=new	LinkedList<String>();
			files2Unpack=new	Hashtable<String,LinkedList<String>>();
			filesLevelfromMageTab=new	Hashtable<String,Integer>();
			filesLevelUnknown=new	LinkedList<String>();
			filesLevel1=new	LinkedList<String>();
			filesLevel2=new	LinkedList<String>();
			filesLevel3=new	LinkedList<String>();
			filesLevel4=new	LinkedList<String>();
			filesAux=new	LinkedList<String>();
			filesMisc=new	LinkedList<String>();
			outputFiles=new	LinkedList<String>();
			selectedLevels=new	Hashtable<String,Boolean>();
			this.levelFiles=dataFilesDir;
			this.auxFiles=dataFilesDir;
			this.miscFiles=dataFilesDir;
			System.out.println("Reading the directory structure of the database. This may take several minutes...");
			validateCancerTypes(cancerType, remoteSiteURL, cancerFile);
			validateCenterTypes();
			validateCenters(centerID,centerFile);
			validatePlatformTypes(platformType,platformFile);
			validateDataTypes(dataType,dataFile);
			System.out.println("Done");
			aliquotsIDPattern=createAliquotPattern(analytesID,projectName,siteID,
					patientID,sampleID,portionID,plateID,centerID,aliquotList);
			if(debug)System.out.println(aliquotsIDPattern);
			selectDataLevels(levelType);
			System.out.println("Selecting files for download. This may take several minutes...");
			selectFilesOfInterest(serialNumber,revision,levelType,projectName);
			System.out.println("Done");
			System.out.println("Selecting files to unpack...");
			selectFiles2Unpack();
			System.out.println("Done");
			System.out.println("Checking for orphaned files...");
			checkOrphanFiles();
			System.out.println("Done");
//			aliquotsIDPattern=getSamplesPattern(samplesID,projectName,siteID,patientID,sampleID,portionID,plateID,centerID);
		} else {
			// TODO Auto-generated catch block
			cf.writeError("Failed to open remote database site "+remoteSiteURL);
			System.exit(1);
		}
	}

private	Hashtable<String,String>	parseProperties(File	inFile)
{
	byte[]	buffer=new	byte[1024*1024];
	String	properties="";
	Hashtable<String,String>	map=new	Hashtable<String,String>();
	
	if(inFile!=null)
	{
		FileInputStream in;
		try {
			in = new	FileInputStream(inFile);
			int	readBytes=0;
			do{
				readBytes=in.read(buffer);
				if(readBytes>0)
					properties+=new	String(buffer).substring(0,readBytes);
			}while(readBytes>=1);
			in.close();
		
			String[]	lines=AsserUtil.split(properties,"\n");
			for(String	line:lines)
			{
				String[]	keyValue=AsserUtil.split(line.replace(" ", "").replace("\t", "").replace("\n", ""), "=");
				map.put(keyValue[0], keyValue[1]);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
	return	map;
}
	
private void selectDataLevels(String levelType) {
		// TODO Auto-generated method stub
		String[]	levels=AsserUtil.split(levelType);
		boolean	allSelected=false;
		
		if(levels==null)
			allSelected=true;
		else
			if(levels.length==0)
				allSelected=true;
			else
				if(levels[0].equals("all"))
					allSelected=true;
		if(allSelected){
			selectedLevels.put("level1", true);
			selectedLevels.put("level2", true);
			selectedLevels.put("level3", true);
			selectedLevels.put("level4", true);
			selectedLevels.put("aux", true);
			selectedLevels.put("misc", true);
			selectedLevels.put("unknown", true);
		}
		else
		{
			LinkedList<String>	levelList=new	LinkedList<String>();
			for(String	level:levels)
				levelList.add(level);
			if(levelList.contains("unknown"))
				selectedLevels.put("unknown", true);
			else
				selectedLevels.put("unknown", false);
			if(levelList.contains("level1"))
				selectedLevels.put("level1", true);
			else
				selectedLevels.put("level1", false);
			if(levelList.contains("level2"))
				selectedLevels.put("level2", true);
			else
				selectedLevels.put("level2", false);
			if(levelList.contains("level3"))
				selectedLevels.put("level3", true);
			else
				selectedLevels.put("level3", false);
			if(levelList.contains("level4"))
				selectedLevels.put("level4", true);
			else
				selectedLevels.put("level4", false);
			if(levelList.contains("aux"))
				selectedLevels.put("aux", true);
			else
				selectedLevels.put("aux", false);
			if(levelList.contains("misc"))
				selectedLevels.put("misc", true);
			else
				selectedLevels.put("misc", false);
		}
		reporter.setLevels(selectedLevels);
	}

public	void	downloadUnpackFiles()
{
	downloadUnpackFiles(null, null);

}

public	void	downloadUnpackFiles(String remoteSite, String mirrorDir)
{
	System.out.println("Initiating downloading/unpacking process. This may take a while...");
	for(String	file:files2Unpack.keySet()){
		boolean	isFile=thisIsFile(file);
		String	redirectedFile;
		InputStream	in;
		if((remoteSite!=null)&(mirrorDir!=null))
		{
			redirectedFile=file.replace(remoteSite, mirrorDir);
			in=isFile?cache.readFileFromMirrorURL(redirectedFile):
				null;
		}
		else
			{
				redirectedFile=file;
				in=isFile?cache.readFileFromURL(file):
					null;
			}
		System.out.println("Unpacking "+redirectedFile);
		String	dest;
		if((archivesLevel1.keySet().contains(file))||(archivesLevel2.keySet().contains(file))||
				(archivesLevel3.keySet().contains(file))||(archivesLevel4.keySet().contains(file)))
			dest=levelFiles.getAbsolutePath()+"/";
		else
			if(archivesAux.keySet().contains(file))
				dest=auxFiles.getAbsolutePath()+"/";
			else
				dest=miscFiles.getAbsolutePath()+"/";
		LinkedList<String>	fileList=files2Unpack.get(file);
//		String	internalPath=new	File(file).getName();
//		internalPath=internalPath.substring(0,internalPath.length()-7);
		if(isFile)
			TarZip.unpack(in, new	File(file).getName(), "file:/"+dest, fileList);
		else
			try {
				copyFiles(new	URL(file).getFile(), dest, fileList);
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.exit(1);
			}
		if(GetFromTcga.standAlone)
			System.out.println();
		LinkedList<String>	suppFileList=new	LinkedList<String>();
		
//		String	dir=file.substring(0, file.length()-7);
		if((unpackReadme)&&((file.matches(".*\\.tar\\.gz"))||(file.equalsIgnoreCase("readme.txt"))
				||(file.equalsIgnoreCase("readme_dcc.txt"))||(file.equalsIgnoreCase("description.txt")))){
			String	arcCode=(file.matches(".*\\.tar\\.gz"))? 
				new	File(file.substring(0,file.length()-7)).getName():
					Integer.toHexString(new	File(file).hashCode());
			suppFileList.add("README.txt");
			if(isFile)
				in=((remoteSite!=null)&(mirrorDir!=null))?
						cache.readFileFromMirrorURL(redirectedFile):
							cache.readFileFromURL(redirectedFile);
						else
							in=null;
			if(isFile)
				TarZip.unpack(in, new	File(file).getName(), "file:/"+readmeDir+File.separator+arcCode, suppFileList);
			else
				try {
					copyFiles(new	URL(file).getFile(),readmeDir+File.separator+arcCode,suppFileList);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.exit(1);
				}
			try {
				if(in!=null)
					in.close();
			} catch (IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			suppFileList.clear();
			if(DirBrowser.fileAccessible("file://"+readmeDir+File.separator+arcCode+File.separator+"README.txt",userName,passwd))
				readme.add(arcCode, new	File(readmeDir+File.separator+arcCode+File.separator+"README.txt"));
			
			suppFileList.add("README_DCC.txt");
			if(isFile)
				in=((remoteSite!=null)&(mirrorDir!=null))?
						cache.readFileFromMirrorURL(redirectedFile):
							cache.readFileFromURL(redirectedFile);
						else
							in=null;
			if(isFile)
				TarZip.unpack(in, new	File(file).getName(), "file:/"+readmeDCCDir+File.separator+arcCode, suppFileList);
			else
				try {
					copyFiles(new	URL(file).getFile(),readmeDCCDir+File.separator+arcCode, suppFileList);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.exit(1);
				}
			try {
				if(in!=null)
					in.close();
			} catch (IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			suppFileList.clear();
			if(debug)System.out.println("file:/"+readmeDCCDir+File.separator+arcCode+File.separator+"README_DCC.txt");
			if(DirBrowser.fileAccessible("file://"+readmeDCCDir+File.separator+arcCode+File.separator+"README_DCC.txt",userName,passwd))
				readme_dcc.add(arcCode, new	File(readmeDCCDir+File.separator+arcCode+File.separator+"README_DCC.txt"));
			
			suppFileList.add("DESCRIPTION.txt");
			if(isFile)
				in=((remoteSite!=null)&(mirrorDir!=null))?
						cache.readFileFromMirrorURL(redirectedFile):
							cache.readFileFromURL(redirectedFile);
						else
							in=null;
			if(isFile)
				TarZip.unpack(in, new	File(file).getName(), "file:/"+descriptionDir+File.separator+arcCode, suppFileList);
			else
				try {
					copyFiles(new	URL(file).getFile(), descriptionDir+File.separator+arcCode, suppFileList);
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.exit(1);
				}
			try {
				if(in!=null)
					in.close();
			} catch (IOException e) {
			// TODO Auto-generated catch block
				e.printStackTrace();
			}
			suppFileList.clear();
			if(DirBrowser.fileAccessible("file://"+descriptionDir+File.separator+arcCode+File.separator+"DESCRIPTION.txt",userName,passwd))
				description.add(arcCode, new	File(descriptionDir+File.separator+arcCode+File.separator+"DESCRIPTION.txt"));
		}
	}
	try {
		readme.write(cf,"readme");
		readme_dcc.write(cf,"readme_dcc");
		description.write(cf,"description");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.exit(1);
	}
	
	System.out.println("Done");
}

private void copyFiles(String name, String dest, LinkedList<String> fileList) {
	// TODO Auto-generated method stub
	new	File(dest).mkdirs();
	for(String	file:fileList)
	{
		try {
			Tools.copyFile(new	File(name+File.separator+file), new	File(dest+File.separator+file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
}

private boolean thisIsFile(String file) {
	// TODO Auto-generated method stub
	
	try {
		URL	url=new	URL(file);
		if(url.getProtocol().equalsIgnoreCase("http")||url.getProtocol().equalsIgnoreCase("https"))
			return	true;
		if(url.getProtocol().equalsIgnoreCase("file"))
		{
			return	!new	File(url.getFile()).isDirectory();
		}
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.exit(1);
	}
	
	return true;
}

public	Set<String>	getFiles2Download()
{
	return	files2Unpack.keySet();
}
	
private void checkOrphanFiles() {
		// TODO Auto-generated method stub
	for(String	arcFile:archives2Aliquots.keySet()){
		LinkedList<String>	aliquots=archives2Aliquots.get(arcFile);
		String	arcDir=arcFile.matches(".*\\.tar\\.gz")?
			arcFile.substring(0,arcFile.length()-7):
				arcFile;
		LinkedList<String>	fileList=new	LinkedList<String>();
		String	cacheMirrorUrl=null;
		if(cache.getMirrorUrl()!=null)
			cacheMirrorUrl=arcFile.replace(cache.getBaseUrl().toExternalForm(), cache.getMirrorUrl().toExternalForm());
		if(DirBrowser.fileAccessible(arcFile+".manifest", userName, passwd)||
				DirBrowser.fileAccessible(cacheMirrorUrl+".manifest", userName, passwd)){
			fileList=DirBrowser.readManifest(cache.readFileFromURL(arcFile+".manifest"), arcFile, 
					cacheMirrorUrl, cache,userName,passwd);
		}
		if(DirBrowser.fileAccessible(arcDir,userName,passwd)){
			DirBrowser	browser=new	DirBrowser(arcDir,userName,passwd);
			fileList.addAll(browser.browseFilesOnly());
		}
		else
			if(DirBrowser.fileAccessible(arcFile,userName,passwd)){
				if(!DirBrowser.fileAccessible(arcFile+".manifest", userName, passwd)&&
						!DirBrowser.fileAccessible(cacheMirrorUrl+".manifest", userName, passwd)){
					fileList=TarZip.browse(cache.readFileFromURL(arcFile));
					DirBrowser.generateManifest(arcFile,
							cacheMirrorUrl,fileList,userName,passwd);
					}
			}
			else
				fileList=new	LinkedList<String>();
		boolean	dirContainsOrphans=false;
		if(fileList!=null)
			for(String	file:fileList)
			{
				String	arcDirName=new	File(arcFile).getName();
				arcDirName=arcDirName.matches(".*\\.tar\\.gz")?
						arcDirName.substring(0,arcDirName.length()-7):
							arcDirName;
				if((!file.matches(ignoreFilePattern))&&(!file.equals(arcDirName))){
					boolean	aliquotFound=false;
					for(String	aliquot:aliquots){
						LinkedList<String>	aliquotFileList=aliquotFiles.get(aliquot);
						if(aliquotFileList.contains(file)){
							if(debug)System.out.println("checkOrphanFiles: "+aliquot+" - "+file);
							aliquotFound=true;
							break;
							}
					}
//					if(!aliquotFound)
					if(false)
					{
						String	aliquot=guessAliquot(file,projectName);
						if(aliquot!=null)
						{
							
							
							if(debug)System.out.println("Guessed aliquot: "+aliquot);
							//LinkedList<String>	fileList;
							if(!aliquotFiles.keySet().contains(aliquot))
							{
								fileList=new	LinkedList<String>();
							}
							else
								fileList=aliquotFiles.get(aliquot);
							if(!fileList.contains(file))
								fileList.add(file);
//							if(debug)System.out.println("### ---"+aliquot+"\t--->\t"+aliquotFile);
							aliquotFiles.put(aliquot, fileList);
							if(aliquot.matches(aliquotsIDPattern)){
								if(debug)System.out.println("Matched aliquot: "+aliquot);
								if(!selectedAliquots.contains(aliquot))
									selectedAliquots.add(aliquot);
								if(!outputFiles.contains(file))
									outputFiles.add(file);
								ArchiveCategoryStatisticsNode[]	vector2=archives.get(arcFile);
								ArchiveCategoryStatisticsNode[]	vector=new	ArchiveCategoryStatisticsNode[7];
								for(int	i=0;i<6;i++)
									vector[i]=vector2[i];
								vector[6]=new	ArchiveCategoryStatisticsNode(file,file,vector2[5].dataLevel,
								CategoryType.DATA_FILE, arcDir+"/"+file);
								dataFiles.put(arcDir+"/"+file, vector);
								LinkedList<String>	fileList2;
								if(files2Unpack.containsKey(arcFile))
								{
									fileList2=files2Unpack.get(arcFile);
									fileList2.addAll(fileList);
								}
								else
									fileList2=fileList;
								files2Unpack.put(arcFile, fileList2);
							}
							
							aliquotFound=true;
						}
						else
							aliquotFound=false;
					}
					if(!aliquotFound)
					{
						if(!dirContainsOrphans)
						{
							dirContainsOrphans=true;
							System.out.println("Warning: In "+arcFile+":");
						}
						reporter.addOrphan(arcFile+"\t"+file);
						System.out.println("\t found orphan file "+file);
						if(importOrphans){
							LinkedList<String>	orphFiles=files2Unpack.get(arcFile);
							if(orphFiles==null)
								orphFiles=new	LinkedList<String>();
							if(!orphFiles.contains(file))
								orphFiles.add(file);
					
							ArchiveCategoryStatisticsNode[]	vector2=archives.get(arcFile);
							ArchiveCategoryStatisticsNode[]	vector=new	ArchiveCategoryStatisticsNode[7];
							for(int	i=0;i<6;i++)
								vector[i]=vector2[i];
							vector[6]=new	ArchiveCategoryStatisticsNode(file,file,vector2[5].dataLevel,
							CategoryType.DATA_FILE, arcDir+"/"+file);
							dataFiles.put(arcDir+"/"+file, vector);
							files2Unpack.put(arcFile, orphFiles);
						}
					}
					
				}
			}
		if(GetFromTcga.standAlone)
			System.out.print(".");
		System.out.flush();
	}
	if(GetFromTcga.standAlone)
		System.out.println();
	}

private void selectFiles2Unpack() {
		// TODO Auto-generated method stub
		for(String	arcFile:archives2Aliquots.keySet()){
			if(debug)System.out.println("Selecting for unpacking: "+arcFile);
			LinkedList<String>	aliquots=archives2Aliquots.get(arcFile);
			LinkedList<String>	fileList=new	LinkedList<String>();
			String	arcDir=arcFile.matches(".*\\.tar\\.gz")?
					arcFile.substring(0,arcFile.length()-7):
						arcFile;
			if(debug)System.out.println("arcDir="+arcDir);
			LinkedList<String>	browsedFiles=new	LinkedList<String>();
			String	cacheMirrorUrl=null;
			if(cache.getMirrorUrl()!=null)
				cacheMirrorUrl=arcFile.replace(cache.getBaseUrl().toExternalForm(), cache.getMirrorUrl().toExternalForm());
			if(DirBrowser.fileAccessible(arcFile+".manifest", userName, passwd)||
					DirBrowser.fileAccessible(cacheMirrorUrl+".manifest", 
							userName, passwd)){
				browsedFiles=DirBrowser.readManifest(cache.readFileFromURL(arcFile+".manifest"), arcFile, 
						cacheMirrorUrl,cache,userName,passwd);
			}
			if(DirBrowser.fileAccessible(arcDir,userName,passwd)){
				DirBrowser	browser=new	DirBrowser(arcDir,userName,passwd);
				browsedFiles.addAll(browser.browseFilesOnly());
				if(debug)System.out.println("Dir accessible: "+arcDir);
			}
			else
				if(DirBrowser.fileAccessible(arcDir+".tar.gz",userName,passwd)){
					if(!DirBrowser.fileAccessible(arcFile+".manifest", userName, passwd)&&
							!DirBrowser.fileAccessible(cacheMirrorUrl+".manifest", 
									userName, passwd)){
						browsedFiles=TarZip.browse(cache.readFileFromURL(arcFile));
						DirBrowser.generateManifest(arcFile,
								cacheMirrorUrl,browsedFiles,userName,passwd);
						}
				}
				else
				browsedFiles=new	LinkedList<String>();
			if(debug)System.out.println("--- "+arcDir);
			int	level=0;
			if(filteredArchives.keySet().contains(arcFile)){
				filesLevelUnknown.addAll(fileList);
				level=0;
			}
			if(archivesLevel1.keySet().contains(arcFile)){
				filesLevel1.addAll(fileList);
				level=1;
			}
			if(archivesLevel2.keySet().contains(arcFile)){
				filesLevel2.addAll(fileList);
				level=2;
			}
			if(archivesLevel3.keySet().contains(arcFile)){
				filesLevel3.addAll(fileList);
				level=3;
			}
			if(archivesLevel4.keySet().contains(arcFile)){
				filesLevel4.addAll(fileList);
				level=4;
			}
			if(archivesAux.keySet().contains(arcFile)){
				filesAux.addAll(fileList);
				level=6;
			}
			if(archivesMisc.keySet().contains(arcFile)){
				filesMisc.addAll(fileList);
				level=0;
			}
			for(String	aliquot:aliquots){
				if(debug)System.out.println("Checking aliquot: "+aliquot);
				if(selectedAliquots.contains(aliquot)){
					
					if(debug)System.out.println("Selected aliquot: "+aliquot+":");
					LinkedList<String>	aliquotFileList=aliquotFiles.get(aliquot);
					for(String	aliquotFile:aliquotFileList){
						if(debug)System.out.println("\t"+aliquotFile);
						int	local_level=level;
						if((aliquotFile.toLowerCase().matches(".*\\.lvl-1\\..*"))||(aliquotFile.toLowerCase().matches(".*\\.Level_1\\..*")))
							local_level=1;
						if((aliquotFile.toLowerCase().matches(".*\\.lvl-2\\..*"))||(aliquotFile.toLowerCase().matches(".*\\.Level_2\\..*")))
							local_level=2;
						if((aliquotFile.toLowerCase().matches(".*\\.lvl-3\\..*"))||(aliquotFile.toLowerCase().matches(".*\\.Level_3\\..*")))
							local_level=3;
						if((aliquotFile.toLowerCase().matches(".*\\.lvl-4\\..*"))||(aliquotFile.toLowerCase().matches(".*\\.Level_4\\..*")))
							local_level=4;
						if((browsedFiles.contains(aliquotFile))&&(filesLevelfromMageTab.keySet().contains(aliquotFile))){
							if(local_level>0){
								if(filesLevelfromMageTab.get(aliquotFile)!=local_level){
									System.out.println("Warning: data level derived from the file name does not match with the data level derived from the mage table for file "+aliquotFile+" in "+arcFile+
											"\nValues "+local_level+" and "+filesLevelfromMageTab.get(aliquotFile)+" do not match!");
									reporter.addBadMageTab(arcFile+File.separator+aliquotFile, 
											"filename-based-level="+local_level+", mage-tab-level="+
												filesLevelfromMageTab.get(aliquotFile));
								}
							}
							else
								local_level=filesLevelfromMageTab.get(aliquotFile);
							
						}
						boolean	unpack=false;
						switch(local_level){
						case	1:unpack=selectedLevels.get("level1");
								break;
						case	2:unpack=selectedLevels.get("level2");
								break;
						case	3:unpack=selectedLevels.get("level3");
								break;
						case	4:unpack=selectedLevels.get("level4");
								break;
						case	0:unpack=selectedLevels.get("unknown");
						break;
						}
						if(debug)System.out.println("Unpack="+unpack);
						if(debug)System.out.println("selectFiles2Unpack: aliquotFile="+aliquotFile);
						if(unpack)
							if((browsedFiles.contains(aliquotFile))&&(!fileList.contains(aliquotFile))){
								if(!outputFiles.contains(aliquotFile))
									outputFiles.add(aliquotFile);
								if(!fileList.contains(aliquotFile))
									fileList.add(aliquotFile);
								ArchiveCategoryStatisticsNode[]	vector2=archives.get(arcFile);
								ArchiveCategoryStatisticsNode[]	vector=new	ArchiveCategoryStatisticsNode[7];
								for(int	i=0;i<6;i++)
									vector[i]=vector2[i];
								vector[6]=new	ArchiveCategoryStatisticsNode(aliquotFile,aliquotFile,local_level,
										CategoryType.DATA_FILE, arcDir+".tar.gz"+"/"+aliquotFile);
								dataFiles.put(arcDir+"/"+aliquotFile, vector);
								if(debug)System.out.println("selectFiles2Unpack: "+arcDir+"/"+aliquotFile);
							}
					}
				}
			}
			files2Unpack.put(arcFile, fileList);
			if(fileList.size()>0)
				reporter.addImport(arcFile);
			if(GetFromTcga.standAlone)
				System.out.print(".");
			System.out.flush();
		}
}

private String createAliquotPattern(String analytesID, String projectName,
			String siteID, String patientID, String sampleID, String portionID,
			String plateID, String centerID, LinkedList<String> aliquotList) {
		// TODO Auto-generated method stub
	
			if(aliquotList!=null)
				if(aliquotList.size()>0)
				{
					String	pattern="";
					for(String	aliquot:aliquotList)
					{
						String[]	barCodeFields=AsserUtil.split(aliquot,"-");
						String	barCode=((barCodeFields.length>0)?barCodeFields[0]:".*")+"-"+
									((barCodeFields.length>1)?barCodeFields[1].replace("XX", "\\w\\w"):"\\w\\w")+"-"+
											((barCodeFields.length>2)?barCodeFields[2].replace("XXXX", "\\w\\w\\w\\w"):"\\w\\w\\w\\w")+"-"+
													((barCodeFields.length>3)?barCodeFields[3].replace("XXX", "\\w\\w\\w"):"\\w\\w\\w")+"-"+
															((barCodeFields.length>4)?barCodeFields[4].replace("XXX", "\\w\\w\\w"):"\\w\\w\\w")+"-"+
																	((barCodeFields.length>5)?barCodeFields[5].replace("XXXX", "\\w\\w\\w\\w"):"\\w\\w\\w\\w")+"-"+
																			((barCodeFields.length>6)?barCodeFields[6].replace("XX", "\\w\\w"):"\\w\\w");						
						pattern+=(pattern.length()>0?"|":"")+barCode;
					}
					return	pattern;
				}
			String[]	centers=AsserUtil.split(centerID, ",");
			String	centersStr="";
			for(String	center:centers){
				if(center.matches("\\w\\w")){
					centersStr+=((centersStr.length()>0)?"|":"")+center;
				}
			}
			reporter.setSampleParams(analytesID, siteID, patientID, sampleID, portionID, plateID);
			return	((projectName.length()>0)?"("+projectName.toUpperCase()+"|NAME)":".*")+"-"+
			((siteID.length()>0)?"("+siteID.replace(",", "|")+"|XX)":"\\w\\w")+"-"+
			((patientID.length()>0)?"("+patientID.replace(",", "|")+"|XXXX)":"\\w\\w\\w\\w")+"-"+
			((sampleID.length()>0)?"("+sampleID+"|XXX)":"\\w\\w\\w")+"-"+
			((portionID.length()>0)?"("+portionID+"|XXX)":"\\w\\w\\w")+"-"+
			((plateID.length()>0)?"("+plateID+"|XXXX)":"\\w\\w\\w\\w")+"-"+
			((centerID.length()>0)?"("+centersStr+"|XX)":"\\w\\w");
	}

private void selectFilesOfInterest(String serialNumber, String revision,String	levelType,String	projectName) {
		// TODO Auto-generated method stub
		String	template="",templateLevel1="",templateLevel2="",templateLevel3="",templateLevel4="",templateAux="",templateMageTab="";
		String[]	serialNumbers;
		LinkedList<String>	serialNumbersList;
	
		archiveNodes=new	Hashtable<String,ArchiveCategoryStatisticsNode>();
		archiveFileNames=new	LinkedList<String>();
		if(serialNumber.length()>0){
			serialNumbers=AsserUtil.split(serialNumber);
			serialNumbersList=new	LinkedList<String>();
			for(String	sn:serialNumbers){
				if(sn.matches("[\\d]{1,4}"))
						template+=(template.length()>0?"|":"")+sn;
				else
					System.out.println("Warning: invalid serial number "+sn+". Skipping.");
			}
			template="("+template+")";
		}
		else
			template="[\\d]{1,4}";
		if(revision.length()>0){
			if(!revision.matches("[\\d]{1,6}")){
				System.out.println("Warning: invalid revision number "+revision+". Selecting the latest.");
				revision="";
				template+="\\.[\\d]{1,6}";
			}
			else
				template+="\\."+revision;
		}else
			template+="\\.[\\d]{1,6}";
		template+="\\.[\\d]{1,3}\\.tar\\.gz";
		templateLevel1="Level_1\\."+template;
		templateLevel2="Level_2\\."+template;
		templateLevel3="Level_3\\."+template;
		templateLevel4="Level_4\\."+template;
		templateAux="aux\\."+template;
		templateMageTab="mage-tab\\.[\\d]{1,4}\\.[\\d]{1,6}\\.[\\d]{1,3}\\.tar\\.gz";
		reporter.setProject(projectName);
		reporter.setRevisions(revision);
		reporter.setSerialNumbers(serialNumber);
		for(String	dir:dataTypeDirs){
			if(debug)
				System.out.println("Will explore data-type dir "+dir);
			ArchiveCategoryStatisticsNode[]	NodeVector=archives.get(dir);
			String	prefixTemplate="(?i)"+NodeVector[2].title.replace(".", "\\.")+"_"+NodeVector[0].code.replaceAll("_(p|P)", "")+"\\."+NodeVector[3].code+"\\.";
			int	preffixLength=NodeVector[2].title.length()+NodeVector[0].code.length()+NodeVector[3].code.length()+3;
			String	archiveTemplate=prefixTemplate+template;
			String	archiveTemplateLevel1=prefixTemplate+templateLevel1;
			String	archiveTemplateLevel2=prefixTemplate+templateLevel2;
			String	archiveTemplateLevel3=prefixTemplate+templateLevel3;
			String	archiveTemplateLevel4=prefixTemplate+templateLevel4;
			String	archiveTemplateAux=prefixTemplate+templateAux;
			String	archiveTemplateMageTab=prefixTemplate+templateMageTab;
			if(debug)System.out.println("selectFilesOfInterest: "+dir+" - "+archiveTemplate);//-----DEBUG
			DirBrowser	archivesDir=new	DirBrowser(dir, userName,passwd);
			if(debug)System.out.println("selectFilesOfInterest: browser initialized on "+dir);//-----DEBUG
			LinkedList<String>	archivesFilesOnly=archivesDir.browseFilesOnlyExcluding(ignoreArchivePattern);
			LinkedList<String>	archivesDirsOnly=archivesDir.browseDirsOnlyExcluding(ignoreArchivePattern);
			LinkedList<String>	archivesFiles=new	LinkedList<String>();
			archivesFiles.addAll(archivesFilesOnly);
			archivesFiles.addAll(archivesDirsOnly);
			if(debug)System.out.println("selectFilesOfInterest: detected "+archivesFiles.size() +" files");//-----DEBUG
			LinkedList<String>	selectedFiles=new	LinkedList<String>();
			boolean	tarGzPrezent=false;
			Hashtable<Integer,Integer>	SerialNum2Revision=new	Hashtable<Integer,Integer>();
			Hashtable<Integer,Integer>	SerialNum2RevisionLevel1=new	Hashtable<Integer,Integer>();
			Hashtable<Integer,Integer>	SerialNum2RevisionLevel2=new	Hashtable<Integer,Integer>();
			Hashtable<Integer,Integer>	SerialNum2RevisionLevel3=new	Hashtable<Integer,Integer>();
			Hashtable<Integer,Integer>	SerialNum2RevisionLevel4=new	Hashtable<Integer,Integer>();
			Hashtable<Integer,Integer>	SerialNum2RevisionMageTab=new	Hashtable<Integer,Integer>();
			Hashtable<Integer,Integer>	SerialNum2RevisionAux=new	Hashtable<Integer,Integer>();
			for(String	fileName:archivesFiles){
				if(DirBrowser.fileAccessible(dir+File.separator+fileName+"!", userName, passwd)){
					System.out.println("Archive "+fileName+" not fully downloaded, skipping...");
					continue;
				}
				else
					if(debug)System.out.println("Debug:>>> control file "+fileName+"! is inaccessible");
				String	matchMode="";
				ArchiveCategoryStatisticsNode[]	ArchiveVector=new	ArchiveCategoryStatisticsNode[6];
				ArchiveVector[0]=NodeVector[0];
				ArchiveVector[1]=NodeVector[1];
				ArchiveVector[2]=NodeVector[2];
				ArchiveVector[3]=NodeVector[3];
				ArchiveVector[4]=NodeVector[4];
				String	fullName=dir+fileName;
				if(debug){
					System.out.println("archivesDirsOnly.contains(\""+fileName+"\") = "+archivesDirsOnly.contains(fileName));
					System.out.println("fileName.matches(\""+prefixTemplate+".*\") = "+fileName.matches(prefixTemplate+".*"));
					System.out.println("!DirBrowser.fileAccessible(\""+fullName+".tar.gz\", userName, passwd) = "+!DirBrowser.fileAccessible(fullName+".tar.gz", userName, passwd));
				}
				if(fileName.matches(".*\\.tar\\.gz")|
						((archivesDirsOnly.contains(fileName)&&
								//fileName.matches(prefixTemplate+".*")&&
								!DirBrowser.fileAccessible(fullName+".tar.gz", userName, passwd)))){
					boolean	isDir=!fileName.matches(".*\\.tar\\.gz");
					tarGzPrezent=true;
//					String	tmpFileName="#"+fileName;
					if(debug)System.out.println("prefixTemplate: "+prefixTemplate);
					String	archiveCode=fileName.replaceAll(prefixTemplate, "").replaceAll("Level_\\d\\.","").replaceAll("mage-tab\\.","").replaceAll("aux\\.", "").replaceAll("\\.tar\\.gz", "");
					if(debug)
//						System.out.println("replace(\"unc.edu_GBM.H-miRNA_8x15Kv2.1.0.0.tar.gz\",\"unc\\.edu_GBM\\.H-miRNA_8x15kv2\\.\") = "+
//								new	String("unc.edu_GBM.H-miRNA_8x15Kv2.1.0.0.tar.gz").replaceAll("(?i)unc\\.edu_GBM\\.H-miRNA_8x15kv2\\.", ""));
//					String	archiveCode=(!isDir)?fileName.substring(preffixLength, fileName.length()-7):
//						fileName.substring(preffixLength);
					if(debug)System.out.println("selectFilesOfInterest: examining archive "+fullName);//-----DEBUG
					if(debug)System.out.println("Archive code: "+archiveCode);
					String	fileTemplate=(!isDir)?archiveTemplate:
								archiveTemplate.substring(0, archiveTemplate.length()-9);
					String	archiveTemplateLevel1_part=(!isDir)?archiveTemplateLevel1:
						archiveTemplateLevel1.substring(0,archiveTemplateLevel1.length()-9);
					String	archiveTemplateLevel2_part=(!isDir)?archiveTemplateLevel2:
						archiveTemplateLevel2.substring(0,archiveTemplateLevel2.length()-9);
					String	archiveTemplateLevel3_part=(!isDir)?archiveTemplateLevel3:
						archiveTemplateLevel3.substring(0,archiveTemplateLevel3.length()-9);
					String	archiveTemplateLevel4_part=(!isDir)?archiveTemplateLevel4:
						archiveTemplateLevel4.substring(0,archiveTemplateLevel4.length()-9);
					String	archiveTemplateAux_part=(!isDir)?archiveTemplateAux:
						archiveTemplateAux.substring(0,archiveTemplateAux.length()-9);
					String	archiveTemplateMageTab_part=(!isDir)?archiveTemplateMageTab:
						archiveTemplateMageTab.substring(0,archiveTemplateMageTab.length()-9);
					if(debug)System.out.println(archiveTemplateMageTab);
					if(fileName.toLowerCase().matches(fileTemplate.toLowerCase())){
						matchMode=" - archive";
						if(debug)System.out.println("Selected "+fileName+" !!!");
//						if(fileName.equalsIgnoreCase("lbl.gov_OV.HuEx-1_0-st-v2.17.2.0.tar.gz"))
//							System.exit(2);
						ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, CategoryType.ARCHIVE,fullName);
						if(!archiveFileNames.contains(fullName))
							archiveFileNames.add(fullName);
//						if(selectedLevels.get("unknown"))
							if(!selectedFiles.contains(fullName))
								selectedFiles.add(fullName);
						archiveNodes.put(fileName, ArchiveVector[5]);
						archives.put(fullName, ArchiveVector);
						if(revision.length()==0)
							if(SerialNum2Revision.containsKey(ArchiveVector[5].serialNumber)){
								if(SerialNum2Revision.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
									SerialNum2Revision.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
								}
							}
							else
								SerialNum2Revision.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
						else
							filteredArchives.put(fullName, ArchiveVector);
					}
					else
						if(fileName.toLowerCase().matches(archiveTemplateLevel1_part.toLowerCase())){
							matchMode=" - archive lv1";
							ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, 1, CategoryType.ARCHIVE,fullName);
							if(!archiveFileNames.contains(fullName))
								archiveFileNames.add(fullName);
							if(selectedLevels.get("level1"))
								if(!selectedFiles.contains(fullName))
									selectedFiles.add(fullName);
							archiveNodes.put(fileName, ArchiveVector[5]);
							archives.put(fullName, ArchiveVector);
							if(revision.length()==0)
								if(SerialNum2RevisionLevel1.containsKey(ArchiveVector[5].serialNumber)){
									if(SerialNum2RevisionLevel1.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
										SerialNum2RevisionLevel1.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
									}
								}
								else
									SerialNum2RevisionLevel1.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
							else
								archivesLevel1.put(fullName, ArchiveVector);
						}
						else
							if(fileName.toLowerCase().matches(archiveTemplateLevel2_part.toLowerCase())){
								matchMode=" - archive lv2";
								ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, 2, CategoryType.ARCHIVE,fullName);
								if(!archiveFileNames.contains(fullName))
									archiveFileNames.add(fullName);
								if(selectedLevels.get("level2"))
									if(!selectedFiles.contains(fullName))
										selectedFiles.add(fullName);
								archiveNodes.put(fileName, ArchiveVector[5]);
								archives.put(fullName, ArchiveVector);
								if(revision.length()==0)
									if(SerialNum2RevisionLevel2.containsKey(ArchiveVector[5].serialNumber)){
										if(SerialNum2RevisionLevel2.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
											SerialNum2RevisionLevel2.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
										}
									}
									else
										SerialNum2RevisionLevel2.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
								else
									archivesLevel2.put(fullName, ArchiveVector);
							}
							else
								if(fileName.toLowerCase().matches(archiveTemplateLevel3_part.toLowerCase())){
									matchMode=" - archive lv3";
									ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, 3, CategoryType.ARCHIVE,fullName);
									if(!archiveFileNames.contains(fullName))
										archiveFileNames.add(fullName);
									if(selectedLevels.get("level3"))
										if(!selectedFiles.contains(fullName))
											selectedFiles.add(fullName);
									archiveNodes.put(fileName, ArchiveVector[5]);
									archives.put(fullName, ArchiveVector);
									if(revision.length()==0)
										if(SerialNum2RevisionLevel3.containsKey(ArchiveVector[5].serialNumber)){
											if(SerialNum2RevisionLevel3.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
												SerialNum2RevisionLevel3.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
											}
										}
										else
											SerialNum2RevisionLevel3.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
									else
										archivesLevel3.put(fullName, ArchiveVector);
								}
								else
									if(fileName.toLowerCase().matches(archiveTemplateLevel4_part.toLowerCase())){
										matchMode=" - archive lv4";
										ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, 4, CategoryType.ARCHIVE,fullName);
										if(!archiveFileNames.contains(fullName))
											archiveFileNames.add(fullName);
										if(selectedLevels.get("level4"))
											if(!selectedFiles.contains(fullName))
												selectedFiles.add(fullName);
										archiveNodes.put(fileName, ArchiveVector[5]);
										archives.put(fullName, ArchiveVector);
										if(revision.length()==0)
											if(SerialNum2RevisionLevel4.containsKey(ArchiveVector[5].serialNumber)){
												if(SerialNum2RevisionLevel4.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
													SerialNum2RevisionLevel4.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
												}
											}
											else
												SerialNum2RevisionLevel4.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
										else
											archivesLevel4.put(fullName, ArchiveVector);
									}
									else
										if(fileName.toLowerCase().matches(archiveTemplateMageTab_part.toLowerCase())){
											matchMode=" - archive mage-tab";
											if(debug)System.out.println("Found mage-tab: "+fileName);
											ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, 5, CategoryType.ARCHIVE,fullName);
											if(!archiveFileNames.contains(fullName))
												archiveFileNames.add(fullName);
//											if(selectedLevels.get("misc"))
												if(!selectedFiles.contains(fullName))
													selectedFiles.add(fullName);
											archiveNodes.put(fileName, ArchiveVector[5]);
											archives.put(fullName, ArchiveVector);
												if(SerialNum2RevisionMageTab.containsKey(ArchiveVector[5].serialNumber)){
													if(SerialNum2RevisionMageTab.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
														SerialNum2RevisionMageTab.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
													}
												}
												else
													SerialNum2RevisionMageTab.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
											
										}
										else
											if(fileName.toLowerCase().matches(archiveTemplateAux_part.toLowerCase())){
												matchMode=" - archive aux";
												ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, archiveCode, 6, CategoryType.ARCHIVE,fullName);
												if(!archiveFileNames.contains(fullName))
													archiveFileNames.add(fullName);
												if(selectedLevels.get("aux"))
													if(!selectedFiles.contains(fullName))
														selectedFiles.add(fullName);
												archiveNodes.put(fileName, ArchiveVector[5]);
												archives.put(fullName, ArchiveVector);
												if(revision.length()==0)
													if(SerialNum2RevisionAux.containsKey(ArchiveVector[5].serialNumber)){
														if(SerialNum2RevisionAux.get(ArchiveVector[5].serialNumber)<ArchiveVector[5].revision){
															SerialNum2RevisionAux.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
														}
													}
													else
														SerialNum2RevisionAux.put(ArchiveVector[5].serialNumber, ArchiveVector[5].revision);
												else
													archivesAux.put(fullName, ArchiveVector);
											}
					
				}
				else{
					if(selectedLevels.get("unknown")&&!DirBrowser.fileAccessible(fullName+".tar.gz", userName, passwd)){
						if(debug)
							System.out.println("Selecting to unpack "+fullName);
						files2Unpack.put(fullName, new	LinkedList<String>());
						ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, CategoryType.MISC,fullName);
						archives.put(fullName, ArchiveVector);
					}
				}
				if(debug)System.out.println(matchMode);
			}
			String	serialNumRevTemplate=buildSerialNumRevTemplate(SerialNum2Revision);
			String	serialNumRevTemplateLevel1=buildSerialNumRevTemplate(SerialNum2RevisionLevel1);
			String	serialNumRevTemplateLevel2=buildSerialNumRevTemplate(SerialNum2RevisionLevel2);
			String	serialNumRevTemplateLevel3=buildSerialNumRevTemplate(SerialNum2RevisionLevel3);
			String	serialNumRevTemplateLevel4=buildSerialNumRevTemplate(SerialNum2RevisionLevel4);
			String	serialNumRevTemplateMageTab=buildSerialNumRevTemplate(SerialNum2RevisionMageTab);
			String	serialNumRevTemplateAux=buildSerialNumRevTemplate(SerialNum2RevisionAux);
//			System.out.println("selectFileOfInterest: selecting archives matching "+serialNumRevTemplate);//-------DEBUG
//			System.out.println("selectFileOfInterest: selecting archives matching Level1"+serialNumRevTemplateLevel1);//-------DEBUG
//			System.out.println("selectFileOfInterest: selecting archives matching Level2"+serialNumRevTemplateLevel2);//-------DEBUG
//			System.out.println("selectFileOfInterest: selecting archives matching Level3"+serialNumRevTemplateLevel3);//-------DEBUG
//			System.out.println("selectFileOfInterest: selecting archives matching Level4"+serialNumRevTemplateLevel4);//-------DEBUG
//			System.out.println("selectFileOfInterest: selecting archives matching MageTab"+serialNumRevTemplateMageTab);//-------DEBUG
//			System.out.println("selectFileOfInterest: selecting archives matching Aux"+serialNumRevTemplateAux);//-------DEBUG
			String	mageTableDir="";
			LinkedList<String>	selectedFiles2=new	LinkedList<String>();
			if(revision.length()==0)
				for(String	archFileName:selectedFiles){
					if(debug)System.out.println("selectFiles:           selected "+archFileName);
					if(latestVersion(archFileName,SerialNum2Revision,SerialNum2RevisionLevel1,
							SerialNum2RevisionLevel2,SerialNum2RevisionLevel3,SerialNum2RevisionLevel4,
							SerialNum2RevisionMageTab,SerialNum2RevisionAux)){
//					if(archFileName.toLowerCase().matches(serialNumRevTemplate)){
						if(!selectedFiles2.contains(archFileName))
							selectedFiles2.add(archFileName);
						if(GetFromTcga.verbose)
							System.out.println("Selecting "+archFileName);
						switch(archives.get(archFileName)[5].dataLevel){
						case	0:
								filteredArchives.put(archFileName, archives.get(archFileName));
						break;
						case	1:
								archivesLevel1.put(archFileName, archives.get(archFileName));
						break;
						case	2:
								archivesLevel2.put(archFileName, archives.get(archFileName));
						break;
						case	3:
								archivesLevel3.put(archFileName, archives.get(archFileName));
//								System.out.println("!!!Added "+archFileName);
						break;
						case	4:
								archivesLevel4.put(archFileName, archives.get(archFileName));
						break;
						case	6:
								archivesAux.put(archFileName, archives.get(archFileName));
						break;
						}
					}
				}
			else
				selectedFiles2=selectedFiles;
			for(String	archFileName:selectedFiles2){
//				if(archFileName.toLowerCase().matches(serialNumRevTemplate)){
					if(archives.get(archFileName)[5].dataLevel==5){
						archivesMageTab.put(mageTableDir=archFileName, archives.get(archFileName));
					}
//				}
			}
			LinkedList<String[]>	mageTab;
			if(debug)System.out.println("selectFilesOfInterest: common mage-tab "+mageTableDir);
			if(debug)System.out.println("selectFilesOfInterest: Preparing to get mageTab...");//------DEBUG
			if(mageTableDir.length()>0){
				mageTableDir=mageTableDir.matches(".*\\.tar\\.gz")?
					mageTableDir.substring(0,mageTableDir.length()-7)+"/":
						mageTableDir+"/";
					if(debug)System.out.println("selectFilesOfInterest "+mageTableDir);
				mageTab=getMageTab(mageTableDir);
			}
			else
				mageTab=null;
			for(String	archFileName:selectedFiles2){
				if(debug)System.out.println("selectFilesOfInterest: getting aliquots for "+archFileName+":");//-------DEBUG
				LinkedList<String>	aliquots=getAliquotNames(archFileName,mageTab,projectName);
				if(aliquots.size()==0)
					aliquots=getAliquotNames(archFileName,null,projectName);
				boolean	downloadThis=false;
				for(String	aliquot:aliquots)
					if(aliquot.matches(aliquotsIDPattern)){
						if(debug)System.out.println("\t"+aliquot);
						if(!selectedAliquots.contains(aliquot))
							selectedAliquots.add(aliquot);
						downloadThis=true;
					}
				if(aliquots.size()==0){
					if(debug)System.out.println("Could not identify aliquots.");
					downloadThis=true;
				}
				if(downloadThis)
					archives2Aliquots.put(archFileName, aliquots);
				}
/*			
			if(!tarGzPrezent){
				for(String	fileName:archivesFiles){
					String	matchMode="";
					ArchiveCategoryStatisticsNode[]	ArchiveVector=new	ArchiveCategoryStatisticsNode[6];
					ArchiveVector[0]=NodeVector[0];
					ArchiveVector[1]=NodeVector[1];
					ArchiveVector[2]=NodeVector[2];
					ArchiveVector[3]=NodeVector[3];
					ArchiveVector[4]=NodeVector[4];
					String	fullName=dir+fileName;
					ArchiveVector[5]=new	ArchiveCategoryStatisticsNode(fileName, fileName, 7, CategoryType.ARCHIVE,fullName);
					archiveFileNames.add(fullName);
					archiveNodes.put(fileName, ArchiveVector[5]);
					archives.put(fullName, ArchiveVector);
					archivesMisc.put(fullName, ArchiveVector);
				}
			}*/
			if(GetFromTcga.standAlone)
				System.out.print(".");
			System.out.flush();
		}
		if(GetFromTcga.standAlone)
			System.out.println();
	}

private boolean latestVersion(String archFileName,
		Hashtable<Integer, Integer> serialNum2Revision,
		Hashtable<Integer, Integer> serialNum2RevisionLevel1,
		Hashtable<Integer, Integer> serialNum2RevisionLevel2,
		Hashtable<Integer, Integer> serialNum2RevisionLevel3,
		Hashtable<Integer, Integer> serialNum2RevisionLevel4,
		Hashtable<Integer, Integer> serialNum2RevisionMageTab,
		Hashtable<Integer, Integer> serialNum2RevisionAux) {
	// TODO Auto-generated method stub
	int	serialNum=archives.get(archFileName)[5].serialNumber;
	int	revision=archives.get(archFileName)[5].revision;
	switch(archives.get(archFileName)[5].dataLevel){
	case	0:
		if(revision<serialNum2Revision.get(serialNum))
			return	false;
		if(serialNum2RevisionLevel1.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)>serialNum2RevisionLevel1.get(serialNum))
				return	true;
		}
		else
			return	true;
		if(serialNum2RevisionLevel2.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)>serialNum2RevisionLevel2.get(serialNum))
				return	true;
		}
		else
			return	true;
		if(serialNum2RevisionLevel3.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)>serialNum2RevisionLevel3.get(serialNum))
				return	true;
		}
		else
			return	true;
//		if(serialNum2RevisionLevel4.get(serialNum)!=null){
//			if(serialNum2Revision.get(serialNum)>serialNum2RevisionLevel4.get(serialNum))
//				return	true;
//		}
//		else
//			return	true;
		break;
	case	1:
		if(revision<serialNum2RevisionLevel1.get(serialNum))
			return	false;
		if(serialNum2Revision.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)<=serialNum2RevisionLevel1.get(serialNum))
				return	true;
		}
		else
			return	true;
		break;
	case	2:
		if(revision<serialNum2RevisionLevel2.get(serialNum))
			return	false;
		if(serialNum2Revision.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)<=serialNum2RevisionLevel2.get(serialNum))
				return	true;
		}
		else
			return	true;
		break;
	case	3:
		if(revision<serialNum2RevisionLevel3.get(serialNum))
			return	false;
		if(serialNum2Revision.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)<=serialNum2RevisionLevel3.get(serialNum))
				return	true;
		}
		else
			return	true;
		break;
	case	4:
		if(revision<serialNum2RevisionLevel4.get(serialNum))
			return	false;
		if(serialNum2Revision.get(serialNum)!=null){
			if(serialNum2Revision.get(serialNum)<=serialNum2RevisionLevel4.get(serialNum))
				return	true;
		}
		else
			return	true;
		break;
	case	5:
		if(revision<serialNum2RevisionMageTab.get(serialNum))
			return	false;
		else
			return	true;
	case	6:
		if(revision<serialNum2RevisionAux.get(serialNum))
			return	false;
		else
			return	true;
	}
	return false;
}

private String buildSerialNumRevTemplate(
		Hashtable<Integer, Integer> serialNum2Revision) {
	// TODO Auto-generated method stub
	String	serialNumRevTemplate="(";
	for(Integer	serialNum:serialNum2Revision.keySet()){
		serialNumRevTemplate+=(serialNumRevTemplate.length()>1?"|":"")+serialNum+"\\."+serialNum2Revision.get(serialNum);
	}
	serialNumRevTemplate=".*\\.(("+serialNumRevTemplate+")\\.[\\d]{1,3}\\.tar\\.gz))";
	return serialNumRevTemplate;
}

private LinkedList<String[]> getMageTab(String mageTableDir) {
	// TODO Auto-generated method stub
	String	mageTabFileName="";
	LinkedList<String[]>	mageTab;
	InputStream	in=null;
	boolean	considerLevels=false;
	
	while(mageTableDir.lastIndexOf("/")==mageTableDir.length()-1)
		mageTableDir=mageTableDir.substring(0,mageTableDir.length()-1);
	if(mageTableDir.length()>0){
		
		if(DirBrowser.fileAccessible(mageTableDir,userName,passwd))
		{	
			if(debug)System.out.println("getMageTab: preparing to browse "+mageTableDir);
			DirBrowser	mageTabDirBrowser=new	DirBrowser(mageTableDir,userName,passwd);
			if(debug)System.out.println("getMageTab: browsed "+mageTableDir);
			DirBrowser	mageTabDirMirrorBrowser=null;
			if(cache.getMirrorUrl()!=null)
				mageTabDirMirrorBrowser=new	DirBrowser(mageTableDir.replace(cache.getBaseUrl().toExternalForm(), 
							cache.getMirrorUrl().toExternalForm()), userName,passwd);
			if(debug)System.out.println("getMageTab: browsed "+mageTableDir+", found "+mageTabDirBrowser.browse().size()
					+" files");
			LinkedList<String>	mageTabFiles=mageTabDirBrowser.browseFilesOnly();
			if(mageTabDirMirrorBrowser!=null)
				mageTabFiles.addAll(mageTabDirMirrorBrowser.browseFilesOnly());
			for(String	file:mageTabFiles){
				if(debug)System.out.println("getMageTab: mageTabTemplate="+mageTabTemplate);
				if(file.matches(mageTabTemplate)&&!file.matches(mageTabTemplateExclude)){
					in=cache.readFileFromURL(mageTableDir+"/"+file);
					if(in!=null)
						if(debug)System.out.println("getMageTab: read "+mageTableDir+"/"+file);
					break;
				}
			}
		}
	}
	if(in==null){
		if((DirBrowser.fileAccessible(mageTableDir+".tar.gz",userName,passwd))&&(!DirBrowser.fileAccessible(mageTableDir,userName,passwd))){
			LinkedList<String>	mageTabFiles;
			boolean	manifestAccessible=DirBrowser.fileAccessible(mageTableDir+".tar.gz.manifest", userName, passwd);
			if(cache.getMirrorUrl()!=null)
				manifestAccessible=manifestAccessible||DirBrowser.fileAccessible(mageTableDir.replace(cache.getBaseUrl().toExternalForm(), 
						cache.getMirrorUrl().toExternalForm())+".tar.gz.manifest", userName, passwd);
			if(manifestAccessible){
				if(debug)System.out.println("getMageTab: going to read "+mageTableDir+".tar.gz.manifest");
				String	cacheMirrorUrl=null;
				if(cache.getMirrorUrl()!=null)
					cacheMirrorUrl=mageTableDir.replace(cache.getBaseUrl().toExternalForm(), 
							cache.getMirrorUrl().toExternalForm())+".tar.gz";
				mageTabFiles=DirBrowser.readManifest(cache.readFileFromURL(mageTableDir+".tar.gz.manifest"), 
						mageTableDir+".tar.gz",cacheMirrorUrl,cache,userName,passwd);
				if(debug)System.out.println("getMageTab: read successfully "+mageTableDir+".tar.gz.manifest");
			}
			else
				{
				if(debug)System.out.println("getMageTab: Reading "+mageTableDir+".tar.gz");
				mageTabFiles=TarZip.browse(cache.readFileFromURL(mageTableDir+".tar.gz"));
				String	cacheMirrorUrl=null;
				if(cache.getMirrorUrl()!=null)
					cacheMirrorUrl=mageTableDir.replace(cache.getBaseUrl().toExternalForm(), 
							cache.getMirrorUrl().toExternalForm())+".tar.gz";
				DirBrowser.generateManifest(mageTableDir+".tar.gz",
						cacheMirrorUrl,mageTabFiles,userName,passwd);
				}
			if(debug)System.out.println("getMageTab: size of mageTabFiles "+mageTabFiles.size());
			if(mageTabFiles!=null)
				for(String	file:mageTabFiles){
					if(file.matches(mageTabTemplate)&&!file.matches(mageTabTemplateExclude)){
						try {
							URL	mageTableDirURL=new	URL(mageTableDir);
//							if(mageTableDirURL.getProtocol().equalsIgnoreCase("file")){
								LinkedList<String>	mageTabFileList=new	LinkedList<String>();
								if(!mageTabFileList.contains(file))
									mageTabFileList.add(file);
								TarZip.unpack(cache.readFileFromURL(mageTableDir+".tar.gz"), mageTableDir+".tar.gz", mageTableDirURL.toExternalForm(), mageTabFileList);
								String	cacheMirrorUrl=null;
								if(cache.getMirrorUrl()!=null)
									cacheMirrorUrl=mageTableDirURL.toExternalForm().replace(cache.getBaseUrl().toExternalForm(), 
											cache.getMirrorUrl().toExternalForm());
								if((cache.getMirrorUrl()!=null))
									TarZip.unpack(cache.readFileFromURL(mageTableDir+".tar.gz"), 
											mageTableDir+".tar.gz", 
													cacheMirrorUrl, mageTabFileList);
								in=cache.readFileFromURL(mageTableDirURL+"/"+file);
									if(in!=null)
										if(debug)System.out.println("getMageTab: read "+mageTableDirURL.toExternalForm()+"/"+file);
/*							}
							else
								in=null;*/
						} catch (MalformedURLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							in=null;
							System.exit(1);
						}
					
					
						if(in==null)
							in=TarZip.readFromTARGZ(cache.readFileFromURL(mageTableDir+".tar.gz"), file);
						if(in!=null)
							if(debug)System.out.println("getMageTab: read "+mageTableDir+".tar.gz/"+file);
						break;
					}
				}
		}
	}
	if(in!=null){
		try {
			CSVParser	mageTabCSV=new	CSVParser(in,"\t",0,"",false);
			mageTab=new	LinkedList<String[]>();
			String[]	columnNames=mageTabCSV.getColumnNames();
			int	sampleIDIndex=0;
			for(int i=0;i<columnNames.length;i++)
			{
				if(columnNames[i].equalsIgnoreCase("Comment [TCGA Barcode]"))
					sampleIDIndex=i;
				else
					if(columnNames[i].equalsIgnoreCase("Sample Name"))
						sampleIDIndex=i;
					else
						if(columnNames[i].equalsIgnoreCase("Source Name"))
							sampleIDIndex=i;
						else
							if(columnNames[i].equalsIgnoreCase("Extract Name"))
								sampleIDIndex=i;
				
			}
			
			LinkedList<Integer>	sampleIndex=new	LinkedList<Integer>();
			
			int[]	levels=new	int[columnNames.length];
			int	lastP=0;
			String[]	firstLine=mageTabCSV.next();
			for(int	i=0;i<columnNames.length;i++)
			{
				if(columnNames[i].equalsIgnoreCase("Array Data File")||
						columnNames[i].equalsIgnoreCase("Array Matrix File")||
						columnNames[i].equalsIgnoreCase("Derived Array Data Matrix File")||
						columnNames[i].equalsIgnoreCase("Derived Array Data File")||
						(columnNames[i].equalsIgnoreCase("Hybridization Name"))){
					sampleIndex.add(i);
				}
				if(columnNames[i].equalsIgnoreCase("Comment [TCGA Data Level]"))
					{
					considerLevels=true;
					int	level=0;
					if(firstLine[i].equalsIgnoreCase("Level 1"))
						level=1;
					else
						if(firstLine[i].equalsIgnoreCase("Level 2"))
							level=2;
						else
							if(firstLine[i].equalsIgnoreCase("Level 3"))
								level=3;
							else
								if(firstLine[i].equalsIgnoreCase("Level 4"))
									level=4;
					if(debug)System.out.println("Detected level: "+level);
					for(int	j=lastP;j<=i;j++){
							levels[j]=level;
						}
					lastP=i+1;	
					}
			}
			if(sampleIndex.size()>0){
				mageTab=addSampe2Files(firstLine, sampleIDIndex, sampleIndex, considerLevels, levels, mageTab);
				for(String[]	line:mageTabCSV){
					mageTab=addSampe2Files(line, sampleIDIndex, sampleIndex, considerLevels, levels, mageTab);
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			mageTab=null;
			e.printStackTrace();
			System.exit(1);
		}
	}
	else
		mageTab=null;
	try {
		if(in!=null)
			in.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return	mageTab;
}

private	LinkedList<String[]>	addSampe2Files(String[]	line,int	sampleIDIndex,
									LinkedList<Integer>	sampleIndex,
						boolean	considerLevels,int[]	levels,LinkedList<String[]>	mageTab)
{
	String[]	sample2Files=new	String[sampleIndex.size()+1];
	if(debug)System.out.println("getMageTab line.length="+ line.length);
	if(debug)System.out.println("getMageTab sampleIDIndex="+ sampleIDIndex);
	if(debug)System.out.println("getMageTab line[sampleIDIndex]="+line[sampleIDIndex]);
	if(line[sampleIDIndex]!=null)
		if(line[sampleIDIndex].matches(projectName+"-\\w\\w-\\w\\w\\w\\w-.*")){
			sample2Files[0]=line[sampleIDIndex];
			for(int	i=0;i<sampleIndex.size();i++)
			{					
				String	fileName=line[sampleIndex.get(i)];
				String	basicName=fileName.replaceAll("\\..*", "");
				if(!fileName.equals("->")){
					sample2Files[i+1]=fileName;
					if(debug)System.out.println("getMageTab fileName="+fileName);
					if(debug)System.out.println("getMageTab basicName="+basicName);
					if(considerLevels){
						if(filesLevelfromMageTab.keySet().contains(fileName)){
							if(filesLevelfromMageTab.get(fileName)!=levels[sampleIndex.get(i)]){
								System.out.println("Warning: found ambiguity in data level definition of mage tables for file "+
										fileName+"\nValues "+filesLevelfromMageTab.get(fileName)+" and "+levels[sampleIndex.get(i)]+" do not match!");
								reporter.addBadMageTab(fileName, 
										"mage-tab-level="+filesLevelfromMageTab.get(fileName)+
											", local-mage-tab-level="+levels[sampleIndex.get(i)]);
							}
						}
						else
							filesLevelfromMageTab.put(fileName, levels[sampleIndex.get(i)]);
					}
				}
				else
					sample2Files[i+1]="";
			}
			if(!mageTab.contains(sample2Files))
				mageTab.add(sample2Files);
		}
	return	mageTab;
}

private	LinkedList<String>	getAliquotNames(String	fileName,LinkedList<String[]>	mageTab,String	projectName)
{
	LinkedList<String>	samples=new	LinkedList<String>();
	
//	String	shortName=new	File(fileName).getName();
//		String	shortName=fileName.substring(filePath.length()+1);
	String	dirName=fileName.matches(".*\\.tar\\.gz")?
				fileName.substring(0,fileName.length()-7):
					fileName;
	if(debug)System.out.println("getAliquotNames: starting at "+dirName);
//	LinkedList<String[]>	localMageTab=mageTab;
	LinkedList<String[]>	localMageTab=getMageTab(dirName+"/");
	if(localMageTab==null){
		localMageTab=mageTab;
		if(debug)System.out.println("No local mage-tab at "+dirName+"/"+", switching to common...");
	}
	else
		if(debug)System.out.println("Found local mage-tab at"+dirName+"/");
	LinkedList<String>	archiveFiles=new	LinkedList<String>();
	String	cacheMirrorUrl=null;
	if(cache.getMirrorUrl()!=null)
		cacheMirrorUrl=dirName.replace(cache.getBaseUrl().toExternalForm(), cache.getMirrorUrl().toExternalForm())
		+".tar.gz";
	if(DirBrowser.fileAccessible(dirName+".tar.gz.manifest", userName, passwd)||
			DirBrowser.fileAccessible(cacheMirrorUrl+".manifest", userName, passwd)){
		archiveFiles=DirBrowser.readManifest(cache.readFileFromURL(dirName+".tar.gz.manifest"), dirName+".tar.gz", 
				cacheMirrorUrl,
				cache,userName,passwd);
	}
	if(DirBrowser.fileAccessible(dirName,userName,passwd)){
		DirBrowser	archiveBrowser=new	DirBrowser(dirName,userName,passwd);
		archiveFiles.addAll(archiveBrowser.browseFilesOnly());
		if(debug)System.out.println("Have read the directory "+dirName+". Have found "+archiveFiles.size()+" files");
	}
	else{
		if(debug)System.out.println("Checking whether "+dirName+".tar.gz exists");
		if(DirBrowser.fileAccessible(dirName+".tar.gz",userName,passwd)){
			if(debug)System.out.println("Yes, it does. Reading the content...");
			if(!DirBrowser.fileAccessible(dirName+".tar.gz.manifest", userName, passwd)&&
					!DirBrowser.fileAccessible(cacheMirrorUrl+".manifest", userName, passwd)){
				archiveFiles=TarZip.browse(cache.readFileFromURL(dirName+".tar.gz"));
				DirBrowser.generateManifest(dirName+".tar.gz",
						cacheMirrorUrl,
						archiveFiles,userName,passwd);
				}
		}
	}
	if(localMageTab==null)
		if(debug)System.out.println("Oops! Did not find mage-tab :(");
	if(localMageTab!=null){
		if(debug)System.out.println("getAliquotNames: archiveFiles size is "+archiveFiles.size());
		for(String	arcFile:archiveFiles)
		{
			for(String[]	line:localMageTab)
			{
				LinkedList<String>	fileList;
				boolean	foundFile=false;
				for(int	i=0;i<line.length;i++)
					if(line[i].equalsIgnoreCase(arcFile))
					{
						foundFile=true;
						break;
					}
				String	baseArcFileName=arcFile.replaceAll("\\..*", "");
				if(!foundFile)
					for(int	i=0;i<line.length;i++){
//						String baseLine_i=line[i].replaceAll("\\..*", "");
						String	baseLine_i=line[i];
						if(baseLine_i.equalsIgnoreCase(baseArcFileName))
						{
							foundFile=true;
							break;
						}
				}
				if(foundFile){
					if(!samples.contains(line[0]))
						samples.add(line[0]);
					if(debug)System.out.println(line[0]);
					if(!aliquotFiles.keySet().contains(line[0]))
					{
						fileList=new	LinkedList<String>();
					}
					else
						fileList=aliquotFiles.get(line[0]);
					if(!fileList.contains(arcFile))
						fileList.add(arcFile);
					
					aliquotFiles.put(line[0], fileList);
				}
			}
		}
	}
//	else
	{
		if(debug)
			System.out.println(" ### entiring here...");
		if(archiveFiles!=null)
			for(String	arcFile:archiveFiles)
			{
				String	aliquotFile=arcFile;
				String	aliquot="";
				if(arcFile.matches(".*"+projectName+"-\\w\\w-\\w\\w\\w\\w..*"))
				{
					aliquot=guessAliquot(arcFile,projectName);
					if(aliquot!=null)
					{
						if(!samples.contains(aliquot))
							samples.add(aliquot);
						LinkedList<String>	fileList;
						if(!aliquotFiles.keySet().contains(aliquot))
						{
							fileList=new	LinkedList<String>();
						}
						else
							fileList=aliquotFiles.get(aliquot);
						if(!fileList.contains(aliquotFile))
							fileList.add(aliquotFile);
						if(debug)System.out.println("### ---"+aliquot+"\t--->\t"+aliquotFile);
						aliquotFiles.put(aliquot, fileList);
					}
//					else
//						aliquot="NAME-XX-XXXX-XXX-XXX-XXXX-XX";
				}
			}
	}
	return	samples;
}

private String guessAliquot(String arcFile, String projectName) {
	// TODO Auto-generated method stub
	
	int p=arcFile.indexOf(projectName.toUpperCase()+"-");
	if(p<0)
		return	null;
	String	aliquot=projectName.toUpperCase();
	arcFile=arcFile.substring(p+1+projectName.length());
	if(arcFile.matches("\\w\\w-.*")){
		p=arcFile.indexOf("-");
		aliquot+="-"+arcFile.substring(0,p);
		arcFile=arcFile.substring(p+1);
		if(arcFile.matches("\\w\\w\\w\\w[-\\.].*")){
			p=arcFile.indexOf("-");
			if(p<0)
				p=arcFile.indexOf(".");
			aliquot+="-"+arcFile.substring(0,p);
			arcFile=arcFile.substring(p+1);
			if(arcFile.matches("\\w\\w\\w[-\\.].*")){
				p=arcFile.indexOf("-");
				if(p<0)
					p=arcFile.indexOf(".");
				aliquot+="-"+arcFile.substring(0,p);
				arcFile=arcFile.substring(p+1);
				if(arcFile.matches("\\w\\w\\w[-\\.].*")){
					p=arcFile.indexOf("-");
					if(p<0)
						p=arcFile.indexOf(".");
					aliquot+="-"+arcFile.substring(0,p);
					arcFile=arcFile.substring(p+1);
					if(arcFile.matches("\\w\\w\\w\\w[-\\.].*")){
						p=arcFile.indexOf("-");
						if(p<0)
							p=arcFile.indexOf(".");
						aliquot+="-"+arcFile.substring(0,p);
						arcFile=arcFile.substring(p+1);
						if(arcFile.matches("\\w\\w[-_\\.].*")){
							int	p1=arcFile.indexOf(".");
							int	p2=arcFile.indexOf("-");
							int	p3=arcFile.indexOf("_");
							int	p4=arcFile.length();
							p1=(p1>0)?p1:p4;
							p2=(p2>0)?p2:p4;
							p3=(p3>0)?p3:p4;
							p=(p1<p2)?p1:p2;
							p=(p<p3)?p:p3;
							p=(p<p4)?p:p4;
							aliquot+="-"+arcFile.substring(0,p);
						}
						else
							aliquot+="-XX";
					}
					else
						aliquot+="-XXXX-XX";
				}
				else
					aliquot+="-XXX-XXXX-XX";
			}
			else
				aliquot+="-XXX-XXX-XXXX-XX";
		}
//		else
//			aliquot+="-XXXX-XXX-XXX-XXXX-XX";
	}
//	else
//		aliquot+="-XX-XXXX-XXX-XXX-XXXX-XX";
	
	if(debug)
		System.out.println(" ### Guessed aliquot: "+aliquot);
	
	return aliquot;
}

private void validateCenters(String	centerID,File	centerFile) {
		// TODO Auto-generated method stub
	String[]	centers;
	LinkedList<String>	centerNames;
	
	centerNodes=new	Hashtable<String,ArchiveCategoryStatisticsNode>();
	if(centerID.length()>0){
		System.out.println("Selecting centers:");
		centers=AsserUtil.split(centerID);
		for(int	i=0;i<centers.length;i++){
			if(debug)
				System.out.println("### centers[i]: "+centers[i]);
			String	center;
			System.out.println(center=Codes2Titles.centerName(centerFile, centers[i]));
			if(center.equals(centers[i])){
				System.out.println("Warning: unknown center code "+center);
			}
			reporter.addCenter(center);
		}
	}
	else
	{
		System.out.println("All centers selected.");
		try {
			CSVParser	centerCSV=new	CSVParser(centerFile,"\t",0);
			int	codeColumn=centerCSV.getColumnIndex("Code");
			LinkedList<String>	codeList=new	LinkedList<String>();
			for(String[]	line:centerCSV){
				if(centerCSV.getLineNumber()>0)
					codeList.add(line[codeColumn]);
			}
			int	lineCount=codeList.size();
			centers=new	String[lineCount];
			int	i=0;
			for(String	code:codeList){
				centers[i++]=code;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			centers=new	String[1];
			centers[0]="";
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	centerNames=new	LinkedList<String>();
	for(int	i=0;i<centers.length;i++){
		String	centerName=Codes2Titles.centerName(centerFile, centers[i]);
		centerNodes.put(centers[i], new ArchiveCategoryStatisticsNode(centers[i], 
				centerName, CategoryType.CENTER,""));
		centerNames.add(centerName);
	}
	centerDirs=new	LinkedList<String>();
	for(String	centerTypeDir:centerTypeDirs){
		String	dirURL;
		ArchiveCategoryStatisticsNode[]	centerTypeNodeVector=archives.get(centerTypeDir);
		DirBrowser	centersBrowser=new	DirBrowser(centerTypeDir,userName,passwd);
		LinkedList<String>	unfilteredDirs=centersBrowser.browseDirsOnly();
		for(String	dir:unfilteredDirs){
			if(debug)
				System.out.println("### Center dir: "+dir);
			if(centerNames.contains(dir)){
				centerDirs.add(dirURL=(centerTypeDir+dir+"/"));
				ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[3];
				nodeVector[0]=centerTypeNodeVector[0];
				nodeVector[1]=centerTypeNodeVector[1];
				nodeVector[2]=centerNodes.get(Codes2Titles.centerCode(centerFile, dir));
				if(nodeVector[2]==null)
					nodeVector[2]=new	ArchiveCategoryStatisticsNode(dir,dir,CategoryType.CENTER,"");
				archives.put(dirURL, nodeVector);
			}
			else
			{
				if(centerID.length()==0){
					System.out.println("Warning: detected unknown center "+dir+" at "+centerTypeDir+dir+"/");
					centerDirs.add(dirURL=(centerTypeDir+dir+"/"));
					ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[3];
					nodeVector[0]=centerTypeNodeVector[0];
					nodeVector[1]=centerTypeNodeVector[1];
//					ArchiveCategoryStatisticsNode	node=centerNodes.get(dir);
					ArchiveCategoryStatisticsNode	node=null;
					if(node==null){
						node=new	ArchiveCategoryStatisticsNode(dir,dir,CategoryType.CENTER,"");
					}
					nodeVector[2]=new	ArchiveCategoryStatisticsNode(dir,dir,CategoryType.CENTER,"");
					archives.put(dirURL, nodeVector);
				}
			}
		}
	}
	}

private void validateCenterTypes() {
		// TODO Auto-generated method stub
		centerTypeDirs=new	LinkedList<String>();
		ArchiveCategoryStatisticsNode	bcrNode=new	ArchiveCategoryStatisticsNode("BCR","BCR",CategoryType.CENTER_TYPE,"");
		ArchiveCategoryStatisticsNode	cgccNode=new	ArchiveCategoryStatisticsNode("CGCC","CGCC",CategoryType.CENTER_TYPE,"");
		ArchiveCategoryStatisticsNode	gscNode=new	ArchiveCategoryStatisticsNode("GSC","GSC",CategoryType.CENTER_TYPE,"");
		ArchiveCategoryStatisticsNode	gdacNode=new	ArchiveCategoryStatisticsNode("GDAC","GDAC",CategoryType.CENTER_TYPE,"");
		for(String	cancerDir:cancerDirs){
			if(debug)
				System.out.println("### cancerDir: "+cancerDir);
			ArchiveCategoryStatisticsNode[]	cancerNodeVector=archives.get(cancerDir);
			if(DirBrowser.fileAccessible(cancerDir+"bcr",userName,passwd)){
				centerTypeDirs.add(cancerDir+"bcr/");
				ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[2];
				nodeVector[0]=cancerNodeVector[0];
				nodeVector[1]=bcrNode;
				archives.put(cancerDir+"bcr/", nodeVector);
				if(debug)System.out.println(cancerDir+"bcr/");
			}
			if(DirBrowser.fileAccessible(cancerDir+"cgcc",userName,passwd)){
				centerTypeDirs.add(cancerDir+"cgcc/");
				ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[2];
				nodeVector[0]=cancerNodeVector[0];
				nodeVector[1]=cgccNode;
				archives.put(cancerDir+"cgcc/", nodeVector);
				if(debug)System.out.println(cancerDir+"cgcc/");
			}
			if(DirBrowser.fileAccessible(cancerDir+"gsc",userName,passwd)){
				centerTypeDirs.add(cancerDir+"gsc/");
				ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[2];
				nodeVector[0]=cancerNodeVector[0];
				nodeVector[1]=gscNode;
				archives.put(cancerDir+"gsc/", nodeVector);
				if(debug)System.out.println(cancerDir+"gsc/");
			}
			if(DirBrowser.fileAccessible(cancerDir+"gdac",userName,passwd)){
				centerTypeDirs.add(cancerDir+"gdac/");
				ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[2];
				nodeVector[0]=cancerNodeVector[0];
				nodeVector[1]=gdacNode;
				archives.put(cancerDir+"gdac/", nodeVector);
				if(debug)System.out.println(cancerDir+"gdac/");
			}
		}
	}

/*	private Pattern getSamplesPattern(String samplesID, String projectName,
			String siteID, String patientID, String sampleID, String portionID,
			String plateID, String centerID,File cancerFile,
			File platformFile, File dataFile, File projectFile, File siteFile,
			File centerFile) {
		// TODO Auto-generated method stub
		
		return null;
	}*/

	private void validateDataTypes(String dataType,File	dataFile) {
		// TODO Auto-generated method stub
		
		String[]	types;
		LinkedList<String>	typeNames;
		
		dataTypeNodes=new	Hashtable<String,ArchiveCategoryStatisticsNode>();
		
		if(dataType.length()>0){
			if(invertDataType)
				System.out.println("Skipping datatypes:");
			else
				System.out.println("Selecting datatypes:");
			types=AsserUtil.split(dataType);
			for(int	i=0;i<types.length;i++){
				String	dataTypeName;
				System.out.println(dataTypeName=Codes2Titles.platformName(dataFile, types[i]));
				if(dataTypeName.equals(types[i])){
					System.out.println("Warning: unknown data type code "+types[i]);
				}
				
			}
		}
		else
		{
			System.out.println("All datatypes selected.");
			try {
				CSVParser	dataCSV=new	CSVParser(dataFile,"\t",0);
				int	codeColumn=dataCSV.getColumnIndex("code");
				LinkedList<String>	codeList=new	LinkedList<String>();
				for(String[]	line:dataCSV){
					if(dataCSV.getLineNumber()>0)
						codeList.add(line[codeColumn]);
				}
				int	lineCount=codeList.size();
				types=new	String[lineCount];
				int	i=0;
				for(String	code:codeList){
					types[i++]=code;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				types=new	String[1];
				types[0]="";
				e.printStackTrace();
				System.exit(1);
			}
			
		}
		dataTypeDirs=new	LinkedList<String>();
		LinkedList<String>	typeList=new	LinkedList<String>();
		
		if(invertDataType&&dataType.length()>0)
		{
			System.out.println("Selecting platforms:");
			LinkedList<String>	typeExcludeList=new	LinkedList<String>();
			for(String	type:types)
			{
				typeExcludeList.add(type);
			}
			for(String	platformDir:platformDirs)
			{
				DirBrowser	typesBrowser=new	DirBrowser(platformDir,userName,passwd);
				LinkedList<String>	unfilteredDirs=typesBrowser.browseDirsOnly();
				for(String	typeDir:unfilteredDirs)
					if(!typeExcludeList.contains(typeDir)&&!typeList.contains(typeDir)){
						String	typeName=Codes2Titles.platformName(dataFile, typeDir);
						if(typeName.equalsIgnoreCase(typeDir))
							System.out.println("Warning: unknown datatype code "+typeDir);
						dataTypeNodes.put(typeDir.toLowerCase(), new ArchiveCategoryStatisticsNode(typeDir, 
								typeName, CategoryType.DATA_TYPE,""));
						typeList.add(typeDir.toLowerCase());
						reporter.addDataType(typeName);
						System.out.println(typeName);
					}
			}
		}
		else
			for(int	i=0;i<types.length;i++){
				String	typeName=Codes2Titles.platformName(dataFile, types[i]);
				dataTypeNodes.put(types[i].toLowerCase(), new ArchiveCategoryStatisticsNode(types[i], 
						typeName, CategoryType.DATA_TYPE,""));
				typeList.add(types[i].toLowerCase());
				reporter.addDataType(types[i]);
			}
		
		if(dataType.length()==0)
			invertDataType=false;
		for(String	typeDir:platformDirs){
			if(debug)System.out.println(" ### Platform dir: "+typeDir);
			String	dirURL;
			ArchiveCategoryStatisticsNode[]	dataTypeNodeVector=archives.get(typeDir);
			DirBrowser	typesBrowser=new	DirBrowser(typeDir,userName,passwd);
			LinkedList<String>	unfilteredDirs=typesBrowser.browseDirsOnly();
			if(new	File(typeDir).getName().equalsIgnoreCase("clin")){
				unfilteredDirs.add("");
				typeList.add("");
			}
			for(String	dir:unfilteredDirs){
				if(debug)System.out.println(" #### Data type dir: "+dir);
				if(typeList.contains(dir)){
					dataTypeDirs.add(dirURL=(typeDir+dir+"/"));
					if(debug)System.out.println(" #### Added data type dir: "+dirURL);
					ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[5];
					nodeVector[0]=dataTypeNodeVector[0];
					nodeVector[1]=dataTypeNodeVector[1];
					nodeVector[2]=dataTypeNodeVector[2];
					nodeVector[3]=dataTypeNodeVector[3];
					nodeVector[4]=dataTypeNodes.get(dir);
					archives.put(dirURL, nodeVector);
				}
				else
				{
					if(dataType.length()==0){
						System.out.println("Warning: detected unknown datatype "+dir+" at "+typeDir+dir+"/");
						dataTypeDirs.add(dirURL=(typeDir+dir+"/"));
						ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[5];
						nodeVector[0]=dataTypeNodeVector[0];
						nodeVector[1]=dataTypeNodeVector[1];
						nodeVector[2]=dataTypeNodeVector[2];
						nodeVector[3]=dataTypeNodeVector[3];
						ArchiveCategoryStatisticsNode	node=dataTypeNodes.get(dir);
						if(node==null){
							node=new	ArchiveCategoryStatisticsNode(dir,dir,CategoryType.DATA_TYPE,"");
						}
						nodeVector[4]=node;
						archives.put(dirURL, nodeVector);
					}
				}
				if(GetFromTcga.standAlone)
					System.out.print(".");
				System.out.flush();
			}
		}/*
		for(String	dir:dataTypeDirs){
			System.out.println(dir);
			ArchiveCategoryStatisticsNode[]	nodeVector=archives.get(dir);
			System.out.println(nodeVector[0]+" / "+nodeVector[1]+" / "+nodeVector[2]+" / "+nodeVector[3]+" / "+nodeVector[4]+" / ");
		}*/
		if(GetFromTcga.standAlone)
			System.out.println();
	}

	private void validatePlatformTypes(String platformType,File	platformFile) {
		// TODO Auto-generated method stub
		
		String[]	platforms;
		LinkedList<String>	platformNames;
		
		platformNodes=new	Hashtable<String,ArchiveCategoryStatisticsNode>();
		
		if(platformType.length()>0){	
			if(invertPlatformSelection)
				System.out.println("Skipping platforms:");
			else
				System.out.println("Selecting platforms:");
			platforms=AsserUtil.split(platformType);
			for(int	i=0;i<platforms.length;i++){
				String	platform;
				System.out.println(platform=Codes2Titles.platformName(platformFile, platforms[i]));
				if(platform.equals(platforms[i])){
					System.out.println("Warning: unknown platform code "+platforms[i]);
				}
				
			}
		}
		else
		{
			System.out.println("All platforms selected.");
			try {
				CSVParser	platformCSV=new	CSVParser(platformFile,"\t",0);
				int	codeColumn=platformCSV.getColumnIndex("code");
				LinkedList<String>	codeList=new	LinkedList<String>();
				for(String[]	line:platformCSV){
					if(platformCSV.getLineNumber()>0)
						codeList.add(line[codeColumn]);
				}
				int	lineCount=codeList.size();
				platforms=new	String[lineCount];
				int	i=0;
				for(String	code:codeList){
					platforms[i++]=code;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				platforms=new	String[1];
				platforms[0]="";
				e.printStackTrace();
				System.exit(1);
			}
			
		}
		platformDirs=new	LinkedList<String>();
		LinkedList<String>	platformList=new	LinkedList<String>();
		if(invertPlatformSelection&&platformType.length()>0)
		{
			System.out.println("Selecting platforms:");
			LinkedList<String>	platformExcludeList=new	LinkedList<String>();
			for(String	platform:platforms)
			{
				platformExcludeList.add(platform);
			}
			for(String	centerDir:centerDirs)
			{
				DirBrowser	platformsBrowser=new	DirBrowser(centerDir,userName,passwd);
				LinkedList<String>	unfilteredDirs=platformsBrowser.browseDirsOnly();
				for(String	platformDir:unfilteredDirs)
					if(!platformExcludeList.contains(platformDir)&&!platformList.contains(platformDir)){
						String	platformName=Codes2Titles.platformName(platformFile, platformDir);
						if(platformName.equalsIgnoreCase(platformDir))
							System.out.println("Warning: unknown platform code "+platformDir);
						platformNodes.put(platformDir.toLowerCase(), new ArchiveCategoryStatisticsNode(platformDir, 
								platformName, CategoryType.PLATFORM,""));
						platformList.add(platformDir.toLowerCase());
						reporter.addPlatform(platformDir);
						System.out.println(platformName);
					}
			}
		}
		else
			for(int	i=0;i<platforms.length;i++){
				String	platformName=Codes2Titles.platformName(platformFile, platforms[i]);
				platformNodes.put(platforms[i].toLowerCase(), new ArchiveCategoryStatisticsNode(platforms[i], 
						platformName, CategoryType.PLATFORM,""));
				platformList.add(platforms[i].toLowerCase());
				reporter.addPlatform(platforms[i]);
			}
		
		for(String	platformDir:centerDirs){
			String	dirURL;
			ArchiveCategoryStatisticsNode[]	centerNodeVector=archives.get(platformDir);
			DirBrowser	platformsBrowser=new	DirBrowser(platformDir,userName,passwd);
			LinkedList<String>	unfilteredDirs=platformsBrowser.browseDirsOnly();
			for(String	dir:unfilteredDirs){
				if(platformList.contains(dir)){
					platformDirs.add(dirURL=(platformDir+dir+"/"));
					ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[4];
					nodeVector[0]=centerNodeVector[0];
					nodeVector[1]=centerNodeVector[1];
					nodeVector[2]=centerNodeVector[2];
					nodeVector[3]=platformNodes.get(dir);
					archives.put(dirURL, nodeVector);
				}
				else
				{
					if(platformType.length()==0){
						System.out.println("Warning: detected unknown platform "+dir+" at "+platformDir+dir+"/");
						platformDirs.add(dirURL=(platformDir+dir+"/"));
						ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[4];
						nodeVector[0]=centerNodeVector[0];
						nodeVector[1]=centerNodeVector[1];
						nodeVector[2]=centerNodeVector[2];
						ArchiveCategoryStatisticsNode	node=platformNodes.get(dir);
						if(node==null){
							node=new	ArchiveCategoryStatisticsNode(dir,dir,CategoryType.PLATFORM,"");
						}
						nodeVector[3]=node;
						archives.put(dirURL, nodeVector);
					}
				}
			}
		}
	}

	private void validateCancerTypes(String cancerType,String	basicURL,File	cancerFile) {
		// TODO Auto-generated method stub
		String[]	cancers;
		
		if(cancerType.length()>0){
			System.out.println("Selecting cancer types:");
			String[]	preCancers=AsserUtil.split(cancerType);
			LinkedList<String>	cancerList=new	LinkedList<String>();
//			cancers=new	String[preCancers.length*2];
//			int	i=0;
			boolean	added=false;
			for(String	cancer:preCancers)
			{
				if(debug)
					System.out.println("### Cancer: "+cancer);
				if(DirBrowser.fileAccessible(basicURL+"/"+cancer.toLowerCase(),userName,passwd))
					{cancerList.add(cancer);added=true;}
				if(DirBrowser.fileAccessible(basicURL+"/"+cancer.toLowerCase()+"_p",userName,passwd))
					{cancerList.add(cancer+"_p");added=true;}
				if(DirBrowser.fileAccessible(basicURL+"/"+cancer.toLowerCase()+"_P",userName,passwd))
					{cancerList.add(cancer+"_P");added=true;}
				if(!added)
					{
						cf.writeError("directory entry for "+cancer+" not accessible");
						System.exit(1);
					}
					//System.out.println("ERROR: directory entry for "+cancer+" not accessible");
			}
			
			cancers=cancerList.toArray(preCancers);
		}
		else
		{
			System.out.println("Selecting all cancer types from all the archives:");
			DirBrowser	cancersDir=new	DirBrowser(basicURL,userName,passwd);
			LinkedList<String>	cancerFolders=cancersDir.browseDirsOnly();
			cancers=new	String[cancerFolders.size()];
			int	i=0;
			for(String	element:cancerFolders){
				if(debug)
					System.out.println("### Cancer: "+element);
				cancers[i++]=element.toUpperCase();
			}
		}
		for(String	cancer:cancers)
		{
			if(debug)
				System.out.println(" ### reporter.addCancer("+cancer+")");
			reporter.addCancer(cancer);
		}
		cancerDirs=new	LinkedList<String>();
		cancerNodes=new	Hashtable<String,ArchiveCategoryStatisticsNode>();
		for(String	cancer:cancers){
			String	cancerDir=cancer.toLowerCase();
			String	cancerURLString=basicURL+cancerDir+"/";
			String cancerTitle="";
			if(DirBrowser.fileAccessible(cancerURLString,userName,passwd)){
				cancerDirs.add(cancerURLString);
				ArchiveCategoryStatisticsNode[]	nodeVector=new	ArchiveCategoryStatisticsNode[1];
				ArchiveCategoryStatisticsNode	node;
				cancerNodes.put(cancer,node= new ArchiveCategoryStatisticsNode(cancer,
						cancerTitle=Codes2Titles.cancerTitle(cancerFile, cancer), 
						CategoryType.CANCER_TYPE,cancerURLString));
				nodeVector[0]=node;
				archives.put(cancerURLString, nodeVector);
				if(cancer.equals(cancerTitle))System.out.println("Warning: Did not recognize cancer type "+
						cancer+", but still selected");
				else
					System.out.println(cancer+" - "+cancerTitle);
			} else {
				// TODO Auto-generated catch block
				cf.writeError("Failed to open directory " +cancerURLString+" for "+cancer);
//				System.out.println("Failed to open directory " +cancerURLString+" for "+cancer);
				System.exit(1);
			}
		}
		
	}

	public void generateAliquotReferenceTable(File aliquotReferenceFile, IndexFile array,String arrayKey) {
		final	int	CODE_IDX=0;
		final	int	ANALYTE_CODE_IDX=1;
		final	int	PATIENT_CODE_IDX=2;
		final	int	LEVEL_UNKNOWN_IDX=3;
		final	int	LEVEL1_IDX=3;
		final	int	LEVEL2_IDX=LEVEL1_IDX;
		final	int	LEVEL3_IDX=LEVEL1_IDX;
		final	int	LEVEL4_IDX=LEVEL1_IDX;
		final	int	AUX_IDX=LEVEL1_IDX;
		final	int	MISC_IDX=LEVEL1_IDX;
		final	int	COLUMN_COUNT=MISC_IDX+1;
		
		String[]	tableHeader=new	String[COLUMN_COUNT];
		LinkedList<String[]>	refTable=new	LinkedList<String[]>();
		LinkedList<String>	addedAliquots=new	LinkedList<String>();
		
		tableHeader[CODE_IDX]="AliquotCode";
		tableHeader[ANALYTE_CODE_IDX]="AnalyteCode";
		tableHeader[PATIENT_CODE_IDX]="PatientCode";
//		tableHeader[LEVEL_UNKNOWN_IDX]="LevelUnknown";
		tableHeader[LEVEL1_IDX]="DataFiles";
//		tableHeader[LEVEL2_IDX]="Level2";
//		tableHeader[LEVEL3_IDX]="Level3";
//		tableHeader[LEVEL4_IDX]="Level4";
//		tableHeader[AUX_IDX]="Aux";
//		tableHeader[MISC_IDX]="Misc";
		refTable.add(tableHeader);
		Collections.sort(selectedAliquots);
		for(String	aliquot:selectedAliquots){
			
			String[]	line=new	String[COLUMN_COUNT];
			LinkedList<String>	levelUnList=new	LinkedList<String>();
			LinkedList<String>	level1List=new	LinkedList<String>();
			LinkedList<String>	level2List=new	LinkedList<String>();
			LinkedList<String>	level3List=new	LinkedList<String>();
			LinkedList<String>	level4List=new	LinkedList<String>();
			LinkedList<String>	auxList=new	LinkedList<String>();
			LinkedList<String>	miscList=new	LinkedList<String>();
			if(debug)System.out.println(aliquot);
			line[CODE_IDX]=aliquot;
			line[ANALYTE_CODE_IDX]=aliquot.substring(0,projectName.length()+16);
			line[PATIENT_CODE_IDX]=aliquot.substring(0,projectName.length()+8);
			aliquotFiles.put(aliquot, removeDuplicates(aliquotFiles.get(aliquot)));
			for(String	file:aliquotFiles.get(aliquot)){
				if(debug)System.out.println("Reference: "+file);
				if(outputFiles.contains(file))
					if(filesLevelUnknown.contains(file))
						levelUnList.add(file);
					else
						if(filesLevel1.contains(file))
							level1List.add(file);
						else
							if(filesLevel2.contains(file))
								level2List.add(file);
							else
								if(filesLevel3.contains(file))
									level3List.add(file);
								else
									if(filesLevel4.contains(file))
										level4List.add(file);
									else
										if(filesAux.contains(file))
											auxList.add(file);
										else
											miscList.add(file);
			}
			line[LEVEL_UNKNOWN_IDX]="";
			for(String	file:levelUnList){
				if(line[LEVEL_UNKNOWN_IDX].length()>0)
					line[LEVEL_UNKNOWN_IDX]+=",";
				line[LEVEL_UNKNOWN_IDX]+=file;
			}
//			line[LEVEL1_IDX]="";
			for(String	file:level1List){
				if(line[LEVEL1_IDX].length()>0)
					line[LEVEL1_IDX]+=",";
				line[LEVEL1_IDX]+=file;
			}
//			line[LEVEL2_IDX]="";
			for(String	file:level2List){
				if(line[LEVEL2_IDX].length()>0)
					line[LEVEL2_IDX]+=",";
				line[LEVEL2_IDX]+=file;
			}
//			line[LEVEL3_IDX]="";
			for(String	file:level3List){
				if(line[LEVEL3_IDX].length()>0)
					line[LEVEL3_IDX]+=",";
				line[LEVEL3_IDX]+=file;
			}
//			line[LEVEL4_IDX]="";
			for(String	file:level4List){
				if(line[LEVEL4_IDX].length()>0)
					line[LEVEL4_IDX]+=",";
				line[LEVEL4_IDX]+=file;
			}
//			line[AUX_IDX]="";
			for(String	file:auxList){
				if(line[AUX_IDX].length()>0)
					line[AUX_IDX]+=",";
				line[AUX_IDX]+=file;
			}
//			line[MISC_IDX]="";
			for(String	file:miscList){
				if(line[MISC_IDX].length()>0)
					line[MISC_IDX]+=",";
				line[MISC_IDX]+=file;
			}
			refTable.add(line);
			
			String	key=aliquot;
			String	regexp=key;
			if(arrayKey.equalsIgnoreCase("analyte"))
			{
				key=aliquot.substring(0,aliquot.length()-8);
				regexp=key+"-\\w\\w\\w\\w-\\w\\w";
			}
			else
				if(arrayKey.equalsIgnoreCase("sample")){
					key=aliquot.substring(0,aliquot.length()-12);
					regexp=key+"-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w";
				}
				else
					if(arrayKey.equalsIgnoreCase("patient")){
						key=aliquot.substring(0,aliquot.length()-16);
						regexp=key+"-\\w\\w\\w-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w";
					}
			if(debug)System.out.println("regexp="+regexp);
			if(!TextUtils.collectionContains(addedAliquots, regexp))
			{
				String[]	filteredAliquots=TextUtils.filterStringCollection(aliquotFiles.keySet(),regexp);
				addedAliquots.addAll(Arrays.asList(filteredAliquots));
				LinkedList<String>	files=new	LinkedList<String>();
			
				for(String	curAliquot:filteredAliquots)
				{
					files.addAll(aliquotFiles.get(curAliquot));
				}
				if(files.size()==1){
					File	firstFile=new	File(levelFiles.getAbsoluteFile()+File.separator+files.getFirst());
					String	fileName=files.getFirst();
					if(debug)System.out.println(" ### Checking if file is in selected level: "+fileName);
					if(outputFiles.contains(fileName)){
						array.add(key, firstFile);
						if(debug)System.out.println("Adding to array: "+key+"="+firstFile.getAbsolutePath());
					}
				}
				else
				{
					int	i=1;
					for(String	fileName:files)
					{
						String	altKey=key+"_"+i+"_"+files.size();
						File	itemFile=new	File(levelFiles.getAbsoluteFile()+File.separator+fileName);
						if(debug)System.out.println(" ### Checking if file is in selected level: "+fileName);
						if(outputFiles.contains(fileName)){
							array.add(altKey, itemFile);
							if(debug)System.out.println("Adding to array: "+altKey+"="+itemFile.getAbsolutePath());
							i++;
						}
					}
				}
			}
		}
		try {
				array.write(cf,"array");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		try {
			FileWriter	fout=new	FileWriter(aliquotReferenceFile);
			for(String[]	line:refTable){
				String	str="";
				for(String	element:line){
					str+=((str.length()>0)?"\t":"")+element;
				}
				fout.write(str+"\n");
			}
			fout.flush();
			fout.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
	}

	public void generateFileReferenceTable(File fileReferenceFile) {
		// TODO Auto-generated method stub
		final	int	FILE_NAME_IDX=0;
		final	int	DATA_LEVEL_IDX=FILE_NAME_IDX+1;
		final	int	CANCER_TYPE_IDX=DATA_LEVEL_IDX+1;
		final	int	CENTER_TYPE_IDX=CANCER_TYPE_IDX+1;
		final	int	CENTER_IDX=CENTER_TYPE_IDX+1;
		final	int	PLATFORM_IDX=CENTER_IDX+1;
		final	int	DATA_TYPE_IDX=PLATFORM_IDX+1;
		final	int	ARCHIVE_CODE_IDX=DATA_TYPE_IDX+1;
		final	int	ARCHIVE_SERIAL_IDX=ARCHIVE_CODE_IDX+1;
		final	int	ARCHIVE_REVISION_IDX=ARCHIVE_SERIAL_IDX+1;
		final	int	ARCHIVE_SERIES_IDX=ARCHIVE_REVISION_IDX+1;
		final	int	ARCHIVE_FILE_NAME_IDX=ARCHIVE_SERIES_IDX+1;
		final	int	ALIQUOTS_IDX=ARCHIVE_FILE_NAME_IDX+1;
		final	int	COLUMN_COUNT=ALIQUOTS_IDX+1;
		
		String[]	tableHeader=new	String[COLUMN_COUNT];
		LinkedList<String[]>	refTable=new	LinkedList<String[]>();
		
		tableHeader[FILE_NAME_IDX]="FileName";
		tableHeader[DATA_LEVEL_IDX]="DataLevel";
		tableHeader[CANCER_TYPE_IDX]="CancerType";
		tableHeader[CENTER_TYPE_IDX]="CenterType";
		tableHeader[CENTER_IDX]="Center";
		tableHeader[PLATFORM_IDX]="Platform";
		tableHeader[DATA_TYPE_IDX]="DataType";
		tableHeader[ARCHIVE_CODE_IDX]="ArchiveCode";
		tableHeader[ARCHIVE_SERIAL_IDX]="ArchiveSerial";
		tableHeader[ARCHIVE_REVISION_IDX]="ArchiveRevision";
		tableHeader[ARCHIVE_SERIES_IDX]="ArchiveSeries";
		tableHeader[ARCHIVE_FILE_NAME_IDX]="ArchiveFileName";
		tableHeader[ALIQUOTS_IDX]="Aliquots";
		
		refTable.add(tableHeader);
		for(String	fullName:new	TreeSet<String>(archives.keySet())){
			String[]	line=new	String[COLUMN_COUNT];
			ArchiveCategoryStatisticsNode[]	vector=archives.get(fullName);
			
			if(vector.length==6)
				if(vector[5].category==CategoryType.MISC){
					line[FILE_NAME_IDX]=vector[5].code;
					line[DATA_LEVEL_IDX]=String.valueOf(vector[5].dataLevel);
					line[CANCER_TYPE_IDX]=vector[0].code.replaceAll("_(p|P)", "");
					line[CENTER_TYPE_IDX]=vector[1].code;
					line[CENTER_IDX]=vector[2].code;
					line[PLATFORM_IDX]=vector[3].code;
					line[DATA_TYPE_IDX]=vector[4]!=null?vector[4].code:"";
					line[ARCHIVE_CODE_IDX]="";
					line[ARCHIVE_FILE_NAME_IDX]=fullName;
					line[ALIQUOTS_IDX]="NA";
					refTable.add(line);
					if(debug)System.out.println("generateFileReferenceTable: adding archive "+fullName);
				}
		}
		
		for(String	dataFile:new	TreeSet<String>(dataFiles.keySet())){
			if(debug)System.out.println("generateFileReference: dataFile="+dataFile);
			String[]	line=new	String[COLUMN_COUNT];
			ArchiveCategoryStatisticsNode[]	vector=dataFiles.get(dataFile);
			
			line[FILE_NAME_IDX]=vector[6].code;
			line[DATA_LEVEL_IDX]=String.valueOf(vector[6].dataLevel);
			line[CANCER_TYPE_IDX]=vector[0].code.replaceAll("_(p|P)", "");
			line[CENTER_TYPE_IDX]=vector[1].code;
			line[CENTER_IDX]=vector[2].code;
			line[PLATFORM_IDX]=vector[3].code;
			line[DATA_TYPE_IDX]=vector[4].code;
			line[ARCHIVE_CODE_IDX]=vector[5].code.substring(0,vector[5].code.length()-7);
			line[ARCHIVE_FILE_NAME_IDX]=vector[6].url;
			String	aliquots="";
			for(String	aliquot:aliquotFiles.keySet()){
				LinkedList<String>	fileList=aliquotFiles.get(aliquot);
				if(fileList.contains(vector[6].code))
					aliquots+=(aliquots.length()>0?",":"")+aliquot;
			}
			line[ALIQUOTS_IDX]=aliquots;
			
			refTable.add(line);
			if(debug)System.out.println("generateFileReferenceTable: adding archive "+vector[6].url);
		}
		
		try {
			FileWriter	fout=new	FileWriter(fileReferenceFile);
			for(String[]	line:refTable){
				String	str="";
				for(String	element:line){
					str+=((str.length()>0)?"\t":"")+element;
				}
				fout.write(str+"\n");
//				System.out.println("Writintg: "+str);
			}
			fout.flush();
			fout.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	private	LinkedList<String>	removeDuplicates(LinkedList<String>	list){
		LinkedList<String>	list2=new	LinkedList<String>();
		
		for(String	element:list){
			if(!list2.contains(element))
				list2.add(element);
		}
		
		return	list2;
	}


}

