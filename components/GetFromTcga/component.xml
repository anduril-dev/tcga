<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>GetFromTcga</name>
    <version>1.6.11</version>
    <doc>
    TCGA data importer.
    Performs two functions:
    <ul>
    	<li>Imports the data from The Cancer Genome Atlas (TCGA) database.</li>
    
    	<li>Supports: mirroring to local file system, http, https and file protocols.</li>
    </ul>
    
    Typical use cases:
    	<ul>
    		<li> Importing the data directly from TCGA for further analysis: <i>data = GetFromTcga(noCache = false, cancerType = "cancer_types", platformType = "platform_types", ...)</i></li>
    		<li> Importing the data from a local mirror:   <i>data = GetFromTcga(remoteSite = "url_to_local_mirror", cancerType = "cancer_types", platformType = "platform_types", ...)</i></li>
    		<li> Creating/synchronizing the local mirror with the TCGA site: data <i>data = GetFromTcga(mirrorDir = "url_to_local_mirror", noOutput=true, cancerType = "cancer_types", platformType = "platform_types", ...)</i></li>
    	</ul>
    
    Data should be selected in the following order:
    <ol>
    	<li> Parameter <i>cancerType</i> - Choose cancer types (by default all cancers are selected); </li>
    	<li> Parameter <i>centerID</i> - For all the chosen cancer types, choose centers where the data of interest is stored (by default all centers are selected); </li>
    	<li> Parameter <i>platformType</i> - For all selected above cancers and centers choose platforms on which data of interest are obtained (by default all platforms are selected); </li>
    	<li> Parameter <i>dataType</i> - For all selected above cancers, centers and platforms choose types of the data of interest (by default all data types are considered); </li>
    	<li> Parameter <i>levelType</i> - For all selected above cancers, centers, platforms and data types choose levels of data of interest (by default all 4 levels + unknown + auxiliary data + misc. data are selected); </li>     	 
    </ol>
    This selection defines which .tar.gz files should be downloaded/unpacked. If all the parameters from above are left empty, then all .tar.gz files from the database are considered.
    The following selection criteria define which files exactly should be extracted into <i>dataFiles</i> folder from the selected .tar.gz files:
    <ol>
    	<li> Parameter <i>siteID</i> - Choose IDs of sites sources of the data (by default all the sites are considered) </li>
    	<li> Parameter <i>patientID</i> - Choose patients IDs whose data should be extracted </li>
    	<li> Parameter <i>sampleID</i> - Choose samples of interest </li>
    	<li> Parameter <i>portionID</i> - Choose portions of interest </li>
    	<li> Parameter <i>plateID</i> - Choose plates of interest </li>
    </ol>
    In other words, the selection criteria from above define the list of aliquots of interest. For the aliquot barcode format we refer to
    <a href="http://tcga-data.nci.nih.gov/docs/TCGA_Data_Primer.pdf" target="_new">http://tcga-data.nci.nih.gov/docs/TCGA_Data_Primer.pdf</a>
    "About Aliquot Barcodes". 
    
    This component provides four output ports: 
    <ul>
    	<li><i>dataFiles</i> with extracted files with the data of interest;</li> 
    	<li><i>aliquotReference</i> table which associates to each aliquot the list of its data files;</li>
    	<li><i>fileReference</i> annotation table for all the extracted files.</li>
    	<li><i>report</i> information on downloaded files and inconsistences discovered in the database</li>
    </ul> 
    </doc>
    <author email="vladimir.rogojin@helsinki.fi">Vladimir Rogojin</author>
    <category>Data Import</category>
    <launcher type="java">
        <argument name="class" value="GetFromTcga" />
        <argument name="source" value="GetFromTcga.java" />
    </launcher>
    <requires type="jar" URL="http://commons.apache.org/codec/">commons-codec-1.4.jar</requires>
    <requires type="jar" URL="http://commons.apache.org/compress/">commons-compress-1.1.jar</requires>
    <requires type="jar" URL="http://htmlparser.sourceforge.net/project-info.html">htmllexer.jar</requires>
    <requires type="jar" URL="http://htmlparser.sourceforge.net/project-info.html">htmlparser.jar</requires>
    <requires type="jar">csbl-javatools.jar</requires>
    <requires type="jar">tcga.jar</requires>
    <type-parameters>
    	<type-parameter	name="T1" />
    </type-parameters>
    <inputs>
    	<input	name="archivesQuery"	type="Properties" optional="true">
    		<doc>Defines selection criteria for archives. Keys: serialNumber, 
        		outputRevision, cancerType, platformType, invertPlatformSelection, dataType, 
        		invertDataType, submissionDate, levelType. All other keys are ignored.
        		These criteria are overridden by the equally named parameters. 
        		</doc>
    	</input>
        <input	name="aliquotsQuery"	type="Properties" optional="true">
        	<doc>Defines selection criteria for aliquots. Keys: samplesID, projectName, siteID, 
        		patientID, sampleID, portionID, plateID, centerID. All other keys are ignored. 
        		These criteria are overridden by the equally named parameters. 
        		</doc> 
        </input>
        <input	name="aliquots"	type="IDList" optional="true">
        	<doc>List partial aliquot codes for files to be imported. This list overrides aliquots 
        	selected in <i>aliquotsQuery</i> inport as well as by <i>samplesID</i>, <i>projectName</i>, <i>siteID</i>, 
        	<i>patientID</i>, <i>sampleID</i>, <i>portionID</i>, <i>plateID</i> and <i>centerID</i> parameters.
        	</doc>
        </input>
        <input	name="connection"	type="Properties"	optional="true">
        	<doc>Defines the way to access functional TCGA data files. Currently supports just direct access 
        	to TCGA functional data storage. The URL of the storage should be provided by <i>site.url</i>.
        	The access parameters by <i>connection</i> port override <i>protectedSite</i> and
        	<i>remoteProtectedSite</i> parameters.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="dataFiles" type="BinaryFolder">
            <doc>The folder with the imported data files</doc>
        </output>
        <output name="aliquotReference" type="CSV">
        	<doc>Aliquot barcode to files reference table</doc>
        </output>
        <output name="fileReference" type="CSV">
        	<doc>Annotation table for imported data files</doc>
        </output>
        <output	name="report" type="HTML">
        	<doc>Generates default report and provides necessary source files to generate customized reports.<br></br>
        		The following information is included into the report:
        		<ul>
        			<li>In case mirror was enabled, the newly downloaded files into the mirror</li>
        			<li>In case the output was enabled, the archives considered for the output</li> 
        			<li>Orphan files</li>
        			<li>Mage-tab inconsistences</li>
        		</ul>
        		Files included into the report:
        		<ul>
        			<li>index.html - default report</li>
        			<li>header.html - the header of index.html</li>
        			<li>footer.html - the footer of index.html</li>
        			<li>downloaded.html, downloaded.csv - newly downloaded files</li>
        			<li>included.html, included.csv - archives considered for output</li>
        			<li>orphans.html, orphans.csv - orphan files</li>
        			<li>bad_mage_tab.html, bad_mage_tab.csv - mage-tab inconsistences</li>
        			<li>cancers.csv, cancers.html - cancers considered in teh query</li>
        			<li>centerID.csv, centerID.html - centers considered in the query</li>
        			<li>data_levels.csv, data_levels.html - data levels considered in the query</li>
        			<li>data_types.csv, data_types.html - data types considered in the query</li>
        			<li>platforms.csv, platforms.html - platforms considered in the query</li> 
        		</ul>
        		Note that files downloaded.html and included.html can be generated from downloaded.csv, 
        			included.csv and fileReference outPort, as well as from cancers.csv, centerID.csv, data_levels.csv, data_types.csv, platforms.csv. 
        	</doc>
        </output>
        <output	name="array"	type="T1"	array="true">
        	<doc>Array of data files being imported. Format &lt;aliquot_code&gt; = &lt;data_file&gt;. 
        		Note: <i>useArray</i> parameter should be set to true and the query should produce at most one output data file 
        		per each aliquot.</doc>
        </output>
        <output	name="readme"	type="TextFile"		array="true">
        	<doc>Array of readme files per each tar.gz archive used to import the data. Format: &lt;archive_code&gt;=&lt;readme_file&gt;.</doc> 
        </output>
        <output	name="readme_dcc"	type="TextFile"		array="true">
        	<doc>Array of readme_dcc files per each tar.gz archive used to import the data. Format: &lt;archive_code&gt;=&lt;readme_dcc_file&gt;</doc> 
        </output>
        <output	name="description"	type="TextFile"		array="true">
        	<doc>Array of archive description files per each tar.gz archive used to import the data. Format: &lt;archive_code&gt;=&lt;description_file&gt;.</doc> 
        </output>
    </outputs>
    <parameters>
    	<parameter type="string" name="remoteSite" default="https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/anonymous/tumor/">
    		<doc>URL of the remote public database (By default it is the public TCGA site: <a href="https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/anonymous/tumor/">https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/anonymous/tumor/</a>).
    		Currently http, https and file protocols are supported.
    		The public site is accessed if no authentification data are provided, otherwise the private site is accessed</doc>
    	</parameter>
    	<parameter type="string" name="remoteProtectedSite" default="https://tcga-data-secure.nci.nih.gov/tcgafiles/tcga4yeo/tumor/">
    		<doc>URL of the remote protected database (By default it is the private TCGA site: <a href="https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/tcga4yeo/tumor/">https://tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/tcga4yeo/tumor/</a>).
    		Currently http, https and file protocols are supported.
			It is accessed if the authentification data are provided. The authentification files should be generated by <a href="tcgapasswd.html">TCGAPasswd</a> java application.</doc>
    	</parameter>
    	<parameter type="string" name="authFile" default="">
            <doc>The encrypted file with the authentification data to access protected area from TCGA. Use java program <a href="tcgapasswd.html">TCGAPasswd</a> to generate it.</doc>
        </parameter> 
        <parameter type="string" name="keyFile" default="">
            <doc>The file with the key to decrypt the authentification file. Use java program <a href="tcgapasswd.html">TCGAPasswd</a> to generate the key file.</doc>
        </parameter>
    	<parameter type="string" name="mirrorDir" default="">
    		<doc>URL of the directory where the mirror with the archives from TCGA should be stored.
    			Currently only local file system for the mirror is supported (Use mirrorDir="file:/&lt;absolute path to your mirror&gt;" to access your mirror at your local file system.)
    			This directory synchronizes with the remoteSite directory.
    			If left empty, no mirror is created/synchronized</doc>
    	</parameter>
    	<parameter	type="boolean" name="syncMirror" default="true">
    		<doc>Should all the files existing in the mirror be updated. Default - yes.
    		</doc>
    	</parameter>
    	<parameter	type="boolean" name="browseOnly" default="false">
    		<doc>If TRUE, tables aliquotReference and fileReference and the report are still generated.
    			However, no data files are downloaded/updated into the mirror and no data imports is performed (even in case noOutput=false). 
    			True value should be used only in order to "browse" the archive (both remote site and the mirror)
    			according to the selection criteria and to generate reference tables.
    		</doc>
    	</parameter>
    	<parameter	type="boolean" name="eraseMirror" default="false">
    		<doc>If true, then the mirror folder is erased prior to the data download 
    		(If you are executing this operation, do not forget to specify the mirror folders: mirrorDir and/or protectedMirrorDir)</doc>
    	</parameter>
    	<parameter	type="boolean" name="eraseCache" default="false">
    		<doc>If true, then the cache folder is erased prior to the data download</doc>
    	</parameter>
    	<parameter	type="boolean" name="noCache" default="true">
    		<doc>By default the cache is disabled. It might be beneficial to enable cache if the data are being fetched not from the local file system.
    			Physically, the cache folder is created in the temp directory under the component's execution folder.
    			The temp folder is cleared on component's exit.
    			Warning: currently there is no size limit control on the cache. Keep this in mind when working with large amount of data.
   				</doc>
    	</parameter>
    	<parameter	type="boolean" name="noOutput" default="false">
    		<doc>By default the output is enabled. If set TRUE, then the component does not import any files from the database, but still generates aliquotReference and fileReference tables. 
    		Disabling the output usually is used when creating a local mirror of the TCGA site.</doc>
    	</parameter>
    	<parameter type="boolean" name="verbose" default="false">
    		<doc>Verbose output</doc>
    	</parameter>
    	<parameter	type="boolean" name="importOrphans" default="false">
    		<doc>If TRUE, the data files which were not assigned to any of aliquots (orphans) will be imported. By default the orphans are not imported. 
    		</doc>
    	</parameter>
    	<parameter	type="string" name="serialNumber" default="">
    		<doc>By default samples from all the archives are selected. However, it is possible 
    		     to select the archives with the specific serial number.</doc>
    	</parameter>
    	<parameter	type="string" name="outputRevision" default="">
    		<doc>By default samples from the latest revision are imported. However, it is possible 
    		     to import samples from earlier revisions.</doc>
    	</parameter>
    	<parameter type="string" name="cancerType" default="">
    		<doc>Comma separated abbreviations of cancer types to be imported. By default all cancers are selected. 
    		<a href="cancers.html">The list of all supported cancer types</a> 
    		</doc>
    	</parameter>
    	<parameter type="string" name="platformType" default="">
    		<doc>Comma separated abbreviations for platforms. By default all platforms are selected.
    			<a href="platforms.html">The list of all supported platform types</a>  
    			</doc>
    	</parameter>
    	<parameter type="boolean" name="invertPlatformSelection" default="false">
    		<doc>If TRUE, then all platforms are selected except of those indicated in <i>platformType</i></doc>
    	</parameter>
    	<parameter type="string" name="dataType" default="">
    		<doc>Comma separated abbreviations for data types. By default all data types are selected.
    			<a href="data_types.html">The list of all supported data types</a>  
    		</doc>
    	</parameter>
    	<parameter type="boolean" name="invertDataType" default="false">
    		<doc>If TRUE, then all data types are selected except of those indicated in <i>dataType</i></doc>
    	</parameter>
    	<parameter type="string" name="submissionDate" default="">
    		<doc>The date of the submission of the archive (format DD-MM-YYYY)</doc>
    	</parameter>
    	<parameter type="string" name="levelType" default="all">
    		<doc>Comma separated data levels for output. By default <B>all</B> data levels are selected. 
    			<a href="data_levels.html">The list of accessible data levels</a>
    		</doc>
    	</parameter>
    	<parameter type="string" name="samplesID" default=".*">
    		<doc>Currently it is not supported.
    			</doc>
    	</parameter>
    	<parameter type="string" name="projectName" default="TCGA">
    		<doc>The name of the project. Currently only TCGA is available.</doc>
    	</parameter>
		<parameter type="string" name="siteID" default="">
    		<doc>Comma separated IDs of the site samples of interest are coming from. 
    			<a href="siteID.html">Currently there are available 6 sites (the list is subject of change)</a>
    		</doc>
    	</parameter>
    	<parameter type="string" name="patientID" default="">
    		<doc>Patient identifier that is associated with a Site ID. Allowed parameter values: 0001-9999</doc>
    	</parameter>
		<parameter type="string" name="sampleID" default="">
    		<doc>Sample type and sample vial identifiers. 
    			<a href="sampleID.html">The format of sample identifiers is here.</a>
    		</doc>
    	</parameter>
    	<parameter type="string" name="portionID" default="">
    		<doc>Individual 100 mg - 120 mg section of a sample. Consists of a portion code and an analyte code, 
    			<a href="portionID.html">look here for details.</a>
    			</doc>
    	</parameter>
    	<parameter type="string" name="plateID" default="">
    		<doc>Identifies individual plates. Allowed parameter values: 0001-9999</doc>
    	</parameter>
    	<parameter type="string" name="centerID" default="">
    		<doc>Identifies each of the CGCCs or GSCs. Center codes are <a href="centerID.html">here</a>.
       		</doc>
    	</parameter>
    	<parameter type="string" name="arrayKey"	default="aliquot">
    		<doc>Defines how imported files should be referenced in <i>array</i> output port. 
    			The following options for key values are available:
    			<i>aliquot</i>, <i>analyte</i>, <i>sample</i>, <i>patient</i>. By default, <i>aliquot</i> is chosen.
    			Note, that if more than a file are imported with the same key value <i>key</i>, then the key values for those 
    			files are altered as follows: <i>key_i_n</i>, where <i>n</i> is the total number of files with same <i>key</i>
    			and <i>i</i> an integer between <i>1</i> and <i>n</i>, it is the unique index for each file in the group with 
    			same <i>key</i>.
    		</doc> 
    	</parameter>
    	<parameter type="boolean" name="unpackReadme"	default="false">
    		<doc>If set true, <i>readme</i>, <i>readme_dcc</i> and <i>description</i> array output ports are populated with README.txt,
    		README_DCC.txt and DESCRIPTION.txt files respectively from the archives. 
    		Archive names serve as the key values in the arrays.
    		WARNING: if set true, the unpacking time may increase considerably (at most in three times). 
    		</doc> 
    	</parameter>
    	<parameter type="boolean" name="debug"	default="false">
    		<doc>If set TRUE, outputs debug messages</doc>
    	</parameter>
    </parameters>
</component>
