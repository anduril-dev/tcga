import java.io.ByteArrayInputStream;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.security.DigestInputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.KeyMapParser;
import fi.helsinki.ltdk.csbl.tcga.AuthUtils;
import fi.helsinki.ltdk.csbl.tcga.Cache;
import fi.helsinki.ltdk.csbl.tcga.Mirror;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;


public class GetFromTcga extends SkeletonComponent{

	/**
	 * @param args
	 */
	
	public	static			boolean	standAlone=false;
	public	static			boolean	verbose=false;
	public	static	final	String	CANCER_TYPES_FILE = "CancerTypes.txt";
	public	static	final	String	PLATFORM_TYPES_FILE = "PlatformTypes.txt";
	public	static	final	String	DATA_TYPES_FILE = "DataTypes.txt";
	public	static	final	String	PROJECT_NAMES_FILE = "ProjectNames.txt";
	public	static	final	String	SITE_IDS_FILE = "SiteIDs.txt";
	public	static	final	String	CENTER_IDS_FILE = "CenterIDs.txt";
	public	static	File	tmpDir;

//	MyOutput	myOutput;
	
	File	aliquotsQuery;
	File	archivesQuery;
	File	aliquots;
	File	dataFilesDir;
	File	aliquotReferenceFile;
	File	fileReferenceFile;
	File	logFile;
	File	errorFile;
	File	reportFile;
	File	connection;
	IndexFile	array;
	IndexFile	readme;
	IndexFile	readme_dcc;
	IndexFile	description;
	String	remoteSite;
	String	authFileName;
	String	keyFileName;
	String	remoteProtectedSite;
	String	mirrorDir;
//	String	protectedMirrorDir;
	String	syncMirror;
	String	noCache;
	String	noOutput;
	String	serialNumber;
	String	outputRevision;
	String	cancerType;
	String	platformType;
	String	dataType;
	String	submissionDate;
	String	levelType;
	String	samplesID;
	String	projectName;
	String	siteID;
	String	patientID;
	String	sampleID;
	String	portionID;
	String	plateID;
	String	centerID;
	String	arrayKey;
	boolean	eraseCache;
	boolean	eraseMirror;
	boolean	importOrphans;
	boolean	browseOnly;
	boolean	invertPlatformSelection;
	boolean	invertDataType;
	boolean	unpackReadme;
	boolean	debug;
	
	File	cancerFile=new	File(CANCER_TYPES_FILE);
	File	platformFile=new	File(PLATFORM_TYPES_FILE);
	File	dataFile=new	File(DATA_TYPES_FILE);
	File	projectFile=new	File(PROJECT_NAMES_FILE);
	File	siteFile=new	File(SITE_IDS_FILE);
	File	centerFile=new	File(CENTER_IDS_FILE);
	File	authFile;
	File	keyFile;
	File	readmeDir;
	File	readmeDCCDir;
	File	descriptionDir;
	
	boolean	cacheActive;
	String	authData;
	String	userName;
	String	passwd;
	
	CommandFile	cf;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new	GetFromTcga().run(args);
	}
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
//		File	passwordFile=cf.getInput("password");
		this.cf=cf;
		aliquotsQuery=cf.getInput("aliquotsQuery");
		archivesQuery=cf.getInput("archivesQuery");
		aliquots=cf.getInput("aliquots");
		dataFilesDir=cf.getOutput("dataFiles");
		aliquotReferenceFile=cf.getOutput("aliquotReference");
		fileReferenceFile=cf.getOutput("fileReference");
		remoteSite=cf.getParameter("remoteSite");
		Map<String,String>	connectionMap;
		if(cf.inputDefined("connection"))
		{
			System.out.println("Parsing connection file");
			Properties	connection=new	Properties();
			connection.load(new FileInputStream(cf.getInput("connection")));
			if(connection.containsKey("site.url"))
				remoteSite=connection.getProperty("site.url");
		}
		logFile=cf.getLogFile();
		errorFile=cf.getErrorsFile();
		tmpDir=cf.getTempDir();
		reportFile=cf.getOutput("report");
//		array=cf.getOutput("array");
//		readme=cf.getOutput("readme");
//		readme_dcc=cf.getOutput("readme_dcc");
//		description=cf.getOutput("description");
		array=new	IndexFile();
		readme=new	IndexFile();
		readme_dcc=new	IndexFile();
		description=new	IndexFile();
		
		authFileName=cf.getParameter("authFile");
		keyFileName=cf.getParameter("keyFile");
		remoteProtectedSite=cf.getParameter("remoteProtectedSite");
		mirrorDir=cf.getParameter("mirrorDir");
//		protectedMirrorDir=cf.getParameter("protectedMirrorDir");
		syncMirror=cf.getParameter("syncMirror");
		noCache=cf.getParameter("noCache");
		noOutput=cf.getParameter("noOutput");
		serialNumber=cf.getParameter("serialNumber");
		outputRevision=cf.getParameter("outputRevision");
		cancerType=cf.getParameter("cancerType");
		platformType=cf.getParameter("platformType");
		dataType=cf.getParameter("dataType");
		submissionDate=cf.getParameter("submissionDate");
		levelType=cf.getParameter("levelType");
		samplesID=cf.getParameter("samplesID");
		projectName=cf.getParameter("projectName");
		siteID=cf.getParameter("siteID");
		patientID=cf.getParameter("patientID");
		sampleID=cf.getParameter("sampleID");
		portionID=cf.getParameter("portionID");
		plateID=cf.getParameter("plateID");
		centerID=cf.getParameter("centerID");
		eraseCache=cf.getBooleanParameter("eraseCache");
		eraseMirror=cf.getBooleanParameter("eraseMirror");
		importOrphans=cf.getBooleanParameter("importOrphans");
		browseOnly=cf.getBooleanParameter("browseOnly");
		invertPlatformSelection=cf.getBooleanParameter("invertPlatformSelection");
		invertDataType=cf.getBooleanParameter("invertDataType");
		verbose=cf.getBooleanParameter("verbose");
		arrayKey=cf.getParameter("arrayKey");
		unpackReadme=cf.getBooleanParameter("unpackReadme");
		debug=cf.getBooleanParameter("debug");
		
//		myOutput = new MyOutput(logFile,errorFile);
//		if(eraseCache)
//			eraseFolder(new	File("cache/"));
		System.out.println("****************************************************");
		System.out.println("* GetFromTcga v1.6.11                              *");
		System.out.println("****************************************************");
		System.out.println("");
		
		if(debug)
			System.out.println("DEBUG MODE ON!");
		
		authFile=(authFileName.length()>0)?new	File(authFileName):
			null;
		keyFile=(keyFileName.length()>0)?new	File(keyFileName):
			null;

		dataFilesDir.mkdirs();
		
		cf.getOutput("array").mkdirs();
		readmeDir=cf.getOutput("readme");
		readmeDir.mkdirs();
		readmeDCCDir=cf.getOutput("readme_dcc");
		readmeDCCDir.mkdirs();
		descriptionDir=cf.getOutput("description");
		descriptionDir.mkdirs();
		if(eraseMirror){
			URL	mirrorURL=mirrorDir.length()>0?new	URL(mirrorDir):null;
//			URL	protectedMirrorURL=protectedMirrorDir.length()>0?new	URL(protectedMirrorDir):null;
			if(mirrorURL!=null)
				if(mirrorURL.getProtocol().equalsIgnoreCase("file"))
					eraseFolder(new	File(new	URL(mirrorDir).getPath()));
//			if(protectedMirrorURL!=null)
//				if(protectedMirrorURL.getProtocol().equalsIgnoreCase("file"))
//					eraseFolder(new	File(new	URL(protectedMirrorDir).getPath()));
		}			
		boolean	cacheActive=noCache.equalsIgnoreCase("false")?true:(noCache.equalsIgnoreCase("true")?false:true);
		authData=getAuthData(authFile,keyFile);
		userName=getUserName(authData);
		passwd=getPasswd(authData);
		if(debug)
			System.out.println("User = "+userName+"\nPassword = "+passwd);
		
		if(userName.length()==0)
			processDB(false);
		if(userName.length()>0)
			processDB(true);
//		myOutput.close();
		return ErrorCode.OK;
	}
	
	private void eraseFolder(File	folder) {
		// TODO Auto-generated method stub
		
		if(folder.exists()){
			if(folder.isDirectory()){
				if(verbose)
					System.out.println("Erasing folder "+folder.getAbsolutePath());
				File[]	folderContent=folder.listFiles();
				for(File	item:folderContent)
					eraseFolder(item);
			}
			else
				if(verbose)
					System.out.println("Erasing file "+folder.getAbsolutePath());
			folder.delete();
		}
	}

	private String getPasswd(String authData) {
		// TODO Auto-generated method stub
		if(authData==null)
			return	"";
		int	p=0;
		if((p=authData.indexOf("\t"))>0)
		{
			return	new	String(AuthUtils.readArmoured(authData.substring(p+1)));
		}
		else
			{
			cf.writeError("Failed to decode properly authentification data: no password found");
			System.exit(ErrorCode.ERROR.getCode());
			}
		return "";
	}

	private String getUserName(String authData) {
		// TODO Auto-generated method stub
		if(authData==null)
			return	"";
		int	p=0;
		if((p=authData.indexOf("\t"))>0)
			return	authData.substring(0,p);
		else
		{
			cf.writeError("Failed to decode properly authentification data: no username found");
			System.exit(ErrorCode.ERROR.getCode());
		}
		return "";
	}

	private String getAuthData(File authFile, File keyFile) {
		// TODO Auto-generated method stub
		Key	key=AuthUtils.readKey(keyFile);
		if(key!=null)
		{
			return	AuthUtils.getAuth(authFile, key);
		}
		return null;
	}

	private	void	processDB(boolean	privateMode) throws IOException
	{
		Cache	tcgaCache=new	Cache(tmpDir.getAbsolutePath(),remoteSite,mirrorDir,userName,passwd,cacheActive);
		
		Mirror	mirror=null;

		Reporter	reporter=new	Reporter(reportFile,remoteSite,mirrorDir,noOutput.equalsIgnoreCase("true"),
				syncMirror.equalsIgnoreCase("true"),browseOnly);
		
		if(privateMode){
			System.out.println("------------------Processing private database--------------------");
			remoteSite=remoteProtectedSite;
		}
		else
			System.out.println("------------------Processing public database--------------------");
		System.out.println(remoteSite);
		if(privateMode)
			System.out.println("Connecting as user "+userName);
		System.out.println("");
		if(browseOnly)
			System.out.println("Browsing the database. Neither downloads, nor data imports will be performed...");
		if(mirrorDir.length()>0){
			mirror=new	Mirror(remoteSite,mirrorDir,tcgaCache,userName,passwd);
			if(syncMirror.equalsIgnoreCase("true"))
				mirror.buildSyncAllFileList();
		}
		
		AliquotSelector	aliquotSelector=new	AliquotSelector(remoteSite, userName, passwd, 
				importOrphans,cancerType, platformType, 
				invertPlatformSelection, dataType, invertDataType,"", 
				serialNumber, outputRevision, levelType, samplesID, projectName, siteID, patientID, 
				sampleID, portionID, plateID, centerID,
				aliquotsQuery, archivesQuery, aliquots,
				cancerFile,centerFile,platformFile,dataFile,
				readme,readme_dcc,description,
				dataFilesDir,readmeDir,readmeDCCDir,descriptionDir,
				tcgaCache,reporter,unpackReadme,cf,debug);
		if(mirror!=null){
			mirror.setFilesToDownload(aliquotSelector.getFiles2Download());
			if(!browseOnly)
				mirror.downLoadAll();
		}
		aliquotReferenceFile.createNewFile();
		fileReferenceFile.createNewFile();
		aliquotSelector.generateAliquotReferenceTable(aliquotReferenceFile,array,arrayKey);
		aliquotSelector.generateFileReferenceTable(fileReferenceFile);
		if(noOutput.equalsIgnoreCase("false")&&!browseOnly){
			if(mirror!=null)
				aliquotSelector.downloadUnpackFiles(remoteSite,mirrorDir);
			else
				aliquotSelector.downloadUnpackFiles();
		}
		else
			System.out.println("No Output selected");
		reporter.generateReport();
	}
	

	
}
