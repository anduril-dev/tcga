#!/bin/bash

# Use this script to generate a user/password authentification file 
# for GetFromTcga to access the TCGA private data-portal site
# Note: Environmental variable $ANDURIL_BUNDLES should indicate the 
# directory containing TCGA bundle as a subdirectory!

java -cp $ANDURIL_BUNDLES/tcga/lib/java/tcga.jar:$ANDURIL_BUNDLES/tcga/components/GetFromTcga/ TCGAPasswd $1 $2 $3 $4 $5 $6


