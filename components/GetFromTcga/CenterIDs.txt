Code	Center Name	Center Type	Display Name	Short Name
01	broad.mit.edu	CGCC	Broad Institute of MIT and Harvard	BI
02	hms.harvard.edu	CGCC	Harvard Medical School	HMS
03	lbl.gov	CGCC	Lawrence Berkeley National Laboratory	LBL
04	mskcc.org	CGCC	Memorial Sloan-Kettering Cancer Center	MSKCC
05	jhu-usc.edu	CGCC	Johns Hopkins / University of Southern California	JHU_USC
06	hudsonalpha.org	CGCC	HudsonAlpha Institute for Biotechnology	HAIB
07	unc.edu	CGCC	University of North Carolina	UNC
08	broad.mit.edu	GSC	Broad Institute of MIT and Harvard	BI
09	genome.wustl.edu	GSC	Washington University School of Medicine	WUSM
10	hgsc.bcm.edu	GSC	Baylor College of Medicine	BCM
11	rubicongenomics.com	COM	Rubicon Genomics	RG
12	hgsc.bcm.edu	CGCC	Baylor College of Medicine	BCM
13	bcgsc.ca	CGCC	Canada's Michael Smith Genome Sciences Centre	BCGSC
14	broad.mit.edu	GDAC	Broad Institute of MIT and Harvard	BI
15	systemsbiology.org	GDAC	Institute for Systems Biology	ISB
16	lbl.gov	GDAC	Lawrence Berkely National Laboratory	LBL
17	mskcc.org	GDAC	Memorial Sloan-Kettering Cancer Center	MSKCC
18	ucsc.edu	GDAC	University of California, Santa Cruz	UCSC
19	mdanderson.org	GDAC	MD Anderson	MD
20	nationwidechildrens.org	BCR	Nationwide Children's Hospital	
21	intgen.org	BCR	International Genomics Consortium	
