import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.Tools;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.tcga.AuthUtils;
import fi.helsinki.ltdk.csbl.tcga.Cache;


public class URLGet extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new	URLGet().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		File	authFile=cf.getInput("authFile");
		File	keyFile=cf.getInput("keyFile");
		
		String	authData=AuthUtils.getAuthData(authFile, keyFile);
		String	userName=AuthUtils.getUserName(authData);
		String	passwd=AuthUtils.getPasswd(authData);
		
		CSVParser	downList=cf.getInput("downList")!=null?
			new	CSVParser(cf.getInput("downList")):
				null;
		
		
		Cache.setTriesLimit(cf.getIntParameter("tries"));
			
		IndexFile	files=new	IndexFile();
		
		cf.getOutput("files").mkdirs();
		
		String	defaultDestDir=cf.getParameter("destFolder").length()>0?
				cf.getParameter("destFolder"):
					cf.getOutput("files").getAbsolutePath();
		
		if(cf.getParameter("URL").length()>0)
		{
			File	destFile=cf.getParameter("destFile").length()>0?
						new	File(cf.getParameter("destFile")):null;
			URL	url=new	URL(cf.getParameter("URL"));
			files.add(resolveKey(cf.getParameter("key"),destFile,url), 
					downloadFile(cf.getTempDir().getAbsolutePath(),defaultDestDir,url,destFile,userName,
							passwd,cf.getBooleanParameter("useCache"), cf.getIntParameter("tries")));
		}
		
		if(downList!=null){
			int	remoteURLColIdx=cf.getParameter("remoteURLColName").length()>0?
				downList.getColumnIndex(cf.getParameter("remoteURLColName")):0;
			int	destPathsColIdx=cf.getParameter("destPathsColName").length()>0?
						downList.getColumnIndex(cf.getParameter("destPathsColName")):-1;
			int	keyColIdx=cf.getParameter("keyColName").length()>0?
								downList.getColumnIndex(cf.getParameter("keyColName")):-1;
			for(String[]	row:downList)
			{
				File	destFile=null;
				if(destPathsColIdx>-1)
					if(row[destPathsColIdx]!=null)
						if(row[destPathsColIdx].length()>0)
							destFile=new	File(row[destPathsColIdx]);
				String	key=cf.getParameter("key");
				if(keyColIdx>-1)
					if(row[keyColIdx]!=null)
						if(row[keyColIdx].length()>0)
							key=row[keyColIdx];
				URL	url=new	URL(row[remoteURLColIdx]);
				files.add(resolveKey(key,destFile,url), 
						downloadFile(cf.getTempDir().getAbsolutePath(),defaultDestDir,url,destFile,
								userName,passwd,cf.getBooleanParameter("useCache"),
									cf.getIntParameter("tries")));
			}
		}
		files.write(cf, "files");
		
		return ErrorCode.OK;
	}
	
	private	File	downloadFile(String	tmpDir,String	defaultDestDir,URL	url,File	destFile,String	userName,String	passwd,boolean useCache,int tries) throws FileNotFoundException, IOException
	{
		Cache	cache;
		if(destFile!=null)
			destFile=new	File(destFile.getAbsolutePath().replaceAll(".*~", System.getProperties().getProperty("user.home")));
		
		if(url.getProtocol().equalsIgnoreCase("http")||url.getProtocol().equalsIgnoreCase("https"))
		{
			cache=new	Cache(tmpDir,new File(url.toString()).getParent(),"",userName,passwd,useCache);
			if(destFile==null){
				destFile=new	File(defaultDestDir+File.separator+new File(url.toString()).getName());
				if(destFile.exists())
					destFile=new	File(destFile.getParent()+File.separator+
							Integer.toString(url.toString().hashCode()).replaceAll("-", "")+
								destFile.getName());
			}
			destFile.getParentFile().mkdirs();
			int	c=0;
			InputStream	in;
			do{
				in=cache.readFileFromURL(url.toString());
				c++;
			}while((in==null)&&(c<tries));
			if(in==null){
				System.out.println("ERROR: could not get stream "+url);
				return null;
			}
			Tools.copyStreamToFile(in, destFile);
			destFile.setLastModified(Cache.getLastModified(url.toString(), userName, passwd));
			return	destFile;
				
		}
		else{
			if(url.getProtocol().equalsIgnoreCase("file"))
			{
				if(destFile!=null){
					destFile.getParentFile().mkdirs();
					File	locFile;
					Tools.copyFile(locFile=new	File(url.getFile()), destFile);
					destFile.setLastModified(locFile.lastModified());
					return	destFile;
				}
				else
					return	new	File(url.getFile().replaceAll(".*~", System.getProperties().getProperty("user.home"))).getAbsoluteFile();
			}
			else{
				System.out.println("ERROR: unsupported protocol "+url.getProtocol());
			}
		}
		return	null;
	}
	
	private	String	resolveKey(String	keyPattern,File	file,URL	url)
	{
		String	fileName=(file!=null)?
					file.getName():
						new	File(url.toString()).getName();
		String[]	fileNameParts=fileName.split("\\.");
		String	name=fileNameParts[0];
		for(int	i=1;i<fileNameParts.length-1;i++)
			name+="."+fileNameParts[i];
		name=name.replaceAll("[^a-zA-Z0-9_]", "_");
		String	ext=(fileNameParts.length>1)?
					fileNameParts[fileNameParts.length-1]:"";
		if(ext.equals(fileName))
			ext="";
		String	hash=Integer.toString(url.toString().hashCode()).replaceAll("-", "");
		String	localhash=(file!=null)?
				Integer.toString(file.getAbsolutePath().toString().hashCode()).replaceAll("-", ""):
					hash;
		return	keyPattern.replaceAll("@name@",name).replaceAll("@ext@", ext).replaceAll("@hash@", hash).replaceAll("@localhash@", localhash);
	}

}
