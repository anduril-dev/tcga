import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class CGHubXMLCSV2Map extends SkeletonComponent {

	class	FileRecord {
		public	String	fileName;
		public	String	fileSize;
		public	String	checksum;
		
		public	FileRecord(){
			fileName="NA";
			fileSize="NA";
			checksum="NA";
		}
	}
	
	class	AlignmentRecord {
		public	String	shortName;
		
		public	AlignmentRecord(){
			shortName="NA";
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	CGHubXMLCSV2Map().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		CSVParser	csv=new	CSVParser(cf.getInput("csv"));
		
		
		String[]	columns={"name","type","disease","patient","sample","sample_type","sample_type_code","analyte_type",
				"run","read","analysis_id","analysis_accession","study","sample_accession","participant_id","tss_id",
				"last_modified","center_name","alias","Title","Description","analysis_type","library_strategy",
				"reference_alignment","platform","portal_src",
				"filesize","MD5","SOURCE_XML"};
		
		CSVWriter	map=new	CSVWriter(columns,cf.getOutput("map"),true);
		
//		LinkedList<String[]>	csvTable=new	LinkedList<String[]>();
		LinkedList<LinkedList<String[]>>	resultTables=new	LinkedList<LinkedList<String[]>>();
		
		int	nameIdx=csv.getColumnIndex("#NAME#");
		int	tidIdx=csv.getColumnIndex("#TID#");
		int	pidIdx=csv.getColumnIndex("#PID#");
		int	ppidIdx=csv.getColumnIndex("#PPID#");
		int	contentIdx=csv.getColumnIndex("#CONTENT#");
//		System.out.println("nameIdx="+nameIdx);
		
		List<String>	columnNames=Arrays.asList(csv.getColumnNames());
		
		int	accessionIdx=-1;
		if(columnNames.contains("accession"))
			accessionIdx=csv.getColumnIndex("accession");
		int	aliasIdx=-1;
		if(columnNames.contains("alias"))
			aliasIdx=csv.getColumnIndex("alias");
		int	shortNameIdx=-1;
		if(columnNames.contains("short_name"))
			shortNameIdx=csv.getColumnIndex("short_name");
		
		LinkedList<String>	resultIDs=new	LinkedList<String>();
		
/*		for(String[]	row:csv)
		{
			System.out.println(row[0]);
		} */
		
		String	source_xml="NA";
		
		LinkedList<String[]>	resTable=null;
		String	tid="";
		for(String[]	row:csv)
		{
			if(row[nameIdx].equals("SOURCE_XML"))
				source_xml=row[contentIdx];
			if(row[nameIdx].equalsIgnoreCase("result")){
				resultIDs.add(row[tidIdx]);
				resTable=new	LinkedList<String[]>();
				resultTables.add(resTable);
				tid=row[tidIdx];
			}
			else
				if((resTable!=null)&&
						((row[ppidIdx].matches(".*\\."+tid+"\\..*"))||
								(row[ppidIdx].matches(".*\\."+tid))))
					resTable.add(row);
//			csvTable.add(row);
//			System.out.println(row[nameIdx]);
		}
		
//		System.out.println("Number of results: "+resultIDs.size());
		
//		System.out.println("Results: "+resultIDs.size()+", csvTable size: "+csvTable.size());
		for(LinkedList<String[]>	resultTable:resultTables)
		{
//			System.out.println("result: "+resultID);
//			LinkedList<String>	fileIDs=new	LinkedList<String>();
			
//			LinkedList<String[]>	resultTable=new	LinkedList<String[]>();
			
/*			for(String[]	row:csvTable)
			{
				if((row[ppidIdx].matches(".*\\."+resultID+"\\..*"))||
						(row[ppidIdx].matches(".*\\."+resultID)))
							resultTable.add(row);
			}*/
			
			Hashtable<String,FileRecord>	files=new	Hashtable<String,FileRecord>();
			Hashtable<String,AlignmentRecord>	r_alignment=new	Hashtable<String,AlignmentRecord>();
			String	disease="NA";
			String	sample="NA";
			String	sample_type_code="NA";
			String	analyte_type="NA";
			String	run_alias="NA";
			String	run="NA";
			String	analysis_id="NA";
			String	sample_accession="NA";
			String	study="NA";
			String	participant_id="NA";
			String	tss_id="NA";
			String	last_modified="NA";
			String	center_name="NA";
			String	Title="NA";
			String	Description="NA";
			String	library_strategy="NA";
			String	platform="NA";
			String	analysis_data_uri="NA";
			String	reference_alignment="NA";
			
			LinkedList<String>	fileNames=new	LinkedList<String>();
			for(int	i=0;i<resultTable.size();i++)
			{
				String[]	row=resultTable.get(i);
//				System.out.println(row[nameIdx]);
				if(row[nameIdx].equals("file"))
					{files.put(row[tidIdx], new FileRecord());
//					System.out.println("File: "+row[tidIdx]);
					}
				if(row[nameIdx].equalsIgnoreCase("REFERENCE_ALIGNMENT"))
				{
					r_alignment.put(row[tidIdx], new AlignmentRecord());
//					System.out.println("ReferenceAlignment: "+row[tidIdx]);
				}
				if(row[nameIdx].equals("disease_abbr"))
					disease=row[contentIdx];
				if(row[nameIdx].equals("legacy_sample_id"))
					sample=row[contentIdx];
				if(row[nameIdx].equals("sample_type"))
					sample_type_code=row[contentIdx];
				if(row[nameIdx].equals("analyte_type"))
					analyte_type=row[contentIdx];
				if(row[nameIdx].equals("ANALYSIS"))
					{
					run_alias=aliasIdx>-1?row[aliasIdx]:"NA";
					run=accessionIdx>-1?row[accessionIdx]:"NA";
					}
				if(row[nameIdx].equals("analysis_id"))
					analysis_id=row[contentIdx];
				if(row[nameIdx].equals("sample_accession"))
					sample_accession=row[contentIdx];
				if(row[nameIdx].equals("study"))
					study=row[contentIdx];
				if(row[nameIdx].equals("participant_id"))
					participant_id=row[contentIdx];
				if(row[nameIdx].equals("tss_id"))
					tss_id=row[contentIdx];
				if(row[nameIdx].equals("last_modified"))
					last_modified=row[contentIdx];
				if(row[nameIdx].equals("center_name"))
					center_name=row[contentIdx];
				if(row[nameIdx].equals("TITLE"))
					Title=row[contentIdx];
				if(row[nameIdx].equals("DESCRIPTION"))
					Description=row[contentIdx];
				if(row[nameIdx].equals("library_strategy"))
					library_strategy=row[contentIdx];
				if(row[nameIdx].equals("platform"))
					platform=row[contentIdx];
				if(row[nameIdx].equals("analysis_data_uri"))
					analysis_data_uri=row[contentIdx];
//				System.out.println("+++ TAG NAME: "+row[nameIdx]);
			}
			if(analyte_type.equals("NA"))
				if(!sample.equals("NA"))
					analyte_type=sample.substring(19, 20);
			for(int	i=0;i<resultTable.size();i++)
			{
				String[]	row=resultTable.get(i);
//				System.out.println("filename="+row[contentIdx]);
				if(row[nameIdx].equals("filename"))
					files.get(row[pidIdx]).fileName=row[contentIdx];
				if(row[nameIdx].equals("filesize"))
					files.get(row[pidIdx]).fileSize=row[contentIdx];
				if(row[nameIdx].equals("checksum"))
					files.get(row[pidIdx]).checksum=row[contentIdx];
				if(shortNameIdx>-1)
				{
					if(row[nameIdx].equals("STANDARD"))
					{
						String	REF_AL_TID=new String(row[ppidIdx]+"###").replaceFirst("\\w*\\.\\w*\\.\\w*\\.\\w*\\.\\w*\\.\\w*\\.", 
							"").replaceAll("\\.\\w*###","");
//		System.out.println("REF_AL_TID="+REF_AL_TID);
						AlignmentRecord	a_r=r_alignment.get(REF_AL_TID);
						if(a_r!=null){
							a_r.shortName=row[shortNameIdx];
							reference_alignment=a_r.shortName;
						}
					}
				}
			}
			for(String	fileID:files.keySet())
			{
				FileRecord	file=files.get(fileID);
				map.write(file.fileName); // Writing filename
				map.write("BAM"); // Writing type
				map.write(disease);
				if(!sample.equals("NA"))
					map.write(sample.substring(0, 12)); // patient
				else
					map.write("NA");
				map.write(sample);
//				System.out.println("Converting string to integer...");
//				System.out.println(Integer.parseInt("10"));
//				System.out.println("sample_type_code="+sample_type_code);
				if(!sample_type_code.equals("NA")){
					if(Integer.parseInt(sample_type_code)<10) // sample_type =tumor | normal | control
						map.write("tumor");
					else
						if(Integer.parseInt(sample_type_code)<=20)
							map.write("normal");
						else
							map.write("control");
				}
				else
					map.write("NA");
				map.write(sample_type_code);
				map.write(analyte_type);
				map.write(run);
				map.write("provisional"); // read
				map.write(analysis_id);
				map.write("NA"); // analysis accession
				map.write(study);
				map.write(sample_accession);
				map.write(participant_id);
				map.write(tss_id);
				map.write(last_modified);
				map.write(center_name);
				map.write(run_alias);
				map.write(Title);
				map.write(Description);
				map.write("REFERENCE_ALIGNMENT"); // analysis_type
				map.write(library_strategy);
				map.write(reference_alignment);
				map.write(platform);
				map.write("NA"); // portal_src
				map.write(file.fileSize);
				map.write(file.checksum);
				map.write(source_xml);
			}
		}
		
		map.flush();
		map.close();
		
		csv.close();
		
		return ErrorCode.OK;
	}

}
