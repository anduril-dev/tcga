import java.io.File;
import java.net.URL;
import java.util.LinkedList;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.tcga.AuthUtils;
import fi.helsinki.ltdk.csbl.tcga.DirBrowser;
import fi.helsinki.ltdk.csbl.tcga.Cache;

public class URLTCGABrowse extends SkeletonComponent {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		new	URLTCGABrowse().run(args);
	}
	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
	
		String	url=cf.getParameter("URL").replaceAll("://", "###").replaceAll("///","/").replaceAll("//", "/").replaceAll("###", "://");
		url=url+"###";
		url=url.replaceAll("/###", "").replaceAll("###", "");
		if(new	URL(url).getProtocol().matches("file"))
			url=url.replaceAll(".*~", "file:"+System.getProperties().getProperty("user.home"));
		
		
		File	authFile=cf.getInput("authFile");
		File	keyFile=cf.getInput("keyFile");
		
		String	authData=AuthUtils.getAuthData(authFile, keyFile);
		String	userName=AuthUtils.getUserName(authData);
		String	passwd=AuthUtils.getPasswd(authData);
		
//		DirBrowser	dir=new	DirBrowser(url, userName, passwd);
		
//		LinkedList<String>	files=dir.browseFilesOnlyExcluding(cf.getParameter("skip"));
//		LinkedList<String>	dirs=dir.browseDirsOnlyExcluding(cf.getParameter("skip"));
//		LinkedList<String>	files=dir.browseFilesOnly();
//		LinkedList<String>	dirs=dir.browseDirsOnly();

		
		String[]	columns={"name","url","size","time","isDir"};
		CSVWriter	dirWriter=new	CSVWriter(columns,cf.getOutput("files"));
		
		browseDir(url,userName,passwd,cf.getParameter("includePattern"),cf.getParameter("skip"),
				cf.getBooleanParameter("recursive"),dirWriter);
/*		
		for(String	file:files)
		{
			dirWriter.write(file);
			dirWriter.write(url+"/"+file);
			dirWriter.write(Cache.getSize(url+"/"+file, userName, passwd));
			dirWriter.write(Cache.getLastModified(url+"/"+file, userName, passwd));
			dirWriter.write(false);
		}
		
		for(String	folder:dirs)
		{
			dirWriter.write(folder);
			dirWriter.write(url+"/"+folder);
			dirWriter.write(0);
			dirWriter.write(Cache.getLastModified(url+"/"+folder, userName, passwd));
			dirWriter.write(true);
		}
*/		
		dirWriter.close();
		return ErrorCode.OK;
	}

	private void browseDir(String url,String userName,String passwd,String include,
			String exclude,boolean recursive,CSVWriter dirWriter)
	{
		DirBrowser	dir=new	DirBrowser(url, userName, passwd);
		
//		System.out.println("Browsing: "+url);
		LinkedList<String>	files=dir.browseFilesOnlyExcluding(exclude);
		LinkedList<String>	dirs=dir.browseDirsOnlyExcluding(exclude);
		
		for(String	file:files)
		{
			if(file.matches(include)){
				dirWriter.write(file);
				dirWriter.write(url+"/"+file);
				dirWriter.write(Cache.getSize(url+"/"+file, userName, passwd));
				dirWriter.write(Cache.getLastModified(url+"/"+file, userName, passwd));
				dirWriter.write(false);
			}
		}
		
		for(String	folder:dirs)
		{
			dirWriter.write(folder);
			dirWriter.write(url+"/"+folder);
			dirWriter.write(0);
			dirWriter.write(Cache.getLastModified(url+"/"+folder, userName, passwd));
			dirWriter.write(true);
			if(recursive)
				browseDir(url+"/"+folder,userName,passwd,include,exclude,recursive,dirWriter);
		}
	}
}
