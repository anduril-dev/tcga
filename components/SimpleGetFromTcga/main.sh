
set -e

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export_command

tempdir=$( gettempdir )
DIR=$(pwd)

if [[ "${TCGAUSER}" != "" ]] ; then
    WGET_PARAMS="--user ${TCGAUSER} --password ${TCGAPASSWORD}"
fi

mkdir -p "${output_dataFiles}" 

# Update mirror to execution directory

EXECDIR=$(dirname "${output_aliquotReference}" )
cd $EXECDIR

parameter_cancerType=$(echo ${parameter_cancerType} | tr '[:upper:]' '[:lower:]')
export ALIQUOTPROCESS="$DIR/grabcols.py"
export COLLATEALIQUOTS="$DIR/collate.py"
export TCGASERVER="${parameter_remoteSite}/${parameter_cancerType}"

PATH="$DIR:$PATH" # Make it easier to find wget ${WGET_PARAMS}, parallel and sem.
source $DIR/refresh.sh

if [[ "${parameter_debug}" == "false" ]] ; then
#    rm -r "${parameter_remoteSite}" &
    echo 'Not removing debug files because the component is still being developed.'
fi

if [[ "${parameter_browseOnly}" == "true" ]] ; then
    wait
    exit 0
fi

cd "${output_dataFiles}"

echo "Downloading files... this may take long!"
sort -u "${output_allURLs}" > ${tempdir}/allURLs.txt
mv ${tempdir}/allURLs.txt "${output_allURLs}"
split -n l/10 "${output_allURLs}" ${tempdir}/splitURLs
parallel --halt 1 -j "${parameter_threads}" --resume-failed --joblog ../tcgaFinal.joblog wget ${WGET_PARAMS} --retry-connrefused --waitretry=30 -np -q -nd -i {1} ::: ${tempdir}/splitURLs*
parallel -j 1 --resume-failed --joblog ../tcgaFinal.joblog wget ${WGET_PARAMS} --retry-connrefused --waitretry=30 -np -q -nd -i {1} ::: ${tempdir}/splitURLs*

if [[ "${parameter_uncompress}" == "false" ]] ; then
    echo "Exiting without uncompressing any archives."
    exit 0
fi

ARCHIVES="$(find . -maxdepth 1 -name '*.tar.gz' -print)"
if test -n "$ARCHIVES"
then
    echo "Uncompressing and deleting archives..."
    parallel -j "${parameter_threads}" "tar zxf {1}; rm {1}" ::: "$ARCHIVES"
fi

wait

