#!/bin/bash

. $ANDURIL_HOME/bash/functions.sh no_command                                    
myDir=$( dirname $0 )                                                           
cd $myDir

# Install GNU wget 1.15 and parallel

if [[ -f INSTALLED ]] ; then
    exit 0
fi

set -e

URL="http://ftp.gnu.org/gnu/wget/wget-1.15.tar.gz"
wget $URL || curl -O $URL
tar zxf wget-1.15.tar.gz                                                    
cd wget-1.15/                                                               
./configure                                                        
make -j $( grep -c CPU /proc/cpuinfo )

cd ..

cp wget-1.15/src/wget .
rm -r wget-1.15 wget-1.15.tar.gz
./wget http://git.savannah.gnu.org/cgit/parallel.git/plain/src/parallel # Test it
chmod 755 parallel
cp parallel sem

./parallel echo {1} ::: works # Test it.

touch INSTALLED

