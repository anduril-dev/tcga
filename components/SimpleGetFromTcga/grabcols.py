#!/usr/bin/env python

from __future__ import print_function

import os, errno, time, argparse, tempfile, logging, sys, re, collections, heapq, bisect, copy, pprint, random, math, itertools, csv, pdb, shutil, textwrap
import json

import pprint as pp
import itertools as it

# For stdin input, see http://stackoverflow.com/questions/7576525/optional-stdin-in-python-with-argparse

# Parses SDRF files to create aliquotReference.csv and fileReference.csv files,
# similar to GetFromTCGA component's output.
# https://wiki.nci.nih.gov/display/TCGA/Sample+and+Data+Relationship+Format

# See especially: https://wiki.nci.nih.gov/display/TCGA/Sample+and+Data+Relationship+Format#SampleandDataRelationshipFormat-RequiredColumns

# Comment [TCGA Barcode]
# For cases where Extract Name represents the UUID of a TCGA Analyte, the value in Comment [TCGA Barcode] must be the TCGA Barcode for that UUID. (Note by September 28 all submitted archives will be required to follow UUID Validation specifications. This will entail any Extracts that represent TCGA Analytes to have the UUID as Extract Name, followed by a column of "Comment [TCGA Barcode]" for the corresponding TCGA Barcode ) 

# MAGE+TAB archive name: <domain> _ <disease study> . <platform> . <archive type> . <serial index> . <revision> . <series>

class Protocol:
    def __init__(self, idx):
        self.extra = {}
        self.idx_file = None

def parse_tcga_path(path):
    # Example: tcga-data.nci.nih.gov/tcgafiles/ftp_auth/distro_ftpusers/anonymous/tumor//skcm/cgcc/bcgsc.ca/illuminahiseq_mirnaseq/mirnaseq/
    parts=path.split('/')[-5:]
    tumor,centertype,center,platform,datatype=parts
    return {"tumor":tumor,"centertype":centertype, "center":center, "platform":platform, "datatype": datatype }

def chop(s): # Split and strip to lower case.
    return [ D.strip().lower() for D in s.split(",") ]

def main(options):
    path = parse_tcga_path(options.path)
    
    # Opt out based on path elements (which are all in lower case...)
    for filterType in [ "tumor", "platform", "datatype", "center", "centertype" ]:
        opt = getattr(options, filterType)
        if opt != "all" and not path[filterType].lower() in chop(opt):
            return True

    # FIXME: "Advanced" filtering e.g. based on center names ..?

    # Conversions from documented naming
    leveldict = { "level1": "Level 1", "level2": "Level 2", "level3": "Level 3", "level4": "Level 4" }
    options.levels = [ leveldict[S] if S in leveldict else S for S in chop(options.levels) ]

    if options.sampleid!="all":
        sampleid_expressions = [re.compile(S) for S in chop(options.sampleid)]
    else:
        sampleid_expressions = [re.compile(".*")]

    reader = csv.reader(open(options.input, "rU"), delimiter="\t") # rU is for 'universal newline mode'.
    iterator = iter(reader)
    header = next(iterator)
    tcgaFilepathIdxs = []
    extract_name = None
    barcode = None

    protocols = []
    protocol = Protocol(None)

    # Parse columns, especially split protocols
    for idx, col in enumerate(header):
        if 'protocol ref' in col.lower():
            protocol=Protocol(idx)
            protocols.append(protocol)
        elif 'extract name' in col.lower():
            extract_name = idx # Either UUID or Barcode - if UUID, then TCGA Comment Barcode should be present...
        elif 'comment [tcga barcode]' in col.lower():
            barcode = idx # Either UUID or Barcode - if UUID, then TCGA Comment Barcode should be present...
        elif '[tcga archive name]' in col.lower():
            protocol.idx_archive = idx
        elif 'data file' in col.lower() or 'data matrix file' in col.lower():
            protocol.idx_file = idx
            tcgaFilepathIdxs.append(idx)
        elif '[tcga data level]' in col.lower():
            protocol.idx_level = idx
        else:
            protocol.extra[col] = idx

    fileReferenceOut = open(options.output2, "w")
#    fileReferenceOut.write("FileName\tDataLevel\tCancerType\tCenterType\tCenter\tPlatform\tDataType\tArchiveCode\tArchiveSerial\tArchiveRevision\tArchiveSeries\tArchiveFileName\tAliquots\n")
    fileReferenceOut.write("FileName\tDataLevel\tPath\tProtocolExtra\n")

    aliquotOut = open(options.output, "w")
    aliquotOut.write("AliquotCode\tAnalyteCode\tPatientCode\tDataFiles\n")
    for row in iterator:
        if barcode != None:
            name = row[barcode]
            analyteCode = name[:len("TCGA-A6-2670-01A-02D")]
            patientCode = name[:len("TCGA-A6-2670")]
        elif extract_name != None:
            name = row[extract_name]
            analyteCode = name[:len("TCGA-A6-2670-01A-02D")]
            patientCode = name[:len("TCGA-A6-2670")]
        else:
            name = "NA"
            patientCode = "NA"
            analyteCode = "NA"

        if name != "NA":
            sid = name[len("TCGA-A6-2670-"):]
            sid = sid[:sid.find("-")]
            found=False
            for exp in sampleid_expressions:
                if exp.match(sid):
                    found=True
                    break
            if not found:
                continue

        fileString = ""
        for p in protocols:
            if p.idx_file == None:
                continue
            if not "all" in options.levels and not row[p.idx_level] in options.levels:
                continue
            fileReferenceOut.write(
                    row[p.idx_file] +
                    "\t" + row[p.idx_level] +
                    "\t" + "https://" + os.path.join(options.path, row[p.idx_archive], row[p.idx_file]) +
                    "\t" + ( json.dumps({ P: row[I] for P,I in p.extra.items() }) if p.extra else "" ) +
                    "\n"
                )
            if not fileString:
                fileString = row[p.idx_file]
            else:
                fileString += "," + row[p.idx_file]

        # files = [ row[I] for I in tcgaFilepathIdxs ]
        # fileString = ",".join(files) # To get all files

        if fileString:
            aliquotOut.write("%s\t%s\t%s\t%s\n"%(
                name,
                analyteCode,
                patientCode,
                fileString ))

    aliquotOut.close()
    fileReferenceOut.close()

    return True

def setup_options():
    ''' Setup the command line options and usage help. '''
    # http://docs.python.org/dev/library/argparse.html

    class MyParser(argparse.ArgumentParser):
        def error(self, message):
            self.print_help() # Seems to confuse people
            sys.stderr.write('error: %s\n' % message)
            sys.exit(2)

    parser=MyParser(prog=__file__, formatter_class=argparse.RawDescriptionHelpFormatter, description="Something", epilog=textwrap.dedent("""\
    Undocumented.
    """))

    A=parser.add_argument
    A("-i","--input",type=str,action="store", dest='input', default="lyhyt.sdrf.csv", help="MAGE-tab file.") # Defaults to stdin!
    A("-o","--output",type=str,action="store", dest='output', default="aliquotOut.csv", help="aliquotReference.csv file path.")
    A("-O","--output2",type=str,action="store", dest='output2', default="fileReferenceOut.csv", help="fileReference.csv file path.")
    A("-l","--level",type=str,action="store", dest='levels', default="all", help="Comma-separated data levels.")
    A("-S","--sample",type=str,action="store", dest='sampleid', default="all", help="Comma-separated sampleIDs.")
    A("-T","--tumor",type=str,action="store", dest='tumor', default="all", help="Comma-separated tumors.")
    A("-P","--platform",type=str,action="store", dest='platform', default="all", help="Comma-separated platforms.")
    A("-t","--datatype",type=str,action="store", dest='datatype', default="all", help="Comma-separated datatypes.")
    A("-c","--centerType",type=str,action="store", dest='centertype', default="all", help="Comma-separated center types.")
    A("-C","--center",type=str,action="store", dest='center', default="all", help="Comma-separated centers.")
    # FIXME: Needed or not? A("-C","--centerName",type=str,action="store", dest='center', default="all", help="Comma-separated center names/codes or center+centertype combinations.")
    A("-p","--path",type=str,action="store", dest='path', help="Base path to file.")
    #A("-f","--flag",action="store_true", dest='flag', help="Boolean flag, default false.")

    options=parser.parse_args()

    return parser,options

if __name__=="__main__":
    parser,options=setup_options()
    func = main
    if "func" in options: # Call subcommand if necessary
        func = options.func
    if not func(options):
        parser.print_help()

