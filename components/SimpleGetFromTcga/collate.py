#!/usr/bin/env python

import csv,sys

def rowjoiner(rows):
    firstcell=None
    cells = None
    for cells in rows:
        if cells[0] != firstcell:
            if firstcell:
                yield cells[:-1] + [",".join(curval)]
            firstcell = cells[0]
            curval=[cells[-1]]
        else:
            curval.append(cells[-1])
    if cells:
        yield cells[:-1] + [",".join(curval)]

csv.writer(sys.stdout,delimiter='\t').writerows(rowjoiner(csv.reader(sys.stdin, delimiter='\t')))

