#!/bin/bash

# Author Lauri Lyly

# This script depends on new functionality since wget ${WGET_PARAMS} 1.14, the --reject-regex
# flag, Also depends on GNU parallel. If you get problems with GNU parallel,
# use a version directly from the GNU project instead of your default
# distribution.

# Downloads MAGE-TAB files from TCGA, and uncompresses SDRF files from them if
# needed.  The script is safe to run multiple times and will fetch any new
# files. Files not indexed from MAGE-TAB files are downloaded as such and
# unarchived.

# When resuming a failed run, remove the *.joblog files to trigger reruns.

# Creates aliquotReference.csv and fileReference.csv files for each
# uncompressed SDRF file.

# Exit codes used by the script apart from success are as follows
# 64: Query didn't match
# 65: Clinical data summary missing for a tumor type.
# Anything else: Either connection failure to TCGA, or report a bug.

set -e # Exit on errors.

SERVER="${TCGASERVER}" # Currently assumes tumor type is given in the URL
# To refactor this, it would be better to prefetch the index.html files,
# than download them every time for all tumors...

if [[ "${parameter_dataType}" == "clin" || "${parameter_dataType}" == "biotab" ]] ; then
    echo 'You have requested the clinical datatype. This is different from other data types because the raw data files have been processed to biotab files. If you actually want to download the raw clinical data files, use e.g. "CLIN" instead of "clin". You can also use "biotab" to be less ambiguous.'
    URL=${SERVER}/bcr/biotab/clin
    wget ${WGET_PARAMS} --retry-connrefused --waitretry=30 -q -O ${tempdir}/clindata $URL || true
    if [[ ! -f ${tempdir}/clindata ]] ; then
        echo -e "No summary of clinical data found at ${URL}."
        exit 65
    fi
    FILES=$(cat "${tempdir}/clindata" | grep href | tail -n +2 | grep -o '".*"' | sed -e 's/"//g')
    for F in $FILES ; do 
        echo "https://${URL}/${F}"
    done >> "${output_allURLs}"
    echo "Writing empty aliquotReference and fileReference outputs on purpose."
    touch ${output_aliquotReference} ${output_fileReference}
    return
fi

echo "Inspecting the TCGA root directory at ${SERVER}..."

# Get enough depth to grep directories for datatypes and platforms.
# --reject-regex is used to avoid downloading the redundant index.html?foo=bar
# style of files.  I am using parallel to be able to skip this step if it has
# already been run.  Kludge: wget ${WGET_PARAMS} doesn't recognize the tumor URL as a
# directory, and it is not possible to request the index.html file directly, so
# using a child directory instead (bcr is found in all of them)

parallel --halt 2 --resume-failed --joblog tcgaInitialize.joblog wget ${WGET_PARAMS} --retry-connrefused --waitretry=30 -q -np -r -l 5 -A index.html --reject-regex \"\(.*\)\\?\(.*\)\" {1}/bcr ::: "https://${SERVER}"

echo "Parallel returned $?. Matching centers and datasets at paths."

# Convert data set selection to a valid, terminating regular expression.

if [[ "${parameter_dataType}" == "all" ]] ; then
    DATA_RE="[^/]+$"
else
    DATA_RE="($(echo ${parameter_dataType} | sed -e "s/,/|/g" -e "s/ //g"))$"
fi

if [[ "${parameter_center}" == "all" ]] ; then
    CENTER_RE="[^/]+"
else
    CENTER_RE="($(echo ${parameter_center} | sed -e "s/,/|/g" -e "s/ //g"))"
fi

if [[ "${parameter_centerType}" == "all" ]] ; then
    CENTERTYPE_RE="[^/]+"
else
    CENTERTYPE_RE="($(echo ${parameter_centerType} | sed -e "s/,/|/g" -e "s/ //g"))"
fi

if [[ "${parameter_platform}" == "all" ]] ; then
    PLATFORM_RE="[^/]+"
else
    PLATFORM_RE="($(echo ${parameter_platform} | sed -e "s/,/|/g" -e "s/ //g"))"
fi

TOTAL_RE="${CENTERTYPE_RE}/${CENTER_RE}/${PLATFORM_RE}/${DATA_RE}"

SUBDIRS=$(find ${SERVER} -mindepth 4 -maxdepth 4 -type d | grep -iE "${TOTAL_RE}" || true )

if [[ "$SUBDIRS" == "" ]] ; then
    echo -e "Nothing matches your query. The regular expression used to match paths up to the datatype directory level is ${TOTAL_RE}. Here are the directories that should have matched it:\n"
    find ${SERVER} -mindepth 4 -maxdepth 4 -type d 
    exit 64
fi

echo "Matched subdirectories"
find ${SERVER} -mindepth 4 -maxdepth 4 -type d

# Isolate revision number from the archive serial index, because only one
# revision is usually needed.  The paths are split into a TSV file with the
# revision in its own column.  All archiveSerialIndices and series' are
# supposedly needed. The series are chunks of the same archive, and
# archiveSerialIndices number different archives.

# TCGA archive names follow the pattern:
# Domain.domainn_tumorType.platform.archiveSerialIndex.revision.series
# I'm pretty sure this information is obsolete. Nowadays it is more like:
# Domain.domainn_tumorType.platform.SOMETHING.archiveSerialIndex.revision.series
# where SOMETHING is Level_X, or aux, ...

for line in $SUBDIRS ; do
    # Find all archives and split their filenames by tabs.
    # It's also possible to get the filesize...
    grep -o '".*\..*\..*\..*.tar.gz"' "${line}/index.html" | sed -r 's,"(.*)\.([0-9]+)\.([0-9]+).tar.gz",'"${line}"'/\1\n\2\n\3.tar.gz,g' 
done | tsvfold prefix revision suffix > "${tempdir}/expr"

echo "Selecting only newest revisions."
tsvquery "${tempdir}/expr" 'select prefix, max(revision) as revision from tsv1 group by prefix' | tail -n +2 > "${tempdir}/grepfile"
# Reconstruct paths and split to mage-tab archives and other archives.
# Files that are not referred to from MAGE-TABs, such as aux archives,
# are also separated into their own list.
grep -f "${tempdir}/grepfile" "${tempdir}/expr" | tr '\t' '.' | tee >( grep mage-tab > "${tempdir}/mageTab.urls" ) | grep -v "mage-tab" | sed "s,^,https://," | tee >( grep -E "\.aux\." > ${tempdir}/auxFiles.urls ) > "${output_allURLs}"

if [[ ! -s "${tempdir}/mageTab.urls" ]] ; then
    echo "No mage-tab files found in the selection. Downloading the whole selection."
    echo "Writing empty aliquotReference and fileReference outputs on purpose - don't know how to extract sample information."
    touch ${output_aliquotReference} ${output_fileReference}
    return
fi

echo "Finding out which directories have MAGE-TABs. Refusing to download other
archives from those directories, because they are referred from from the
MAGE-TABs. The files are downloaded individually instead. Auxiliary archives
not referred to from MAGE-TABs will be downloaded as well if present."

while read line ; do
    echo $(dirname $line)/
done < ${tempdir}/mageTab.urls | sort -u > ${tempdir}/mageTabDirs
cp "${output_allURLs}" ${tempdir}/allUrls.beforeFiltering.urls # For debugging with _launch
(grep -vf "${tempdir}/mageTabDirs" "${output_allURLs}" || true) > "${tempdir}/allUrls.urls"
mv ${tempdir}/allUrls.urls "${output_allURLs}"
cat ${tempdir}/auxFiles.urls >> "${output_allURLs}"

echo "Downloading matching mage-tab indices."
wget ${WGET_PARAMS} --retry-connrefused --waitretry=30 -q -r -np -i "${tempdir}/mageTab.urls"

function processAliquots {
    for SDRF in ${@:3} ; do 
        # Since there could theoretically be more than one SDRF file, let's prefix them...
        # Any easier way to export these to Python?
        "${1}" -p "${2}" -i "${SDRF}" -o "${SDRF}.aliquotReference.csv" -O "${SDRF}.fileReference.csv" -l "${parameter_levelType}" $SAMPLEID -P "${parameter_platformType}"
    done
}
export -f processAliquots

echo "Uncompressing SDRF files and processing them to aliquotReference.csv and fileReference.csv files."

find ${SERVER} -name "*mage-tab*.tar.gz" -exec echo "cd $PWD/\$(dirname {}) ; EXTRACTED=\$(tar --keep-old-files --wildcards --no-anchored '*.sdrf.txt' -zxvf $PWD/{}) ; processAliquots ${ALIQUOTPROCESS} \$(dirname {}) \$EXTRACTED" \; | parallel --halt 2 -j "${parameter_threads}" --joblog tcgaSDRF.joblog

echo "Joining to combined aliquotReference and fileReference files."

echo -e "FileName\tDataLevel\tPath\tProtocolExtra" > "${output_fileReference}"
echo -e "AliquotCode\tAnalyteCode\tPatientCode\tDataFiles" > "${output_aliquotReference}"

# Sort aliquots, in case the same barcode exist in multiple SDRF files, to collate it on a single line.

find ${SERVER} -name "*aliquotReference.csv" -exec tail -n +2 {} \; | sort | "${COLLATEALIQUOTS}" >> "${output_aliquotReference}"
find ${SERVER} -name "*fileReference.csv" -exec tail -n +2 {} \; >> "${output_fileReference}"

tsvcut -c Path "${output_fileReference}" | tail -n +2 | sort -u >> "${output_allURLs}"

