import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;
import fi.helsinki.ltdk.csbl.tcga.AuthUtils;
import fi.helsinki.ltdk.csbl.tcga.Cache;
import fi.helsinki.ltdk.csbl.tcga.DirBrowser;


public class TCGASeqStorMap extends SkeletonComponent {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new	TCGASeqStorMap().run(args);
	}

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		// TODO Auto-generated method stub
		
		File	authFile=cf.getInput("authFile");
		File	keyFile=cf.getInput("keyFile");
		
		String	authData=AuthUtils.getAuthData(authFile, keyFile);
		String	userName=AuthUtils.getUserName(authData);
		String	passwd=AuthUtils.getPasswd(authData);
		
		String[]	columns={"url","name","type","disease","patient","sample","sample_type","sample_type_code","analyte_type","run","read","aux_files","size"};
		CSVWriter	fileReference=new	CSVWriter(columns,cf.getOutput("fileReference"));
		
		CSVParser	dirs=new	CSVParser(cf.getInput("dirs"));
		
		for(String[]	dir:dirs)
		{
			scanDirectory(new	URL(dir[0]),userName,passwd,fileReference);
		}
		fileReference.close();
		return ErrorCode.OK;
	}

	private	void	scanDirectory(URL	dir,String	user,String	passwd,CSVWriter	fileReference) throws MalformedURLException
	{
		DirBrowser	dirContent=new	DirBrowser(dir.toString(),user,passwd);
		
		System.out.println("Scanning "+dir.toString());
		for(String	subDir:dirContent.browseDirsOnlyExcluding("\\.svn|\\.hg"))
		{
			scanDirectory(new	URL(dir.toString()+"/"+subDir),user,passwd,fileReference);
		}
		for(String	file:dirContent.browseFilesOnly())
		{
			if(file.matches(".*\\.(bam|sra|BAM|SRA)"))
			{
				String	fileURL=dir.toString()+"/"+file;
				
				
			
//				String	read=fileURL.replace("/"+file, "").replace(".*/", "");
//				System.out.println(">>> "+read);
//				String	run=fileURL.replace("/"+read+"/"+file, "").replace(".*/", "");
//				String	aliquot=fileURL.replace("/"+run+"/"+read+"/"+file, "").replace(".*/", "");
//				String	sampleTypeDirName=fileURL.replace("/"+aliquot+"/"+run+"/"+read+"/"+file,"").replace(".*/", "");
//				String	patient=fileURL.replace("/"+sampleTypeDirName+"/"+aliquot+"/"+run+"/"+read+"/"+file,"").replace(".*/", "");
//				String	disease=fileURL.replace("/"+patient+"/"+sampleTypeDirName+"/"+aliquot+"/"+run+"/"+read+"/"+file,"").replace(".*/", "");
//				String	type=fileURL.replace("/"+disease+"/"+patient+"/"+sampleTypeDirName+"/"+aliquot+"/"+run+"/"+read+"/"+file,"").replace(".*/reads/.*","reads").replace(".*/analysis","analysis");

				String[]	decomposeURL=fileURL.split("/");
				int	n=decomposeURL.length;
				System.out.println(">>> "+fileURL);
				if(n<7)
					break;
				
				String	read=decomposeURL[n-2];
				String	run=decomposeURL[n-3];
				String	aliquot=decomposeURL[n-4];
				String	sampleTypeDir=decomposeURL[n-5];
				String	patient=decomposeURL[n-6];
				String	disease=decomposeURL[n-7];
				String	type=fileURL.replace("/"+disease+"/"+patient+"/"+sampleTypeDir+"/"+aliquot+"/"+run+"/"+read+"/"+file,"").replaceAll(".*/reads/.*","reads").replaceAll(".*/analysis","analysis");
				System.out.println("### "+type);
				if(!type.equals("reads")&&!type.equals("analysis"))
					break;
				System.out.println("Including "+fileURL);
				fileReference.write(fileURL);
				fileReference.write(file);
//				System.out.println(">>> "+read);
				String	sample_type_code=aliquot.replaceAll("TCGA-\\w\\w-\\w\\w\\w\\w-","").replaceAll("\\w-\\w\\w\\w-\\w\\w\\w\\w-\\w\\w","");
				String	sample_type="";
				if(sample_type_code.matches("01|02|03|04|05|06|07|08|09"))
					sample_type="tumor";
				else
					if(sample_type_code.matches("10|11|12|13|14|15|16|17|18|19"))
						sample_type="normal";
					else
						if(sample_type_code.matches("20|21|22|23|24|25|26|27|28|29"))
							sample_type="control";
						else
						{
							System.out.println("ERROR: unknown sample code "+sample_type_code+" in "+aliquot);
							System.exit(1);
						}
				String	analyte_type=aliquot.replaceAll("TCGA-\\w\\w-\\w\\w\\w\\w-\\w\\w\\w-\\w\\w","").replaceAll("-\\w\\w\\w\\w-\\w\\w","");
				String	auxFiles="";
				for(String	auxFile:dirContent.browseFilesOnly())
				{
					if(auxFile.matches(file+"\\..*")){
						System.out.println("Auxfile: "+auxFile);
						auxFiles=auxFiles+(auxFiles.length()>0?",":"")+auxFile;
					}
				}
				long	size=Cache.getSize(fileURL, user, passwd);
				fileReference.write(type);
				fileReference.write(disease);
				fileReference.write(patient);
				fileReference.write(aliquot);
				fileReference.write(sample_type);
				fileReference.write(sample_type_code);
				fileReference.write(analyte_type);
				fileReference.write(run);
				fileReference.write(read);
				fileReference.write(auxFiles);
				fileReference.write(size);
			}
		}
		System.out.println("Finished scanning "+dir.toString());
	}
}
